public class HL_OpportunityClientSubjectHandler {
    public static void UpdatePublicPrivate(List<Opportunity_Client_Subject__c> ocsList){
        //List of Opportunities to Update
        List<Opportunity__c> oppsToUpdate = new List<Opportunity__c>();
        //Set of Opp IDs
        Set<Id> opps = new Set<Id>();
        //Get a Map of Opps with Client/Subject ownership
        Map<Id,Set<String>> oppsToOwnership = new Map<Id,Set<String>>();
        Set<String> currentVals;
        for(Opportunity_Client_Subject__c ocs : ocsList)
            opps.Add(ocs.Opportunity__c);
        //Get all related Client/Subjects
        for(Opportunity_Client_Subject__c cs : [SELECT Opportunity__c, Client_Subject__r.Ownership FROM Opportunity_Client_Subject__c WHERE Opportunity__c IN:opps]){ 
            if(!oppsToOwnership.containsKey(cs.Opportunity__c))
                oppsToOwnership.put(cs.Opportunity__c, new Set<String> {cs.Client_Subject__r.Ownership});
            else{
                currentVals = oppsToOwnership.get(cs.Opportunity__c);
                currentVals.add(cs.Client_Subject__r.Ownership);
                oppsToOwnership.put(cs.Opportunity__c, currentVals);
            } 
        }
        //Update the Opportunities
        for(Opportunity__c opp : [SELECT Public_Or_Private__c FROM Opportunity__c WHERE Id IN:oppsToOwnership.keySet()]){
            currentVals = oppsToOwnership.get(opp.Id);
            if(currentVals.contains('Public Debt') || currentVals.contains('Public Equity'))
                opp.Public_Or_Private__c = 'Public';
            else
                opp.Public_Or_Private__c = 'Private';
            oppsToUpdate.add(opp);
        }
        
        if(oppsToUpdate.size() > 0)
            update oppsToUpdate;
    }
}