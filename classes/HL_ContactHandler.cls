public with sharing class HL_ContactHandler {
    private boolean isExecuting = false;
    private integer batchSize = 0;

    public HL_ContactHandler(boolean isExecuting, integer size) {
        this.isExecuting = isExecuting;
        this.batchSize = size;
    }

    public void OnAfterInsert(Map<Id, Contact> newContactMap) {
        this.CreateUpdateAffiliation(new Map<Id, Contact>(), newContactMap);
        this.UpdateRivaLinkObjects(newContactMap);
    }

    public void OnAfterUpdate(Map<Id, Contact> oldContactMap, Map<Id, Contact> newContactMap, List<Contact> contactList) {
        this.CreateUpdateAffiliation(oldContactMap, newContactMap);
        this.CascadeConName(contactList, oldContactMap);
		this.CreateAlumniContact(oldContactMap, newContactMap, contactList);
        //Check for Email Changes
        Map<Id, Contact> emailChangeMap = this.GetEmailChangeMap(oldContactMap, newContactMap);
        if (emailChangeMap.size() > 0)
            this.UpdateRivaLinkObjects(emailChangeMap);     
    }

    public void OnAfterDelete(Map<Id, Contact> oldContactMap) {
        this.HandleMergeScenarios(oldContactMap);
    }

    private void HandleMergeScenarios(Map<Id, Contact> oldContactMap){
        MergedData mergedData = this.GetMergedData(oldContactMap);
        this.CreateMergeAffiliations(mergedData);
        this.UpdateEventRecords(mergedData);
    }

    //Handle merge of Contacts to Properly Create the Affiliation when it is a move to a new Company
    private List<Affiliation__c> CreateMergeAffiliations(MergedData mergedData) {
        List<Affiliation__c> affiliationInsertList = new List<Affiliation__c>();

        if (mergedData.MergedContactMap.size() > 0) {
            for (Contact c : [SELECT AccountId FROM Contact WHERE Id IN: mergedData.MasterIdToMergedIdsMap.keyset()]) {
                for (Contact mergedContact : mergedData.MergedContactMap.values()) {
                    //If the Account Ids are Different, we want to Create the Affiliation Record
                    if (mergedContact.MasterRecordId == c.Id && mergedContact.AccountId != c.AccountId) {
                        affiliationInsertList.add(new Affiliation__c(Account__c = mergedContact.AccountId,
                                                  Contact__c = c.Id, Title__c = mergedContact.Title,
                                                  Type__c = 'Former Employee', Status__c = 'Former', End_Date__c = Date.today(),
                                                  Start_Date__c = mergedContact.CreatedDate.date()));
                    }
                }
            }

            if (affiliationInsertList.size() > 0)
                insert affiliationInsertList;
        }

        return affiliationInsertList;
    }

    //Handle Merge of Contacts to Update the Event Records
    private List<Event> UpdateEventRecords(MergedData mergedData) {
        List<Event> eventUpdateList = new List<Event>();

        if (mergedData.MergedContactMap.size() > 0) {
            for (Event e : [SELECT Primary_External_Contact_Id__c,
                            Primary_External_Contact__c,
                            Primary_External_Contact_Company_Id__c,
                            Primary_Attendee_Id__c,
                            Primary_Attendee__c
                            FROM Event
                            WHERE Primary_External_Contact_Id__c IN: mergedData.MergedContactMap.keySet()
                                OR Primary_Attendee_Id__c IN: mergedData.MergedContactMap.keySet()]) {

                Contact externalMasterContact = mergedData.MergedIdToNewContactMap.get(e.Primary_External_Contact_Id__c);
                Contact internalMasterContact = mergedData.MergedIdToNewContactMap.get(e.Primary_Attendee_Id__c);

                if (externalMasterContact != null) {
                    e.Primary_External_Contact_Id__c = externalMasterContact.Id;
                    e.Primary_External_Contact__c = externalMasterContact.Name;
                    e.Primary_External_Contact_Company_Id__c = externalMasterContact.AccountId;
                }

                if (internalMasterContact != null) {
                    e.Primary_Attendee_Id__c = internalMasterContact.Id;
                    e.Primary_Attendee__c = internalMasterContact.Name;
                }

                if (externalMasterContact != null || internalMasterContact != null)
                    eventUpdateList.add(e);
            }
        }

        if (eventUpdateList.size() > 0)
            update eventUpdateList;

        return eventUpdateList;
    }

    private MergedData GetMergedData(Map<Id, Contact> oldContactMap) {
        MergedData mergedData = new MergedData();

        for (Contact c : oldContactMap.values()) {
            if (!String.isBlank(c.MasterRecordId)) {
                if (mergedData.MasterIdToMergedIdsMap.get(c.MasterRecordId) == null)
                    mergedData.MasterIdToMergedIdsMap.put(c.MasterRecordId, new Set<Id> {c.Id});
                else {
                    Set<Id> mergedIdSet = mergedData.MasterIdToMergedIdsMap.get(c.MasterRecordId);
                    mergedIdSet.add(c.Id);
                    mergedData.MasterIdToMergedIdsMap.put(c.MasterRecordId, mergedIdSet);
                }

                mergedData.MergedContactMap.put(c.Id, c);
            }
        }

        //Create a Map of Merged Contact Id to New Contact, The New Contact Must be Queried for as we need other Contact Fields from the Record
        for (Contact c : [SELECT AccountId, Name FROM Contact WHERE Id IN: mergedData.MasterIdToMergedIdsMap.keySet()]) {
            Set<Id> mergedIdSet = mergedData.MasterIdToMergedIdsMap.get(c.Id);
            for (Id mergedId : mergedIdSet)
                mergedData.MergedIdToNewContactMap.put(mergedId, c);
        }

        return mergedData;
    }

    private Map<Id, Contact> GetEmailChangeMap(Map<Id, Contact> oldContactMap, Map<Id, Contact> newContactMap) {
        //Check for Email Changes
        Map<Id, Contact> emailChangeMap = new Map<Id, Contact>();

        for (Contact c : newContactMap.values()) {
            if (!String.isBlank(c.Email) && c.Email <> oldContactMap.get(c.Id).Email)
                emailChangeMap.put(c.Id, c);
        }

        return emailChangeMap;
    }

    public void CascadeConName(List<Contact> conToCascadeList, Map<Id, Contact> oldContactMap) {
        Set<Id> conIdSet = new Set<Id>();
        for (Contact con : conToCascadeList) {
            if (con.FirstName != oldContactMap.get(con.Id).FirstName || con.LastName != oldContactMap.get(con.Id).LastName)
                conIdSet.add(con.Id);
        }

        if (!conIdSet.isEmpty()) {
            List<Offsite_Template_Coverage_Contact__c> offsiteUpdateList = new List<Offsite_Template_Coverage_Contact__c>();

            for (Offsite_Template_Coverage_Contact__c otcc : [SELECT Id, Contact_Name__c, Coverage_Contact__r.Coverage_Contact__r.FirstName, Coverage_Contact__r.Coverage_Contact__r.LastName FROM Offsite_Template_Coverage_Contact__c WHERE Coverage_Contact__r.Coverage_Contact__c IN: conIdSet]) {
                otcc.Contact_Name__c = otcc.Coverage_Contact__r.Coverage_Contact__r.FirstName + ' ' + otcc.Coverage_Contact__r.Coverage_Contact__r.LastName;
                offsiteUpdateList.add(otcc);
            }

            if (!offsiteUpdateList.isEmpty())
                update offsiteUpdateList;
        }
    }

    private void CreateUpdateAffiliation(Map<Id, Contact> oldContactMap, Map<Id, Contact> newContactMap) {
        List<Affiliation__c> affiliationList = new List<Affiliation__c>();//List of Affiliation__c to create and update
        Map<Id, Date> contactIdToAffiliationEndDateMap = new Map<Id, Date>(); //Map of contact id to Affiliation__c end date where status is former
        Map<Id, Contact> filteredContactMap = new Map<Id, Contact>();//map of filtered contact
        Set<Id> contactIdHavingAffiliationSet = new Set<Id>();//set of contact ids which is having atleast one Affiliation__c record whose status is current

        List<RecordType> recordTypeList = [SELECT Id FROM RecordType WHERE RecordType.DeveloperName = 'External_Contact' LIMIT 1];

        String recordTypeId = (!recordTypeList.isEmpty()) ? recordTypeList[0].Id : '';

        //Iterating on Affiliation__c for getting the latest former Affiliation__c end date if exist
        for (Affiliation__c objAffiliation : [SELECT Id,
                                              End_Date__c,
                                              Contact__c
                                              FROM Affiliation__c
                                              WHERE Status__c = 'Former'
                                                      AND Contact__c IN: newContactMap.keySet()
                                                      AND End_Date__c != null
                                                      ORDER BY End_Date__c DESC]) {
            if (!contactIdToAffiliationEndDateMap.containsKey(objAffiliation.Contact__c))
                contactIdToAffiliationEndDateMap.put(objAffiliation.Contact__c, objAffiliation.End_Date__c);
        }

        //Iterating on contact whose account id is changed and creating new Affiliation__c records.
        for (Contact objContact : newContactMap.values()) {
            if (!oldContactMap.isEmpty() && !contactIdHavingAffiliationSet.contains(objContact.Id)
                    && oldContactMap.get(objContact.Id).AccountId != null && objContact.RecordTypeId == recordTypeId
                    && oldContactMap.get(objContact.Id).AccountId != objContact.AccountId) {
                affiliationList.add(new Affiliation__c(Account__c = oldContactMap.get(objContact.Id).AccountId,
                                                       Contact__c = objContact.Id, Title__c = objContact.Title,
                                                       Type__c = 'Former Employee', Status__c = 'Former', End_Date__c = Date.today(),
                                                       Start_Date__c = contactIdToAffiliationEndDateMap.containsKey(objContact.Id)
                                                               ? contactIdToAffiliationEndDateMap.get(objContact.Id)
                                                               : objContact.CreatedDate.date()));
            }
        }

        if (!affiliationList.isEmpty())
            upsert affiliationList;
    }

    //When a new contact is inserted, if it has been part of our activity sync (which is based on email)
    //We want to update the Riva middle object so that the contact gets linked back to the activity
    private void UpdateRivaLinkObjects(Map<Id, Contact> contactMap) {
        List<Activity_Link__c> linkUpdateList = new List<Activity_Link__c>();

        //Create a Map of Email to Contact Id
        Map<String, Id> emailMap = new Map<String, Id>();
        for (Contact c : contactMap.values()) {
            if (!String.isBlank(c.Email))
                emailMap.put(c.Email, c.Id);
        }

        //Get a list of Matching Riva Activity Link Records
        for (Activity_Link__c al : [SELECT ContactId__c, Email__c FROM Activity_Link__c WHERE ContactId__c = null AND Email__c IN: emailMap.keySet()]) {
            al.ContactId__c = emailMap.get(al.Email__c);
            linkUpdateList.add(al);
        }

        if (linkUpdateList.size() > 0)
            update linkUpdateList;

    }

    public void GeoLocationContactUpdate(Map<Id, Contact> oldContactMap, Map<Id, Contact> newContact, Boolean isInsert) {
        List<id> contactIdList = new List<Id>();
        //check if Mailing Address has been updated
        Boolean addressChangedFlag = false;
        for (Contact contact : newContact.Values()) {
            if (!isInsert) {
                Contact oldContact = oldContactMap.get(contact.Id);
                if ((contact.MailingStreet != oldContact.MailingStreet) || (contact.MailingCity != oldContact.MailingStreet) ||
                        (contact.MailingCountry != oldContact.MailingCountry) ||
                        (contact.MailingPostalCode != oldContact.MailingPostalCode)) {
                    addressChangedFlag = true;
                }
            }
            // ifaddress is null or has been changed, geocode it
            if ((contact.Geolocation__Latitude__s == null) || (addressChangedFlag == true)) {
                contactIdList.add(contact.id);
            }
        }

        if (contactIdList.size() > 0) {
            ContactGeocodeAddress.DoAddressGeocode(contactIdList);
        }
    }

    private class MergedData {
        public Map<Id, Contact> MergedContactMap {get; set;}
        public Map<Id, Contact> MergedIdToNewContactMap {get; set;}
        public Map<Id, Set<Id>> MasterIdToMergedIdsMap {get; set;}

        public MergedData() {
            this.MergedContactMap = new Map<Id, Contact>();
            this.MergedIdToNewContactMap = new Map<Id, Contact>();
            this.MasterIdToMergedIdsMap = new Map<Id, Set<Id>>();
        }
    }
	
    //Create Alumni Contact Record when Employee Record's status turns inactive
	private void CreateAlumniContact(Map<Id, Contact> oldContactMap, Map<Id, Contact> newContactMap, List<Contact> newContactList) {
        
		//Initialize & Get necessary Ids     
		Set<string> existingContactSet = new Set<string>();    
        Set<string> lastNamesSet = new Set<string>(); 
		string hlTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Houlihan Employee').getRecordTypeId();
        string externalTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('External Contact').getRecordTypeId();
        List<Account> ctAccountList = [SELECT Id, Name FROM Account WHERE Name = 'Contacts in Transition' Limit 1];
		List<Contact> alumniContacts = new List<Contact>();
        
        if(hlTypeId != null && externalTypeId != null && ctAccountList.size() > 0) {
            
            List<Contact> updatedContactList = new List<Contact>();
			
            //Check relatively small list; usually just 1, to see if there are any status updates for a HL employee record
            for(Contact newContact : newContactList) {

                if(newContact.RecordTypeId == hlTypeId && newContact.Status__c == 'Inactive' && newContact.Status__c != oldContactMap.get(newContact.Id).Status__c) {
                    updatedContactList.add(newContact);
                    lastNamesSet.add(newContact.Legal_Last_Name__c);
                    lastNamesSet.add(newContact.LastName);
                }
            }
            
            //If status update exists
            if(updatedContactList.size() > 0) {
                         
                //Build list of all external contacts 
                for(Contact existingContact : [SELECT ID, Name, FirstName, MiddleName, LastName, HL_Alumni__c, HL_Alumni_Contact__c, HL_Alumni_HR_Employee_ID__c FROM Contact 
                                               WHERE RecordTypeId = :externalTypeId AND LastName IN :lastNamesSet LIMIT 49999]) {
                                               
                    //Build Set of External Contact names and HR Alumni Employee Ids - External will not have default Employee Id
                    string HR_Id = (existingContact.HL_Alumni_HR_Employee_ID__c == null ? '' : string.valueOf(existingContact.HL_Alumni_HR_Employee_ID__c));
                    existingContactSet.add(existingContact.FirstName + ' ' + existingContact.MiddleName + ' ' + existingContact.LastName + HR_Id); 
                    existingContactSet.add(existingContact.Name + HR_Id);  
                    
                    //Build list of external contacts that are alumni and were unchecked                              
                    if(existingContact.HL_Alumni_Contact__c != null && existingContact.HL_Alumni__c == false) {                 
                        alumniContacts.add(existingContact);
                    }                               
            	}	
                List<Contact> updateContacts = new List<Contact>();
                List<Contact> createContacts = new List<Contact>();
        		Contact c;
                
                //Iterate through updated contacts
                for(Contact newContact : updatedContactList) {
					
                    boolean foundMatchingAlumni = false;
                    
                    //Find existing alumni match and check it
                    for(Contact alumni : alumniContacts) {
                        if(alumni.HL_Alumni_Contact__c == newContact.ID) {
                            alumni.HL_Alumni__c = true;
                            updateContacts.add(alumni);
                            foundMatchingAlumni = true;
                        }
                    }
                    
                    if(!foundMatchingAlumni) {

                        string contactFullNameId = '';
                    	string contactFullName = '';

                    	//List is all employee records - get Employee Id
                    	string HR_Id = (newContact.HR_EMPLOYEEID__c == null ? '' : string.valueOf(newContact.HR_EMPLOYEEID__c));
                    
                    	//Check if Legal Name exists or use regular name and build key to search set on
                    	if(newContact.Legal_First_Name__c != null && newContact.Legal_First_Name__c != '' && newContact.Legal_Last_Name__c != null && newContact.Legal_Last_Name__c != '') {
                        	contactFullNameId = newContact.Legal_First_Name__c + ' ' + newContact.Legal_Middle_Name__c + ' ' + newContact.Legal_Last_Name__c + string.valueOf(newContact.HR_EMPLOYEEID__c);
                        	contactFullName = newContact.Legal_First_Name__c + ' ' + newContact.Legal_Middle_Name__c + ' ' + newContact.Legal_Last_Name__c;
                    	}
                    	else {
                        	contactFullNameId = newContact.FirstName + ' ' + newContact.MiddleName + ' ' + newContact.LastName + string.valueOf(newContact.HR_EMPLOYEEID__c);
                        	contactFullName = newContact.FirstName + ' ' + newContact.MiddleName + ' ' + newContact.LastName;
                    	}
                                      
                    	// Check that Legal Name & Legal Name + Employee Id does not exist in External Contact Set already
                    	if(existingContactSet.size() == 0 || (!existingContactSet.contains(contactFullName) && !existingContactSet.contains(contactFullNameId) && !existingContactSet.contains(newContact.Name))) {
                        	c = new Contact(                   
                            	FirstName = newContact.Legal_First_Name__c == null || newContact.Legal_First_Name__c == ''? newContact.FirstName: newContact.Legal_First_Name__c,
                            	MiddleName = newContact.Legal_Middle_Name__c == null || newContact.Legal_Middle_Name__c == ''? newContact.MiddleName: newContact.Legal_Middle_Name__c,
                            	LastName = newContact.Legal_Last_Name__c == null || newContact.Legal_Last_Name__c == ''? newContact.LastName: newContact.Legal_Last_Name__c,
                            	Gender__c = newContact.Gender__c,
                            	Birthdate = newContact.Birthdate,
                            	AccountId = ctAccountList[0].Id,
                            	RecordTypeId = externalTypeId,
                            	HL_Alumni__c = true,
                            	HL_Alumni_Contact__c = newContact.Id
                        	);
                        	createContacts.add(c);
						
                        	//Add new contact to existing set
                        	existingContactSet.add(c.FirstName + ' ' + c.MiddleName + ' ' + c.LastName + HR_Id);  
                    	}
                	}
                }
                                      
                if(createContacts.size() > 0) {
                    insert createContacts;    
                }  
                
                if(updateContacts.size() > 0) {
                    update updateContacts;    
                }   
            }
        }
            
    }    
}