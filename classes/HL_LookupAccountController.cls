public with sharing class HL_LookupAccountController {
  public Account Account {get;set;} // new account to create
  public List<aAccount> SearchResults{get;set;} 
  public String PrimaryEntityId {get; set;}
  public String SecondaryEntityId {get; set;}
  public String SearchString{get;set;} 
  public String SearchOption {get{
        if(String.isBlank(searchOption))
            searchOption = SearchOptions[0].getValue();
        return searchOption;
    } set;}
  public List<SelectOption> SearchOptions{get{
        List<SelectOption> searchOptions = new List<SelectOption>();
        searchOptions.add(new SelectOption('Starts With','Starts With'));
        searchOptions.add(new SelectOption('Contains', 'Contains'));
        return searchOptions;
    }}
  public Integer LookupAction{get; set;}
  public Account NewAccount {get; set;}
  public String NewSort {get; set;}
  public String SortField {get{return sortField == null ? 'Name' : sortField;} set{
        toggleDirection(value);
        SortField = value;
    }}
  public String SortDirection {get{return sortDirection == null ? 'ASC' : sortDirection;} set;}
  public ApexPages.StandardSetController setCon {
        get{
             String raw = String.escapeSingleQuotes(SearchString.remove(',').remove('*'));
             String search = '%' + raw  + '%';
             String startsSearch = raw + '%';
             
            if(setCon == null)
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                    'SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Client_Number__c ' +  
                    'FROM Account ' +
                    'WHERE ' + (SearchOption == 'Contains' ? 'Name LIKE \'' + search + '\' '  : 'Name LIKE \'' + startsSearch + '\' ') 
                    + (LookupAction == 3 ? ' AND RecordType.DeveloperName IN (\'Capital_Provider\', \'Operating_company\', \'Conflicts_Check_LDCCR\') ' : '') +
                    'ORDER BY ' + SortField + ' ' + SortDirection + ' LIMIT 1000'
             ));
            setCon.setPageSize(1000);
            return setCon;
        }
        set;
    }
  public enum AccountLookupAction{
        AddDebtStructureLender, //0
        AddPreTransactionEquityHolder, //1
        AddPostTransactionEquityHolder, //2
        SingleSelectOrNewAccount, //3
        AddOpportunityPEFirm, //4
        AddPreTransactionLender, //5
        AddPostTransactionLender, //6
        AddOpportunityAdditionalClient, //7
        AddOpportunityAdditionalSubject, //8
        AddEngagementAdditionalClient, //9
        AddEngagementAdditionalSubject, //10
        AddActivityCompany //11
  }
  public HL_LookupAccountController() {
      Map<String, String> q = ApexPages.currentPage().getParameters();
      account = new Account();
      if(q.containsKey('action'))
          LookupAction = Integer.valueOf(q.get('action'));
      if(q.containsKey('entity'))
      	PrimaryEntityId = q.get('entity');
      if(q.containsKey('secondary'))
         SecondaryEntityId = q.get('secondary');   
  }
   
  public PageReference Search() {
    setCon = null;
    RunSearch();
    return null;
  }
  
  private void RunSearch() {
    SearchResults = PerformSearch(SearchString);               
  } 
  
  private List<aAccount> PerformSearch(string search) {
 	List<Account> results = setCon.getRecords();
    List<aAccount> aResults = new List<aAccount>();
    for(Account a : results)
        aResults.Add(new aAccount(a));
    return aResults; 
  }
    
  // save the new account record
  public PageReference saveAccount() {
    insert Account;
    NewAccount = Account;
    // if additional client or subject, insert the record
    AccountLookupAction action = AccountLookupAction.values()[LookupAction];
    if(action == AccountLookupAction.AddOpportunityAdditionalClient || action == AccountLookupAction.AddOpportunityAdditionalSubject){
          SearchResults = new List<aAccount>();
          SearchResults.add(new aAccount(Account,true));
          SaveSelected('Company Added To ' + (action == AccountLookupAction.AddOpportunityAdditionalClient ? 'Additional Clients' : 'Additional Subjects'));
    }
    if(action == AccountLookupAction.AddPreTransactionEquityHolder || action == AccountLookupAction.AddPostTransactionEquityHolder){
          SearchResults = new List<aAccount>();
          SearchResults.add(new aAccount(Account,true));
          SaveSelected('Company Added To Equity Holders');
    }
    // reset the account
    Account = new Account();
    return null;
  }   
  
  public void SaveSelected(String saveMsg){
     AccountLookupAction action = AccountLookupAction.values()[LookupAction];
     Set<Id> selectedAccounts = new Set<Id>();
     Map<Id,Decimal> ownershipPercentMap = new Map<Id, Decimal>();
     if(SearchResults <> null)
     {
         for(aAccount a : SearchResults)
         {
             if(a.Selected)
             {
                selectedAccounts.Add(a.Account.Id);
                ownershipPercentMap.put(a.Account.Id, a.PercentOwnership);
             }
         }
         
         if(selectedAccounts.size() > 0)
         {
             if(action == AccountLookupAction.AddDebtStructureLender)
             {
                 List<Engagement_Client_Subject__c> lendersToInsert = new List<Engagement_Client_Subject__c>();
                 for(String a : selectedAccounts)
                     lendersToInsert.add(new Engagement_Client_Subject__c(Client_Subject__c = a, Engagement__c = PrimaryEntityId, Other_Related_Object_Id__c = SecondaryEntityId, Role__c='Lender', Type__c = 'Lender'));
                 
                 InsertRecords(lendersToInsert, saveMsg);
             }
             
             if(action == AccountLookupAction.AddPreTransactionEquityHolder){
                 List<Engagement_Client_Subject__c> ehToInsert = new List<Engagement_Client_Subject__c>();
                 for(String a : selectedAccounts)
                     ehToInsert.add(new Engagement_Client_Subject__c(Client_Subject__c = a, Engagement__c = PrimaryEntityId, Percent_Ownership__c = (ownershipPercentMap.get(a) <> null ? (Decimal)ownershipPercentMap.get(a) : 0.0), Role__c = 'Pre-Transaction', Type__c = 'Equity Holder'));  
                 
                 InsertRecords(ehToInsert, saveMsg);
             } 
             
             if(action == AccountLookupAction.AddPostTransactionEquityHolder){
                 List<Engagement_Client_Subject__c> ehToInsert = new List<Engagement_Client_Subject__c>();
                 for(String a : selectedAccounts)
                     ehToInsert.add(new Engagement_Client_Subject__c(Client_Subject__c = a, Engagement__c = PrimaryEntityId, Percent_Ownership__c = (ownershipPercentMap.get(a) <> null ? (Decimal)ownershipPercentMap.get(a) : 0.0), Role__c = 'Post-Transaction', Type__c = 'Equity Holder'));  
                 
                 InsertRecords(ehToInsert, saveMsg);
             }
             
             if(action == AccountLookupAction.AddOpportunityPEFirm){
                 List<Opportunity_Client_Subject__c> peToInsert = new List<Opportunity_Client_Subject__c>();
                 for(String a : selectedAccounts)
                     peToInsert.add(new Opportunity_Client_Subject__c(Client_Subject__c = a, Opportunity__c = PrimaryEntityId, Other_Related_Object_Id__c = SecondaryEntityId, Private_Equity_Ownership__c = (ownershipPercentMap.get(a) <> null ? (Decimal)ownershipPercentMap.get(a) : 0.0), Type__c = 'PE Firm'));  
                 
                 InsertRecords(peToInsert, saveMsg);
             }
             
             if(action == AccountLookupAction.AddPreTransactionLender){
                 List<Engagement_Client_Subject__c> lToInsert = new List<Engagement_Client_Subject__c>();
                 for(String a : selectedAccounts)
                     lToInsert.add(new Engagement_Client_Subject__c(Client_Subject__c = a, Engagement__c = PrimaryEntityId, Other_Related_Object_Id__c = SecondaryEntityId, Loan_Amount__c = (ownershipPercentMap.get(a) <> null ? (Decimal)ownershipPercentMap.get(a) : 0.0), Type__c = 'Lender'));  
                 
                 InsertRecords(lToInsert, saveMsg);
             }
             
             if(action == AccountLookupAction.AddPostTransactionLender){
                 List<Engagement_Client_Subject__c> lToInsert = new List<Engagement_Client_Subject__c>();
                 for(String a : selectedAccounts)
                     lToInsert.add(new Engagement_Client_Subject__c(Client_Subject__c = a, Engagement__c = PrimaryEntityId, Other_Related_Object_Id__c = SecondaryEntityId, Loan_Amount__c = (ownershipPercentMap.get(a) <> null ? (Decimal)ownershipPercentMap.get(a) : 0.0), Type__c = 'Lender'));  
                 
                 InsertRecords(lToInsert, saveMsg);
             }
             
             if(action == AccountLookupAction.AddOpportunityAdditionalClient){
                 List<Opportunity_Client_Subject__c> oToInsert = new List<Opportunity_Client_Subject__c>();
                 for(String a : selectedAccounts)
                     oToInsert.add(new Opportunity_Client_Subject__c(Client_Subject__c = a, Opportunity__c = PrimaryEntityId, Type__c = 'Client'));  
                 
                 InsertRecords(oToInsert, saveMsg);
             }
             
             if(action == AccountLookupAction.AddOpportunityAdditionalSubject){
                 List<Opportunity_Client_Subject__c> oToInsert = new List<Opportunity_Client_Subject__c>();
                 for(String a : selectedAccounts)
                     oToInsert.add(new Opportunity_Client_Subject__c(Client_Subject__c = a, Opportunity__c = PrimaryEntityId, Type__c = 'Subject'));  
                 
                 InsertRecords(oToInsert, saveMsg);
             }
         }
         else
             HL_PageUtility.ShowError('No Company Selected');
     }
  	 else
          HL_PageUtility.ShowError('No Company Selected');
  }
   public void SaveSelected(){
       SaveSelected('Selections Saved');
   }
   
    private void InsertRecords(List<SObject> recordsToInsert, String saveMsg){
         try
         {
             insert recordsToInsert;
             ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, saveMsg));
             SearchString = null;
             SearchResults = null;
         }
        catch(DmlException ex){
            ApexPages.addMessages(ex);
        }   
    }
    public class aAccount{
        public Account Account {get; set;}
        public Boolean Selected {get; set;}
        public Decimal PercentOwnership {get; set;}
        public aAccount(Account a)
        {
            this(a,false);
        }
        public aAccount(Account a, Boolean isSelected){
            Account = a;
            Selected = isSelected;
        }
    }
    
    // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }

  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
    
  public void ApplySort()
  {
        setCon = null;
        this.SortField = NewSort;
        RunSearch();
  }
  public void toggleDirection(string newSort){
       sortDirection = (SortField == newSort) ? (sortDirection == 'DESC' ? 'ASC' : 'DESC') : 'ASC';
  }
}