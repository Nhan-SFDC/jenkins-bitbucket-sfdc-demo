public class HL_OpportunityApprovalHandler {
    public static Integer maxRelated = 5;
    
    public static Map<Id, String> GetRelatedTransactionData(List<Opportunity_Approval__c> oas, Boolean includeClient){
        List<Engagement__c> existing;
        
        //Create a Set of Opportunity IDs
        Set<Id> opps = New Set<Id>();
        for(Opportunity_Approval__c oa : oas)
        	opps.add(oa.Related_Opportunity__c);    
        
        //Create a Set of Subject IDs and a Map of Opportunity to Subject Id
        Set<Id> subjects = new Set<Id>();
        for(Opportunity__c o : [SELECT ID, Subject__c FROM Opportunity__c WHERE ID IN: opps])
            subjects.add(o.Subject__c);
 
        //Create a Map of Subject and Client Ids with related Engagements
        List<Engagement__c> engagements = Database.Query('SELECT Id, Name, Engagement_Number__c, Stage__c, Job_Type__c, Close_Date__c, Subject__c, Client__c FROM Engagement__c WHERE Subject__c IN:subjects' + (includeClient?' OR Client__c IN:subjects ' : '') + ' ORDER BY Close_Date__c DESC, Date_Engaged__c DESC');
        Map<Id,List<Engagement__c>> engageMap = new Map<Id,List<Engagement__c>>();
        
        for(Engagement__c e : engagements){
            if(engageMap.get(e.Subject__c) == null)
                engageMap.put(e.Subject__c, new List<Engagement__c> {e});
            else
            {
               existing = engageMap.get(e.Subject__c);
               if(existing.size() < maxRelated)
               {
               		existing.add(e);
               		engageMap.put(e.Subject__c, existing);
               }
            }
            If(includeClient)
            {
                if(engageMap.get(e.Client__c) == null)
                    engageMap.put(e.Client__c, New List<Engagement__c>{e});
                else {
                    existing = engageMap.get(e.Client__c);
                    if(existing.size() < maxRelated){
                        existing.add(e);
                        engageMap.put(e.Client__c,existing);
                    }
                }  
            }
        }
        
        //Create Map of Subject Id/Client Ids with Related Transaction Text
        Map<Id, String> relatedTransactionMap = new Map<Id, String>();
        for(String key : engageMap.keySet())
        {
            String tt = '';
            List<Engagement__c> eList = engageMap.get(key);
            for(Engagement__c e:eList)
                tt += e.Name + ' - ' + (e.Job_Type__c <> null ? e.Job_Type__c:'N/A') + ' - ' + (e.Engagement_Number__c <> null ? e.Engagement_Number__c:'N/A') + ' - ' + (e.Stage__c <> null ? e.Stage__c : 'N/A') + ' - ' + (e.Close_Date__c <> null ? String.valueOf(e.Close_Date__c):'Active') + '\n';
            
            relatedTransactionMap.put(key, tt);
        }
        
        return relatedTransactionMap;
    }
    
    public static void UpdateFormApproved(List<Opportunity_Approval__c> oas){
        List<Opportunity__c> oppsToUpdate = new List<Opportunity__c>();
        Set<Id> nbcToCheck = new Set<Id>();
        Set<Id> feisToCheck = new Set<Id>();
        
        for(Opportunity_Approval__c oa : oas){
            if((oa.Form_Type__c == 'NBC' || oa.Form_Type__c == 'CNBC') && !String.IsBlank(oa.Grade__c) && oa.Grade__c <> 'F')
                nbcToCheck.Add(oa.Related_Opportunity__c);
            
            if(oa.Form_Type__c == 'FEIS' && oa.Reviewed__c)
                feisToCheck.Add(oa.Related_Opportunity__c);
            
        }
        
        if(nbcToCheck.size() > 0){
            for(Opportunity__c o : [SELECT NBC_Approved__c FROM Opportunity__c WHERE Id IN:nbcToCheck AND NBC_Approved__c =: false]){
                o.NBC_Approved__c = true;
                oppsToUpdate.Add(o);
            }
        }
        if(feisToCheck.size() > 0){
            for(Opportunity__c o : [SELECT FEIS_Approved__c FROM Opportunity__c WHERE Id IN:feisToCheck AND FEIS_Approved__c =: false]){
                o.FEIS_Approved__c = true;
                oppsToUpdate.Add(o);
            }
        }
        if(oppsToUpdate.size() > 0)
           update oppsToUpdate;
    }
}