({
  aggregateProjectSelections: function(component, helper) {
    var opportunities = component.get("v.opportunities");
    var engagements = component.get("v.engagements");
    var specialProjects = component.get("v.specialProjects");
    var projectSelections = [];

    engagements.forEach(function(e) {
      projectSelections.push({
        Id: e.Id,
        DisplayType: "Engagement",
        DisplayTypeShort: "E -",
        Type: "Engagement__c",
        Name: e.Name + "-" + e.Line_of_Business__c + "-" + e.Engagement_Number__c,
        RequireComments: false
      });
    });
    specialProjects.forEach(function(sp) {
      projectSelections.push({
        Id: sp.Id,
        DisplayType: "Special Project",
        DisplayTypeShort: "S -",
        Type: "Special_Project__c",
        Name: sp.Name,
        RequireComments: sp.Require_Comments__c
      });
    });
    opportunities.forEach(function(o) {
      projectSelections.push({
        Id: o.Id,
        DisplayType: "Opportunity",
        DisplayTypeShort: "O -",
        Type: "Opportunity__c",
        Name: o.Name + "-" + o.Line_of_Business__c + "-" + o.Opportunity_Number__c,
        RequireComments: false
      });
    });

    component.set("v.projectSelections", projectSelections);
  },
  getCategory: function(component, helper) {
    helper.callServer(component, "c.GetCategory", function(response) {
      var category = response;
      component.set("v.category", category);
      helper.getIsSupervisor(component, helper, category);
    });
    helper.callServer(component, "c.GetCurrentUserId", function(response){
      component.set("v.userId", response);
    });
  },
  getIsSupervisor: function(component, helper, category) {
    helper.callServer(component, "c.IsSupervisor", function(response) {
      component.set("v.isSupervisor", response);
      helper.getTimeRecordStaffMember(component, helper, category);
    }, {
      "category": category
    });
  },
  getEngagements: function(component, helper) {
    var staffMember = component.get("v.selectedTimeRecordPeriodStaffMember");
    var category = component.get("v.category");

    if (staffMember) {
      helper.callServer(component, "c.GetEngagements", function(response) {
        component.set("v.engagements", response);
        helper.aggregateProjectSelections(component, helper);
      }, {
        "category": category,
        "userId": staffMember.User__c
      });
    }
  },
  getOpportunities: function(component, helper) {
    var staffMember = component.get("v.selectedTimeRecordPeriodStaffMember");
    var category = component.get("v.category");

    if (staffMember) {
      helper.callServer(component, "c.GetOpportunities", function(response) {
        component.set("v.opportunities", response);
        helper.aggregateProjectSelections(component, helper);
      }, {
        "category": category,
        "userId": staffMember.User__c
      });
    }
  },
  getSpecialProjects: function(component, helper) {
    helper.callServer(component, "c.GetSpecialProjects", function(response) {
      component.set("v.specialProjects", response);
      helper.aggregateProjectSelections(component, helper);
    });
  },
  getTimeRecordStaffMember: function(component, helper, category) {
    helper.callServer(component, "c.GetCurrentTimeRecordPeriodStaffMemberRecord", function(response) {
      component.set("v.selectedTimeRecordPeriodStaffMember", response);
      helper.onSelectedStaffMemberChanged(component, helper);
    }, {
      "category": category
    });
  },
  onSelectedStaffMemberChanged: function(component, helper) {
    helper.getOpportunities(component, helper);
    helper.getEngagements(component, helper);
    helper.getSpecialProjects(component, helper);
  },
  setActiveTab: function(component, activeTab, inactiveTabs) {
    var elemTabHeadingActive = document.getElementById(activeTab);
    var elemTabActive = document.getElementById(activeTab.replace('-heading', ''));

    if (!$A.util.hasClass(elemTabHeadingActive, "slds-active"))
      $A.util.addClass(elemTabHeadingActive, "slds-active");

    inactiveTabs.forEach(function(tab) {
      var element = document.getElementById(tab);
      if ($A.util.hasClass(element, "slds-active"))
        $A.util.removeClass(element, "slds-active");
    });

    if (!$A.util.hasClass(elemTabActive, "slds-show")) {
      $A.util.addClass(elemTabActive, "slds-show");
      if ($A.util.hasClass(elemTabActive, "slds-hide"))
        $A.util.removeClass(elemTabActive, "slds-hide");
    }

    inactiveTabs.forEach(function(tab) {
      var element = document.getElementById(tab.replace('-heading', ''));

      if ($A.util.hasClass(element, "slds-show")) {
        $A.util.removeClass(element, "slds-show");

        if (!$A.util.hasClass(element, "slds-hide"))
          $A.util.addClass(element, "slds-hide");
      }

      component.set("v.activeTab", activeTab.replace('-heading', ''));
    });

    //Most Tabs will Show the Period Picker and Staff List so Default Here and the Exceptions will Handle on their own.
    component.set("v.periodPickerClass", "");
    component.set("v.staffListClass", "");
  },
  staffTimeSheetTabSelected: function(component, event, helper) {
    var staffTimeSheetActiveTab = event.getParam("selectedId");
    if (staffTimeSheetActiveTab === "tab-recorder") {
      component.set("v.periodPickerClass", "hidden");
      component.set("v.staffListClass", "hidden");
    } else {
      component.set("v.periodPickerClass", "");
      component.set("v.staffListClass", "");
    }
  },
  toggleSpinner: function(component, hide) {
    var m = component.find("modalspinner");

    if (hide) {
      $A.util.addClass(m, "slds-hide");
      component.set("v.suppressSpinners", true);
    } else
      $A.util.removeClass(m, "slds-hide");
    var spinner = component.find('spinner');
    var evt = spinner.get("e.toggle");
    evt.setParams({
      isVisible: !hide
    });
    evt.fire();
  }
})