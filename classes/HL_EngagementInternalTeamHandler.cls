public class HL_EngagementInternalTeamHandler {
    private boolean isExecuting = false;
	private integer batchSize = 0;
	public boolean IsTriggerContext{get{ return isExecuting;}}
    public static Boolean isAfterInsertFlag = false;
    public static Boolean isAfterUpdateFlag = false;
    public static Boolean isAfterDeleteFlag = false;
    
    public HL_EngagementInternalTeamHandler(boolean isExecuting, integer size){
     	isExecuting = isExecuting;
        batchSize = size;   
    }
    
    /*
        @MethodName         : OnAfterInsert
        @param              : List of Engagement_Internal_Team__c
        @Description        : This function will be called on after insert of the Engagement_Internal_Team__c records 
    */
     public void OnAfterInsert(List<Engagement_Internal_Team__c> eitList){
        if(!isAfterInsertFlag){
            isAfterInsertFlag = true;
        	UpdateInternalTeamAggregate(eitList);
            SL_ManageSharingRules.manageSharingRules(eitList, 'Engagement__c', 'Engagement_Internal_Team__c', 'insert');
        }
    }

    /*
        @MethodName         : OnAfterUpdate
        @param              : Old and new map of Engagement_Internal_Team__c
        @Description        : This function will be called on after update of the Engagement_Internal_Team__c records 
    */
    public void OnAfterUpdate(List<Engagement_Internal_Team__c> eitList, Map<Id, Engagement_Internal_Team__c> mapOldEIT, Map<Id, Engagement_Internal_Team__c> mapNewEIT)
    {
        if(!HL_TriggerContextUtility.ByPassOnPercentages)
            UpdateInternalTeamAggregate(eitList); 
        SL_ManageSharingRules.createDeleteShareOnUpdate(mapOldEIT, mapNewEIT, 'Engagement__c', 'Engagement_Internal_Team__c'); 
    }
    
    /*
        @MethodName         : OnAfterDelete
        @param              : map of Engagement_Internal_Team__c  
        @Description        : This function will be called on after delete of the Engagement_Internal_Team__c records
                              to delete the engagement share records related to Engagement_Internal_Team__c 
    */
    public void onAfterDelete(List<Engagement_Internal_Team__c> eitList, Map<Id, Engagement_Internal_Team__c> mapEITOld)
    {
        if(!isAfterDeleteFlag){
            isAfterDeleteFlag = true;
            UpdateInternalTeamAggregate(eitList);
        }
        
        SL_ManageSharingRules.manageSharingRules(mapEITOld.values(), 'Engagement__c', 'Engagement_Internal_Team__c', 'delete');
    }

    public void onAllAfterEvents(Map<Id, Engagement_Internal_Team__c> oldMap, Map<Id, Engagement_Internal_Team__c> newMap)
    {
        HL_TriggerContextUtility.ByPassOnPercentages = true;
        HL_PercentageSplitHandler handler = new HL_PercentageSplitHandler();
        handler.recalculate(oldMap, newMap);
    }  
    
	private void UpdateInternalTeamAggregate(List<Engagement_Internal_Team__c> eitList){
        //Engagements To Update
        List<Engagement__c> eToUpdate = new List<Engagement__c>();
        //Create Set of Engagement Ids
        Set<Id> engs = new Set<Id>();
        for(Engagement_Internal_Team__c eit : eitList)
            engs.add(eit.Engagement__c);
         //Create a map of Engagement|role with team member ids
        Map<String,Set<String>> engTeamMap = new Map<String,Set<String>>();
        for(Engagement_Internal_Team__c er : [SELECT Engagement__c, Contact__r.User__c, Staff_Role__r.Name FROM Engagement_Internal_Team__c WHERE Engagement__c != null AND Engagement__c IN : engs AND End_Date__c = null])
        {
            String key = er.Engagement__c + '_' + er.Staff_Role__r.Name;
            if(engTeamMap.get(key) == null)
                engTeamMap.put(key, new Set<String>{er.Contact__r.User__c});
            else
            {
                Set<String> existingIds = (Set<String>)engTeamMap.get(key);
                existingIds.add(er.Contact__r.User__c);
                engTeamMap.put(er.Engagement__c, existingIds);
            }
        }
        //Get engagements that may need to be updated
        for(Engagement__c e : [SELECT Id, Internal_Team_Aggregate__c FROM Engagement__c WHERE Id IN : engs])
        {
                e.z_Admin_Intern_Aggregate__c = PopulateAggregateField(e.Id + '_Intern', engTeamMap);
                e.z_Analyst_Aggregate__c = PopulateAggregateField(e.Id + '_Analyst', engTeamMap);
                if(e.z_Analyst_Aggregate__c.Length() > 255)
                {
                    e.z_Analyst_Aggregate2__c = e.z_Analyst_Aggregate__c.subString(247);
                    e.z_Analyst_Aggregate__c = e.z_Analyst_Aggregate__c.subString(0,246);
                }
                e.z_Associate_Aggregate__c = PopulateAggregateField(e.Id + '_Associate', engTeamMap);
                if(e.z_Associate_Aggregate__c.Length() > 255){
                    e.z_Associate_Aggregate2__c = e.z_Associate_Aggregate__c.subString(247);
                    e.z_Associate_Aggregate__c = e.z_Associate_Aggregate__c.subString(0,246);
                }
                e.z_Final_Rev_Aggregate__c = PopulateAggregateField(e.Id + '_Final Reviewer', engTeamMap);
                e.z_Initiator_Aggregate__c= PopulateAggregateField(e.Id + '_Initiator', engTeamMap);
                if(e.z_Initiator_Aggregate__c.Length() > 255){
                    e.z_Initiator_Aggregate2__c = e.z_Initiator_Aggregate__c.subString(247);
                    e.z_Initiator_Aggregate__c = e.z_Initiator_Aggregate__c.subString(0,246);
                }
                e.z_Manager_Aggregate__c = PopulateAggregateField(e.Id + '_Manager', engTeamMap);
                e.z_Marketing_Aggregate__c = PopulateAggregateField(e.Id + '_Marketing Team', engTeamMap);
                if(e.z_Marketing_Aggregate__c.Length() > 255){
                    e.z_Marketing_Aggregate2__c = e.z_Marketing_Aggregate__c.subString(247);
                    if(e.z_Marketing_Aggregate2__c.Length() > 255){
                        e.z_Marketing_Aggregate3__c = e.z_Marketing_Aggregate2__c.subString(247);
                        e.z_Marketing_Aggregate2__c = e.z_Marketing_Aggregate2__c.subString(0,246);
                    }
                    e.z_Marketing_Aggregate__c = e.z_Marketing_Aggregate__c.subString(0,246);
                }
                e.z_NonRegistered_Aggregate__c = PopulateAggregateField(e.Id + '_Non-Registered', engTeamMap);
                e.z_PE_HF_Aggregate__c = PopulateAggregateField(e.Id + '_PE/Hedge Fund', engTeamMap);
                e.z_Prelim_Rev_Aggregate__c = PopulateAggregateField(e.Id + '_Prelim Reviewer', engTeamMap);
                e.z_Pricing_Aggregate__c = PopulateAggregateField(e.Id + '_Pricing Committee Approver', engTeamMap);
                e.z_Principal_Aggregate__c = PopulateAggregateField(e.Id + '_Principal', engTeamMap);
                e.z_Public_Aggregate__c = PopulateAggregateField(e.Id + '_Public Person', engTeamMap);
                e.z_Reviewer_Aggregate__c = PopulateAggregateField(e.Id + '_Reviewer', engTeamMap);
                e.z_Seller_Aggregate__c = PopulateAggregateField(e.Id + '_Seller', engTeamMap);
                e.z_Specialty_Aggregate__c = PopulateAggregateField(e.Id + '_Ind/Prod/Geog Person', engTeamMap);
            	eToUpdate.add(e);
        }
        
        if(eToUpdate.size() > 0)
           update eToUpdate;
    }
    
    private static String PopulateAggregateField(String mapKey, Map<String,Set<String>> teamMap){
        String fieldValue = '';
        if(teamMap.get(mapKey) <> null){
            for(String id : (Set<String>)teamMap.get(mapKey)){
                if(id <> null)
                	fieldValue = String.isBlank(fieldValue) ? id : (fieldValue.contains(id) ? fieldValue : fieldValue + '|' + id);
            }
        }
        else
            fieldValue = '';
        
        return fieldValue;
    }
}