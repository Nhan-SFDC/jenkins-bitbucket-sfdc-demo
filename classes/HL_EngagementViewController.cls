public class HL_EngagementViewController {
    //Assuming that SysAdmin and CAO's are included as part of EIT Permissions
    public Boolean OnEIT{get{
        if(onEIT == null)
            onEIT = HL_Utility.IsSysAdmin() || HL_Utility.IsCAO() || HL_EIT.IsActiveOnTeam(Engagement.Id,UserInfo.getUserId());
        return onEIT;
    }set;}
    public Boolean AddCommentsAccess{get{ return OnEIT;}}
    public Engagement__c Engagement {get; set;}
    public HL_EngagementViewController(ApexPages.StandardController controller){
        Engagement = (Engagement__c)controller.getRecord();
    }
}