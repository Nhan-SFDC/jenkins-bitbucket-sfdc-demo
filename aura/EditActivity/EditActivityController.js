({
	doInit:function(component, event, helper){
        var activityId = component.get("v.activityId");
        var getActivityAction = component.get("c.GetById");
        var primaryId;
        
        getActivityAction.setParams({eventId:activityId});
        getActivityAction.setCallback(this, function(a){
           var eventRecord = a.getReturnValue();
           component.set("v.activity", eventRecord);
           component.set("v.primaryAttendee", eventRecord.Primary_External_Contact_Id__c);
           component.set("v.primaryEmployee", eventRecord.Primary_Attendee_Id__c);
           primaryId = eventRecord.ParentId__c == null ? activityId : eventRecord.ParentId__c;
           var attendeesAction = component.get("c.GetAttendees");
           var employeesAction = component.get("c.GetEmployees");
           var companiesAction = component.get("c.GetComps");
           var oppsAction = component.get("c.GetOpps");
           var campAction = component.get("c.GetCamp"); 
           attendeesAction.setParams({"eventId":primaryId});
           employeesAction.setParams({"eventId":primaryId});
           companiesAction.setParams({"eventId":primaryId});
           oppsAction.setParams({"eventId":primaryId}); 
           campAction.setParams({"eventId":primaryId});
           attendeesAction.setCallback(this,function(aa){component.set("v.relatedAttendees",aa.getReturnValue()); });
           employeesAction.setCallback(this,function(ea){component.set("v.relatedEmployees",ea.getReturnValue()); });
           companiesAction.setCallback(this,function(ca){component.set("v.relatedCompanies",ca.getReturnValue()); });
           oppsAction.setCallback(this,function(oa){component.set("v.relatedOpps",oa.getReturnValue()); });
           campAction.setCallback(this,function(cp){component.set("v.relatedCamp",cp.getReturnValue()); });
           $A.enqueueAction(attendeesAction);
           $A.enqueueAction(employeesAction);
           $A.enqueueAction(companiesAction);
           $A.enqueueAction(oppsAction);
           $A.enqueueAction(campAction);
           helper.populateActivityTypes(component, helper);
        });
	 	$A.enqueueAction(getActivityAction);
        
        //Get the Activity Supplement Record
        var activitySupplementAction = component.get("c.GetActivitySupplement");
        activitySupplementAction.setParams({eventId:activityId});
        activitySupplementAction.setCallback(this, function(response){
            component.set("v.activitySupplement", response.getReturnValue());                                 
        });       
        $A.enqueueAction(activitySupplementAction);
    },
    onFollowupCheck : function(component, event, helper) {
        var followup = component.find("scheduleFollowup");
        //Toggle the Followup Fields
        component.set("v.hasFollowup",followup.get("v.value"));
	}, 
    onFollowupClick : function(component){
      	var hasFollowup = component.find("scheduleFollowup").get("v.value");
        component.find("scheduleFollowup").set("v.value", !hasFollowup);
        component.set("v.hasFollowup",!hasFollowup);
    },
    onFollowupStartDateChange : function(component){
        //component.find("followupEndDate").set("v.value", component.find("followupStartDate").get("v.value"));
    },
    onFollowupStartTimeChange : function(component){
      component.find("followupEndTime").set("v.value", component.find("followupStartTime").get("v.value"));
    },
    onFollowupSyncToOutlookClick : function(component){
        var followupSyncToOutlook = component.find("followupSyncToOutlook").get("v.value");
        component.find("followupSyncToOutlook").set("v.value", !followupSyncToOutlook);
    },
    onPrimaryEmployeeClick : function(component, event){
      	var source = event.getSource();
        var id = source.get("v.buttonTitle");
        component.set("v.primaryEmployee",id);
    },
    onPrimaryExternalAttendeeClick : function(component, event){
      	var source = event.getSource();
        var id = source.get("v.buttonTitle");
        component.set("v.primaryAttendee",id);
    },
    onPrivateClick : function(component, event){
         var isPrivate = component.find("isPrivate").get("v.value");
         component.find("isPrivate").set("v.value", !isPrivate);
    },
    onNotifyCoverageTeamClick : function(component, event){
        var notifyCoverageTeam = component.find("notifyCoverageTeam").get("v.value");
        component.find("notifyCoverageTeam").set("v.value", !notifyCoverageTeam);
    },
    onRemoveExternalAttendeeClick : function(component, event, helper){
        if(confirm('Remove?')){
            helper.removeRelatedObject(component,event,"v.externalAttendees");
        }  
    },
    onRemoveEmployeeClick : function(component, event, helper){
        if(confirm('Remove?')){
            helper.removeRelatedObject(component,event,"v.hlEmployees");
        }  
    },
    onRemoveCompanyClick : function(component, event, helper){
        if(confirm('Remove?')){
            helper.removeRelatedObject(component,event,"v.companiesDiscussed");
        }  
    },
    onRemoveOppClick : function(component, event, helper){
        if(confirm('Remove?')){
            helper.removeRelatedObject(component,event,"v.opportunities");
        }  
    },
    onRemoveCampClick : function(component, event, helper){
        if(confirm('Remove?')){
            helper.removeRelatedObject(component,event,"v.campaign");
        }  
    },
    onStartDateChange : function(component){
        //component.find("endDate").set("v.value", component.find("startDate").get("v.value"));
    },
    onStartTimeChange : function(component){
      component.find("endTime").set("v.value", component.find("startTime").get("v.value"));
    },
    onSyncToOutlookClick : function(component){
        var syncToOutlook = component.find("syncToOutlook").get("v.value");
        component.find("syncToOutlook").set("v.value", !syncToOutlook);
    },
    onTypeChange : function(component, event, helper){
       helper.onTypeChanged(component, helper, true);
    },
    handleExternalContactSelection:function(component, event, helper){
        var so = event.getParam("selectedOption");
        var externalAttendees = component.get("v.relatedAttendees");
        if (!externalAttendees) externalAttendees = [];
        externalAttendees.push(so);
        component.set("v.relatedAttendees",externalAttendees);
    },  
    handleHLEmployeeSelection:function(component, event, helper){
        var so = event.getParam("selectedOption");
        var emps = component.get("v.relatedEmployees");
        if (!emps) emps = [];
        emps.push(so);
        component.set("v.relatedEmployees",emps);
    },  
    handleCompanySelection:function(component, event, helper){
        var so = event.getParam("selectedOption");
        var comps = component.get("v.relatedCompanies");
        if (!comps) comps = [];
        comps.push(so);
        component.set("v.relatedCompanies",comps);
    },  
    handleOpportunitySelection:function(component, event, helper){
        var so = event.getParam("selectedOption");
        var opps = component.get("v.relatedOpps");
        if (!opps) opps = [];
        opps.push(so);
        component.set("v.relatedOpps",opps);
    },
    handleCampaignSelection:function(component, event, helper){
        var so = event.getParam("selectedOption");
        var camp = component.get("v.relatedCamp");
        if (!camp) camp = [];
        camp.push(so);
        component.set("v.relatedCamp",camp);
    },
    saveActivity : function(component, event, helper){
        component.find("btnSave").set("v.disabled",true);
        var isValid = true;
        var activityId = component.get("v.activityId");
        var type = component.find("type").get("v.value");
        var subject = component.find("subject").get("v.value");
        var description = component.find("description").get("v.value");
        var internalNotes = component.find("internalNotes").get("v.value");
        var startDate = component.find("startDate").get("v.value");
        var startTime = component.find("startTime").get("v.value");
        var endDate = component.find("startDate").get("v.value");
        var endTime = component.find("endTime").get("v.value");
        var isPrivate = component.find("isPrivate").get("v.value");
        var syncToOutlook = component.find("syncToOutlook").get("v.value");
        var notify = component.find("notifyCoverageTeam").get("v.value");
        var isComplete = false;
        if(component.find("isComplete"))
        	 isComplete = component.find("isComplete").get("v.value");
        var hasFollowup = component.find("scheduleFollowup").get("v.value");
        var fuType = component.find("followupType").get("v.value");
        var fuStartDate = component.find("followupStartDate").get("v.value");
        var fuStartTime = component.find("followupStartTime").get("v.value");
        var fuEndDate = component.find("followupStartDate").get("v.value");
        var fuEndTime = component.find("followupEndTime").get("v.value");
        var fuSyncToOutlook = component.find("followupSyncToOutlook").get("v.value");
        var fuComments = component.find("followupComments").get("v.value");
        var pa = component.get("v.primaryAttendee");
        var pe = component.get("v.primaryEmployee");
        var externalAttendees = component.get("v.relatedAttendees");
        var emps = component.get("v.relatedEmployees");
        var comps = component.get("v.relatedCompanies");
        var opps = component.get("v.relatedOpps");
        var camp = component.get("v.relatedCamp");
        //Call the Server-Side Save Activity Action
        var action = component.get("c.Save");
        //Validate required fields
        if(!type || !subject || !startDate || !endDate){
            isValid = false;
            alert('Type, Subject and Date Fields are Required');
        }
        //Needs to have both an External and Employee Specified
        if(isValid && (externalAttendees.length <= 0 || emps.length <= 0))
        {
            isValid = false;
            alert('At least one External Attendee and HL Employee Required');
        }
        //Validate Followup Date
        if(isValid && hasFollowup){
            if(!fuStartDate){
                isValid = false;
                alert('Follow-up Date is Required');
            }
            if(!fuType){
            	isValid = false;
            	alert('Follow-up Type is Required');
            }
        }

        if(isValid){
            action.setParams({ activityId: activityId,
                  			   type : type,
                               subject: subject,
                               description: description,
                               internalNotes: internalNotes,
                               startDate:startDate,
                               startTime:startTime,
                               endDate:endDate,
                               endTime:endTime,
                               isPrivate:isPrivate,
                               sync:syncToOutlook,
                               notify:notify,
                               isComplete:isComplete,
                               hasFollowup:hasFollowup,
                               fuType:fuType,
                               fuStartDate:fuStartDate,
                               fuStartTime:fuStartTime,
                               fuEndDate:fuEndDate,
                               fuEndTime:fuEndTime,
                               fuSync:fuSyncToOutlook,
                               fuDescription:fuComments,
                               primaryAttendee:pa,
                               primaryEmp:pe,
                               attendeesJSON:JSON.stringify(externalAttendees),
                               empsJSON:JSON.stringify(emps),
                               compsJSON:JSON.stringify(comps),
                               oppsJSON:JSON.stringify(opps),
                               campJSON:JSON.stringify(camp)});
            // Create a callback that is executed after 
            // the server-side action returns
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    
                   helper.showToast();
                    var action = $A.get("e.force:navigateToComponent");
                    action.setParams({
                        componentDef: "c:MyActivities"
                    });
                    action.fire();
                }
                else if (state === "ERROR"){
                    var errors = response.getError();
                    alert(errors[0].message);
                    if (errors) {
                        $A.logf("Errors", errors);
                        if (errors[0] && errors[0].message) {
                            $A.error("Error message: " + 
                                     errors[0].message);
                        }
                    } else {
                        component.find("btnSave").set("v.disabled",false);
                        $A.error("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }
        else
            component.find("btnSave").set("v.disabled",false);
    },
    scriptsLoaded: function(component, event, helper){
        if (typeof jQuery !== "undefined" && typeof $j === "undefined") {
        	$j = jQuery.noConflict(true);
    	} 
    }
})