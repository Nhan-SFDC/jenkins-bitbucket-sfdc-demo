@isTest
private class Test_HL_OpportunityInternalTeamHandler {
    @isTest private static void TestTeamMemberChanges(){
        List<Account> accountList = SL_TestSetupUtils.CreateAccount('Account' , 1);
        insert accountList;
        Contact c = SL_TestSetupUtils.CreateContact('Contact', 1, SL_TestSetupUtils.ContactType.HOULIHAN_EMPLOYEE)[0];
        c.AccountId = accountList[0].Id;
        insert c;    
        List<String> oppTeamRoles = new List<String> {'Principal','Seller','Manager','Associate','Initiator','Analyst'};
        Opportunity__c opp = SL_TestSetupUtils.CreateOpp('',1)[0];
        insert opp;
        List<Opportunity_Internal_Team__c> oitList = SL_TestSetupUtils.CreateOIT('', oppTeamRoles.size());
        List<Staff_Role__c> staffRoles = new List<Staff_Role__c>();
        
        for(Integer i = 0; i<oppTeamRoles.size(); i++)
            staffRoles.add(new Staff_Role__c(Name=oppTeamRoles[i], Display_Name__c = oppTeamRoles[i], CF__c = true, FAS__c = true));
        insert staffRoles;
        
        for(Integer i = 0; i<oppTeamRoles.size(); i++)
        {
            oitList[i].Contact__c = c.Id;
            oitList[i].Opportunity__c = opp.Id;
            oitList[i].Staff_Role__c = staffRoles[i].Id;
        }
        
        Test.startTest();
            insert oitList;
            delete oitList[0]; 
       	Test.stopTest();
        
        System.assert(([SELECT Id FROM Opportunity_Internal_Team__c]).size() > 0);
    }
}