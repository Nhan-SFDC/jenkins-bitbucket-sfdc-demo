public class HL_EngagementClientSubjectHandler {
    private boolean isExecuting = false;
	private integer batchSize = 0;
	public boolean IsTriggerContext{get{ return isExecuting;}}
    public static Boolean isAfterInsertFlag = false;
    public static Boolean isAfterUpdateFlag = false;
    public static Boolean isAfterDeleteFlag = false;
    
    public HL_EngagementClientSubjectHandler(boolean isExecuting, integer size){
        isExecuting = isExecuting;
        batchSize = size;
    }
    
    public void OnAfterInsert(List<Engagement_Client_Subject__c> ecsList){
        if(!isAfterInsertFlag){
            isAfterInsertFlag = true;
        	UpdatePublicPrivate(ecsList);
        }
    }
    
    public void OnAfterUpdate(List<Engagement_Client_Subject__c> ecsList){
        if(!isAfterUpdateFlag){
            isAfterUpdateFlag = true;
        	UpdatePublicPrivate(ecsList);
        }
    }
    
     public void OnAfterDelete(List<Engagement_Client_Subject__c> ecsList){
         if(!isAfterDeleteFlag){
            isAfterDeleteFlag = true;
        	UpdatePublicPrivate(ecsList);
         }
    }
    
	private void UpdatePublicPrivate(List<Engagement_Client_Subject__c> ecsList){
        //List of Engagements to Update
        List<Engagement__c> engsToUpdate = new List<Engagement__c>();
        //Set of Eng IDs
        Set<Id> engs = new Set<Id>();
        //Get a Map of Engs with Client/Subject ownership
        Map<Id,Set<String>> engsToOwnership = new Map<Id,Set<String>>();
        Set<String> currentVals;
        for(Engagement_Client_Subject__c ecs : ecsList)
            engs.Add(ecs.Engagement__c);
        if(engs.size() > 0){
            //Get all related Client/Subjects
            for(Engagement_Client_Subject__c cs : [SELECT Engagement__c, Client_Subject__r.Ownership FROM Engagement_Client_Subject__c WHERE Engagement__c IN:engs]){ 
                if(!engsToOwnership.containsKey(cs.Engagement__c))
                    engsToOwnership.put(cs.Engagement__c, new Set<String> {cs.Client_Subject__r.Ownership});
                else{
                    currentVals = engsToOwnership.get(cs.Engagement__c);
                    currentVals.add(cs.Client_Subject__r.Ownership);
                    engsToOwnership.put(cs.Engagement__c, currentVals);
                } 
            }

            //Update the Engagements
            for(Id engId : engsToOwnership.keySet()){
                currentVals = engsToOwnership.get(engId);
                engsToUpdate.add(new Engagement__c(
                                                   Id = engId, 
                                                   Public_Or_Private__c = (currentVals.contains('Public Debt') || currentVals.contains('Public Equity') ? 'Public' : 'Private')
                                                  )
                                ); 
            }

            if(engsToUpdate.size() > 0)
                update engsToUpdate;
       }
   }
}