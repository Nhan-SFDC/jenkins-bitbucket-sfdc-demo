global class HL_TimeRecord {
    public static List<Time_Record__c> GetByUser(Id userId, Date startDate, Date endDate){
        return [SELECT Activity_Date__c
                       ,Activity_Type__c
                       ,Amount__c
                       ,Comments__c
                       ,CurrencyIsoCode
                       ,Opportunity__c
                       ,Engagement__c
                       ,Special_Project__c
                       ,Project_Name__c
                       ,Hourly_Rate__c
                       ,Hours_Worked__c
                       ,Staff_Member_Name__c
                       ,Is_Locked__c
                       ,Day_Rollup_Key__c
                FROM Time_Record__c
                WHERE Time_Record_Period_Staff_Member__r.User__c =: userId
                      AND Activity_Date__c >=: startDate
                      AND Activity_Date__c <=: endDate
                ORDER BY Activity_Date__c DESC, Project_Name__c];
    }

    //Aggregates the time records to be stored in the rollup day object
    public static List<AggregateResult> GetAggregatedTimeRecordsByDate(Set<Date> activityDateSet, Set<String> relatedObjectIdSet, Set<Id> smSet){
        return  [SELECT
                 Activity_Date__c,
                 Activity_Type__c activity,
                 Engagement__c,
                 Opportunity__c,
                 Special_Project__c,
                 Time_Record_Period_Staff_Member__c,
                 CurrencyIsoCode,
                 MAX(Day_Rollup_Key__c) key,
                 MAX(Hourly_Rate__c) rate,
                 SUM(Hours_Worked__c) sum
                 FROM Time_Record__c
                 WHERE Time_Record_Period_Staff_Member__c IN:smSet AND Activity_Date__c IN:activityDateSet
                       AND (Opportunity__c IN:relatedObjectIdSet OR Engagement__c IN:relatedObjectIdSet OR Special_Project__c IN:relatedObjectIdSet)
                 GROUP BY Activity_Date__c, Activity_Type__c, CurrencyIsoCode, Engagement__c, Opportunity__c, Special_Project__c, Time_Record_Period_Staff_Member__c];
    }

    public static List<Time_Record__c> GetByEngagements(Id recordTypeId, Set<Id> engagementIdSet){
        return [SELECT Engagement__c,
                       Activity_Date__c,
                       Time_Record_Period_Staff_Member__r.Title__c,
                       Hourly_Rate__c
                FROM Time_Record__c
                WHERE RecordTypeId =: recordTypeId AND Engagement__c IN: engagementIdSet];
    }
}