({
    doInit: function(component, event, helper) {
        helper.getCategory(component, helper);
    },
    onSelectedStaffMemberChanged: function(component, event, helper) {
        var selectedStaffMember = event.getParam("timeRecordPeriodStaffMember");
        component.set("v.selectedTimeRecordPeriodStaffMember", selectedStaffMember);
        component.set("v.suppressSpinners", false);
        helper.onSelectedStaffMemberChanged(component, helper);
    },
    onSelectedDateRangeChanged: function(component, event, helper) {
        var startDate = event.getParam("startDate");
        var endDate = event.getParam("endDate");
        component.set("v.suppressSpinners", false);
        component.set("v.startDate", startDate);
        component.set("v.endDate", endDate);
        component.set("v.dateRange", startDate + ' - ' + endDate);
    },
    onSelectedPeriodChanged: function(component, event, helper) {
        var selectedTimeRecordPeriod = event.getParam("timeRecordPeriod");
        component.set("v.suppressSpinners", false);
        component.set("v.selectedTimeRecordPeriod", selectedTimeRecordPeriod);
    },
    onSuppressLoadingIndicatorFired: function(component, event, helper) {
        var suppress = event.getParam("suppress");
        component.set("v.suppressSpinners", suppress);
    },
    onStaffTimeSheetLoaded: function(component, event, helper) {
        component.set("v.isStaffTimeSheetLoaded", true);
    },
    onStaffTimeSheetTabSelected: function(component, event, helper) {
        helper.staffTimeSheetTabSelected(component, event, helper);
    },
    onStaffTimeSheetWeekLoaded: function(component, event, helper) {
        component.set("v.isStaffTimeSheetWeekLoaded", true);
    },
    onTabStaffClick: function(component, event, helper) {
        helper.setActiveTab(component, "tab-heading-staff", ["tab-heading-billing", "tab-heading-ratesheet"]);
    },
    onTabBillingClick: function(component, event, helper) {
        helper.setActiveTab(component, "tab-heading-billing", ["tab-heading-staff", "tab-heading-ratesheet"]);
    },
    onTabRateSheetClick: function(component, event, helper) {
        helper.setActiveTab(component, "tab-heading-ratesheet", ["tab-heading-billing", "tab-heading-staff"]);
        component.set("v.periodPickerClass", "hidden");
    },
    onTimeRecordPeriodStaffMemberTileListLoaded: function(component, event, helper) {
        component.set("v.isTimeRecordPeriodStaffMemberTileListLoaded", true);
    },
    onTimeRecordPeriodPickerLoaded: function(component, event, helper) {
        component.set("v.isTimeRecordPeriodPickerLoaded", true);
    },
    showSpinner: function(component, event, helper) {
        if (!component.get("v.suppressSpinners"))
            helper.toggleSpinner(component, false);
    },
    hideSpinner: function(component, event, helper) {
        if (!component.get("v.suppressSpinners")) {
            if (component.get("v.isTimeRecordPeriodPickerLoaded") &&
                (!component.get("v.isSupervisor") || component.get("v.isTimeRecordPeriodStaffMemberTileListLoaded")) &&
                (component.get("v.isStaffTimeSheetLoaded") || component.get("v.isStaffTimeSheetWeekLoaded"))) {
                helper.toggleSpinner(component, true);
            }
        } else
            helper.toggleSpinner(component, true);
    }
})