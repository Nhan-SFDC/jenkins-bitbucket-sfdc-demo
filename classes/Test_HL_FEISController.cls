@isTest
private class Test_HL_FEISController {
    @isTest private static void TestBasicFunctionality(){
        //Setup Test Data
        Opportunity__c o = SL_TestSetupUtils.CreateOpp('', 1)[0];
        insert o;
        Opportunity_Approval__c oa = SL_TestSetupUtils.CreateOA('', 1)[0];
        oa.Related_Opportunity__c = o.Id;
        insert oa;
        Test.startTest();
            //Setup Standard Controller
            ApexPages.StandardController sc = new ApexPages.StandardController(oa);
            //Test Controller Properties/Methods
            HL_FEISController con = new HL_FEISController(sc);
            List<Opportunity_Client_Subject__c> csList = con.ClientsSubjects;
            List<Opportunity_Client_Subject__c> cpList = con.Counterparties;
            List<Opportunity_Client_Subject__c> shList = con.ShareholderCompanies;
            con.RefreshClientsSubjects();
            con.RefreshCounterparties();
            con.SaveRecord();
            con.SaveAndReturn();
        Test.stopTest();
        //Confirm there are no errors
        System.assert(!ApexPages.hasMessages(ApexPages.severity.ERROR));
    }
}