<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <content>HL_EngagementOfficeSplitEdit</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Stores the calculated office splits per Engagement</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Engagement_LOB__c</fullName>
        <description>Engagement LOB (Used for Record Sharing)</description>
        <externalId>false</externalId>
        <label>Engagement LOB</label>
        <length>10</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>Related Engagement Record</description>
        <externalId>false</externalId>
        <label>Engagement</label>
        <referenceTo>Engagement__c</referenceTo>
        <relationshipLabel>Engagement Office Splits</relationshipLabel>
        <relationshipName>Engagement_Office_Splits</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Office_Split_Override_Percent__c</fullName>
        <description>Override Office Split Percentage Value</description>
        <externalId>false</externalId>
        <inlineHelpText>If override value is no longer needed, be sure to clear the value and do not set to 0</inlineHelpText>
        <label>Office Split Override Percent</label>
        <precision>7</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Office_Split_Percent__c</fullName>
        <description>Formats to real percent number</description>
        <externalId>false</externalId>
        <formula>BLANKVALUE(Office_Split_Override_Percent__c, Office_Split__c * 100.0)</formula>
        <label>Office Split Percent</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Office_Split__c</fullName>
        <description>Office Revenue Split Percentage</description>
        <externalId>false</externalId>
        <label>Office Split</label>
        <precision>18</precision>
        <required>false</required>
        <scale>6</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Office__c</fullName>
        <description>Office of the Split Calculation</description>
        <externalId>false</externalId>
        <label>Office</label>
        <length>32</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Engagement Office Split</label>
    <nameField>
        <displayFormat>EOS-{00000000}</displayFormat>
        <label>Engagement Office Split Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Engagement Office Splits</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <startsWith>Vowel</startsWith>
</CustomObject>
