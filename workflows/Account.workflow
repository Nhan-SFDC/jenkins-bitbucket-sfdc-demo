<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Ownership_Change</fullName>
        <field>OwnerId</field>
        <lookupValue>sfadmin@hl.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Ownership Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Outlook_Review_Account</fullName>
        <field>Outlook_Review__c</field>
        <literalValue>1</literalValue>
        <name>Update Outlook Review Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change Account Owner to HL Admin</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Outlook Review - Account</fullName>
        <actions>
            <name>Update_Outlook_Review_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.CreatedById</field>
            <operation>equals</operation>
            <value>SFAPIUser</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Houlihan Company</value>
        </criteriaItems>
        <description>Check Outlook Review checkbox when Account created from Outlook</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Ownership Change</fullName>
        <actions>
            <name>Ownership_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
