public class HL_JobType {
    public static String GetJobCode(string jobType){
        List<Job_Type__c> jt = [SELECT Job_Code__c FROM Job_Type__c WHERE Name=:jobType LIMIT 1];
        return jt.size() > 0 ? jt[0].Job_Code__c : '';
    }
    
    public static List<Job_Type__c> GetAll(){
        return [SELECT Name, Job_Code__c FROM Job_Type__c];
    }
    
    //This Map returns the Job Type Name with the Job Code
    public static Map<String, Id> GetJobCodeMap(){
         List<Job_Type__c> jobTypes = GetAll();
         Map<String, Id> mapJobCode = new Map<String, Id>();
         for(Job_Type__c jt : jobTypes)
                mapJobCode.put(jt.Name, jt.Id);
        return mapJobCode;
    }
}