public class HL_EngagementHandler {
    private boolean IsExecuting = false;
    private integer BatchSize = 0;
    public boolean IsTriggerContext{get{ return IsExecuting;}}
    public static Boolean IsBeforeInsertFlag = false;
    public static Boolean IsBeforeUpdateFlag = false;

    public HL_EngagementHandler(boolean executing, integer size){
        IsExecuting = executing;
        BatchSize = size;
    }

    public void OnBeforeInsert(List<Engagement__c> newEngagementList){
        SL_EngagementConversionHelper.PopulateRecordType(newEngagementList);

        if(!IsBeforeInsertFlag){
            IsBeforeInsertFlag = true;

            UpdateCurrencyRate(newEngagementList);
            UpdateRelatedJobType(newEngagementList);
        }
    }

    public void OnBeforeUpdate(List<Engagement__c> oldEngagementList, List<Engagement__c> newEngagementList, Map<Id, Engagement__c> newEngagementListMap){
        if(!IsBeforeUpdateFlag){
            IsBeforeUpdateFlag = true;

            List<Engagement__c> currencyRateUpdatengagementList = new List<Engagement__c>();
            List<Engagement__c> jobTypeUpdatengagementList = new List<Engagement__c>();

            for(Engagement__c old : oldEngagementList)
            {
                Engagement__c newEng = ((Engagement__c)newEngagementListMap.get(old.Id));

                if(old.CurrencyIsoCode <> newEng.CurrencyIsoCode)
                    currencyRateUpdatengagementList.add(newEng);

                if(old.Job_Type__c <> newEng.Job_Type__c)
                    jobTypeUpdatengagementList.add(newEng);
            }

            if(currencyRateUpdatengagementList.size() > 0)
                UpdateCurrencyRate(currencyRateUpdatengagementList);

            if(jobTypeUpdatengagementList.size() > 0)
                UpdateRelatedJobType(jobTypeUpdatengagementList);

            UpdateForComments(newEngagementList);
        }
    }

    private void UpdateCurrencyRate(List<Engagement__c> engagementList){
        Map<String,Id> currencyToRateMap = HL_ExchangeRate.GetCurrencyIsoCodeMap();

        for(Engagement__c e : engagementList){
            if(currencyToRateMap.get(e.CurrencyIsoCode) <> null)
                e.Exchange_Rate__c = currencyToRateMap.get(e.CurrencyIsoCode);
        }
    }

    private void UpdateRelatedJobType(List<Engagement__c> engagementList){
        Map<String, Id> jobTypeToCodeMap = HL_JobType.GetJobCodeMap();

        for(Engagement__c e : engagementList){
            if(!String.isBlank(e.Job_Type__c))
                e.Related_Job_Type__c = jobTypeToCodeMap.get(e.Job_Type__c);
            else
                e.Related_Job_Type__c = null;
        }
    }

    public void OnAfterInsert(Map<Id, Engagement__c> newEngagementMap){
        SL_RelatedOppEgmtHelper.CreateInitialRelatedJoiners(newEngagementMap, 'Engagement_Client_Subject__c');
    }

    public void OnAfterUpdate(Map<Id, Engagement__c> newEngagementMap, Map<Id, Engagement__c> oldEngagementMap){
        Map<Id, Engagement__c> newToProcessMap = new Map<Id, Engagement__c>();
        Map<Id, Engagement__c> oldToProcessMap = new Map<Id, Engagement__c>();
        Map<Id, Engagement__c> newToProcessLOBMap = new Map<Id, Engagement__c>();
        Map<Id, Engagement__c> oldToProcessLOBMap = new Map<Id, Engagement__c>();
        Set<String> excludedStageSet = new Set<String> {'Dead','Hold','Closed'};
        boolean hasRevenueRelatedChange;

        for(Id eID : newEngagementMap.keySet()){
                Engagement__c newE = newEngagementMap.get(eID);
                Engagement__c oldE = oldEngagementMap.get(eID);
                hasRevenueRelatedChange = (newE.Stage__c <> oldE.Stage__c && !excludedStageSet.contains(newE.Stage__c))
                                          || newE.Total_Estimated_Fee__c <> oldE.Total_Estimated_Fee__c || newE.Period_Accrued_Fees__c <> oldE.Period_Accrued_Fees__c;

                if(hasRevenueRelatedChange){
                    newToProcessMap.put(eID, newE);
                    oldToProcessMap.put(eID, oldE);
                }

                if(newE.Line_of_Business__c <> oldE.Line_of_Business__c){
                    newToProcessLOBMap.put(eID, newE);
                    oldToProcessLOBMap.put(eID, oldE);
                }
        }

        if (SL_Statics.ByPassEngagementTrigger == false) {
            SL_Statics.ByPassEngagementTrigger = true;

            //recalculate the percent split - if LOBs changed
            if(newToProcessLOBMap.size() > 0){
                HL_PercentageSplitHandler handler = new HL_PercentageSplitHandler();
                handler.Recalculate(oldToProcessLOBMap, newToProcessLOBMap);
            }

            if(!SL_CheckRecursive.SkipOnConvert){
                //creates/deletes related engagement records based on the client and subject fields
                SL_RelatedOppEgmtHelper.UpdateRelatedJoiners(newEngagementMap, oldEngagementMap, 'Engagement_Client_Subject__c');
                UpdateRevenueAccrual(newToProcessMap,oldToProcessMap);
                ManageDNDSharingRule(newEngagementMap,oldEngagementMap);
            }
        }
        else
            SL_Statics.ByPassEngagementTrigger = false;

        if(!SL_CheckRecursive.SkipOnConvert)
            UpdateRevenueAccrualLocalCalcs(newToProcessMap,oldToProcessMap);
    }

    //Update the Period_Accrued_Fees__c values (FAS jobs) Update the Total_Estimated_Fee__c values (non-FAS jobs)
    private void UpdateRevenueAccrualLocalCalcs(Map<Id, Engagement__c> engagementMap, Map<Id, Engagement__c> oldEngagementMap){
        List<Engagement__c> engagementUpdateList = new List<Engagement__c>();

        for(Engagement__c objEngagement : engagementMap.values()){
            Engagement__c engage = new Engagement__c(id=objEngagement.id);
            if (objEngagement.Line_of_Business__c == 'FAS') {
                // FAS - Calculate Period Accrual Fees based upon Total Estimated Fee
                engage.Total_Accrued_Fees__c = IfNull(objEngagement.Total_Estimated_Fee__c,0) * IfNull(objEngagement.Percentage_of_Completion__c,0) / 100;
                engage.Period_Accrued_Fees__c = IfNull(engage.Total_Accrued_Fees__c,0) - IfNull(objEngagement.Prior_Total_Accrued_Fees__c,0);
                engagementUpdateList.add(engage);
            }
            else {
                // Non-FAS - Calculate Total Estimated Fee based upon Period Accrual Fees
                engage.Total_Accrued_Fees__c = IfNull(objEngagement.Prior_Total_Accrued_Fees__c,0) + IfNull(objEngagement.Period_Accrued_Fees__c,0);
                if (IfNull(objEngagement.Percentage_of_Completion__c,0)>0) {
                    engage.Total_Estimated_Fee__c = (IfNull(engage.Total_Accrued_Fees__c,0) / IfNull(objEngagement.Percentage_of_Completion__c,0)) * 100;
                } else {
                    engage.Total_Estimated_Fee__c = 0;
                }
                engagementUpdateList.add(engage);
            }
        }

        if (engagementUpdateList.size() > 0) {
            update engagementUpdateList;
        }

    }

    //This function will be called after update of the Engagement__c Records.
    private void ManageDNDSharingRule(Map<Id, Engagement__c> newEngagementMap, Map<Id, Engagement__c> oldEngagementMap)
    {
        Set<String> parentIdSet = new Set<String>();//set of engagement id

        //Iterating over Engagement__c to get only those engagement id whose name is updated.
        for(Engagement__c objEngagement : newEngagementMap.values())
        {
            if(objEngagement.Name != oldEngagementMap.get(objEngagement.Id).Name
               && (oldEngagementMap.get(objEngagement.Id).Name.startsWithIgnoreCase('DND')) || objEngagement.Name.startsWithIgnoreCase('DND'))
            {
                parentIdSet.add(objEngagement.Id);
            }
        }

        for(Engagement_Counterparty__c objEC : [SELECT Id FROM Engagement_Counterparty__c WHERE Engagement__c IN : parentIdSet])
            parentIdSet.add(objEC.Id);


        for(Revenue_Accrual__c objEC : [SELECT Id FROM Revenue_Accrual__c WHERE Engagement__c IN : parentIdSet])
            parentIdSet.add(objEC.Id);

        //Calling the method to create the sharing rule according to the Engagement__c names
        if(!parentIdSet.isEmpty())
            SL_ManageSharingRules.DeleteSharesOnChangeParentName(parentIdSet, 'Engagement__c');
    }

    private void UpdateRevenueAccrual(Map<Id, Engagement__c> newEngagementMap, Map<Id, Engagement__c> oldEngagementMap){
        Id revenueAccrualId;
        Revenue_Accrual__c ra;
        Boolean createRevenueAccrual, updateRevenueAccrual, updatePercentComplete, updateTotalEstimatedFee, updateTouch, updatePeriodAccruedFees;
        Set<Id> engagementIdSet = new Set<Id>();
        List<Revenue_Accrual__c> revenueAccrualUpdateList = new List<Revenue_Accrual__c>();
        Map<String, Revenue_Accrual__c> revenueAccrualListByExtIdMap = new Map<String, Revenue_Accrual__c>();
        String externalId, currentMonth, currentYear, currentMonthAndYear;
        List<Monthly_Revenue_Process_Control__c> mrpcList = [SELECT Current_Month__c, Current_Year__c, Current_Month_and_Year__c
                                                             FROM Monthly_Revenue_Process_Control__c
                                                             WHERE IsCurrent__c = true];
        if (mrpcList.size() > 0 ) {
            currentMonth = mrpcList[0].Current_Month__c;
            currentYear = mrpcList[0].Current_Year__c;
            currentMonthAndYear = mrpcList[0].Current_Month_and_Year__c;

            for(Engagement__c objEngagement : newEngagementMap.values()) {
                engagementIdSet.add(objEngagement.Id);
            }

            List<Revenue_Accrual__c> revenueAccrualList = [SELECT External_Id__c, Percent_Complete__c, Total_Estimated_Fee__c, Period_Accrued_Fees__c, Touch__c
                                                           FROM Revenue_Accrual__c
                                                           WHERE Month__c = :currentMonth AND Year__c = :currentYear
                                                           AND Engagement__c IN :engagementIdSet];

            for (Revenue_Accrual__c revAcc : revenueAccrualList) {
                revenueAccrualListByExtIdMap.put(revAcc.External_Id__c, revAcc);
            }

            for(Engagement__c objEngagement : newEngagementMap.values()){
                // Locate current Revenue_Accrual__c record for this Engagement
                externalId = objEngagement.Id;
                externalId = externalId.left(15)+'|'+currentMonthAndYear;

                if ((revenueAccrualListByExtIdMap.get(externalId) == null) && (newEngagementMap.values().size() == 1)) {
                    // Set flag to create Revenue_Accrual__c record
                    createRevenueAccrual = true;
                } else {
                    createRevenueAccrual = false;
                }
                updateRevenueAccrual = false;
                updatePercentComplete = false;
                updateTotalEstimatedFee = false;
                updateTouch = false;
                updatePeriodAccruedFees = false;
                if (objEngagement.Line_of_Business__c == 'FAS') {
                    // FAS line of business
                    // Check if Percentage of Completion or Stage changed
                    if ((objEngagement.Percentage_of_Completion__c != oldEngagementMap.get(objEngagement.Id).Percentage_of_Completion__c) || (objEngagement.Stage__c != oldEngagementMap.get(objEngagement.Id).Stage__c)) {
                        updateRevenueAccrual = true;
                        updatePercentComplete = true;
                    }
                    // Check if Total Estimated Fee changed
                    if (objEngagement.Total_Estimated_Fee__c != oldEngagementMap.get(objEngagement.Id).Total_Estimated_Fee__c) {
                        updateRevenueAccrual = true;
                        updateTotalEstimatedFee = true;
                    }
                }
                if (objEngagement.Line_of_Business__c != 'FAS') {
                    // Non-FAS line of business
                    // Check if Period Accrual Fees changed
                    if (objEngagement.Period_Accrued_Fees__c != oldEngagementMap.get(objEngagement.Id).Period_Accrued_Fees__c) {
                        updateRevenueAccrual = true;
                        updatePeriodAccruedFees = true;
                    }
                }
                // Check if Exclude From Reversal flag changed
                if (objEngagement.Exclude_From_Reversal__c != oldEngagementMap.get(objEngagement.Id).Exclude_From_Reversal__c) {
                    updateRevenueAccrual = true;
                    updateTouch = true;
                }
                if (updateRevenueAccrual == true) {
                    if (createRevenueAccrual == true) {
                        SL_Statics.ByPassRevenueAccrualTrigger = true;
                        HL_RevenueAccrualHandler raHandler = new HL_RevenueAccrualHandler(false,1);
                        if(raHandler.createRecord(objEngagement.Id)) {
                            List<Revenue_Accrual__c>  newrevenueAccrualList = [SELECT External_Id__c, Percent_Complete__c, Total_Estimated_Fee__c, Period_Accrued_Fees__c, Touch__c FROM Revenue_Accrual__c WHERE External_Id__c = :externalId];
                            if (newrevenueAccrualList.size() > 0) {
                                ra = newrevenueAccrualList[0];
                                revenueAccrualListByExtIdMap.put(ra.External_Id__c, ra);

                            } else {
                                updatePercentComplete = false;
                                updateTotalEstimatedFee = false;
                            }
                        } else {
                            updatePercentComplete = false;
                            updateTotalEstimatedFee = false;
                        }
                    } else {
                        ra = revenueAccrualListByExtIdMap.get(externalId);

                        if (ra == null) {
                            updatePercentComplete = false;
                            updateTotalEstimatedFee = false;
                        }
                    }
                    if ((updatePercentComplete == true) || (updateTotalEstimatedFee == true) || (updatePeriodAccruedFees == true)) {
                        ra.Percent_Complete__c = objEngagement.BACKEND_Percentage_of_Completion__c;
                    }
                    if ((updateTotalEstimatedFee == true) || (updatePercentComplete == true)) {
                        ra.Total_Estimated_Fee__c = objEngagement.Total_Estimated_Fee__c;
                    }
                    if (updateTouch == true) {
                        ra.Touch__c = DateTime.Now();
                    }
                    if ((updatePeriodAccruedFees == true) || (updateTotalEstimatedFee == true)) {
                        ra.Period_Accrued_Fees__c = objEngagement.Period_Accrued_Fees__c;
                    }
                    if ((updatePercentComplete == true) || (updateTotalEstimatedFee == true) || (updateTouch == true) || (updatePeriodAccruedFees)) {
                        revenueAccrualUpdateList.add(ra);
                    }
                }
            }

            if (revenueAccrualUpdateList.size() > 0) {
                SL_Statics.ByPassRevenueAccrualTrigger = true;
                update revenueAccrualUpdateList;
            }
        }
    }

    private void UpdateForComments(List<Engagement__c> newEngagementList){
        List<Engagement_Comment__c> commentList = new List<Engagement_Comment__c>();

        for(Engagement__c eng: newEngagementList){
            if(!String.isBlank(eng.Engagement_Comment__c)){
                //If a comment value is found, create a new Engagement_Comments Object
                Engagement_Comment__c engComment = new Engagement_Comment__c();
                engComment.Comment__c = eng.Engagement_Comment__c;
                engComment.Comment_Type__c = 'Internal';
                engComment.Line_of_Business__c = eng.Line_of_Business__c;
                engComment.Engagement__c = eng.Id;
                engComment.CurrencyIsoCode = eng.CurrencyIsoCode;
                commentList.add(engComment);
                FormatLastComment(eng, engComment, true);
                //Remove the value for the new comments field so it is never saved to the object and will be blank the next time Pipeline Manager is displayed
                eng.Engagement_Comment__c = null;
            }
        }

        if(commentList.size() > 0){
            HL_TriggerContextUtility.SetFirstRunFalse();
            insert commentList;
        }
    }

    public static void FormatLastComment(Engagement__c eng, Engagement_Comment__c ec, Boolean openToAll){
        integer newStartLocation,
            currCommentSize = ec.Comment__c.length(),
            newCommentStartSize;
        string newPartialComment, remainderComment,
            firstName = (ec.CreatedBy == null ? UserInfo.getFirstName() : ec.CreatedBy.FirstName),
            lastName = (ec.CreatedBy == null ? UserInfo.getLastName() : ec.CreatedBy.LastName),
            createdDate = (ec.Opp_Comment_Create_Date__c == null ? (ec.CreatedDate == null ? Date.today() : Date.valueOf(ec.CreatedDate)) : ec.Opp_Comment_Create_Date__c).format();

        //If comment was converted from Opp, we need the original creator (same above where we check date)
        if(!String.isBlank(ec.Opp_Comment_Creator__c)){
            string[] nameSplits = ec.Opp_Comment_Creator__c.split(' ');
            if(nameSplits.size() > 1){
                firstName = nameSplits[0];
                lastName = nameSplits[1];
            }
        }

        //Handle if first name is null
        String nameLog = (String.isBlank(firstName) ? '' : firstName.subString(0,1)) + lastName;

        //Update the last comments field as well (Split into 4 parts so the formula field can display the correct values)
        if(openToAll)
        {
            eng.Last_Engagement_Comment_Part_1__c = '';  //Init fields to make sure they are blank from last usage
            eng.Last_Engagement_Comment_Part_2__c = '';
            eng.Last_Engagement_Comment_Part_3__c = '';
            eng.Last_Engagement_Comment_Part_4__c = '';
            eng.Last_Engagement_Comment_Part_1__c = createdDate + ' - ' + nameLog + ': ';

            newCommentStartSize = eng.Last_Engagement_Comment_Part_1__c.length();
            if( currCommentSize + newCommentStartSize <= 255 )
                eng.Last_Engagement_Comment_Part_1__c += ec.Comment__c;
            else
            {
                newPartialComment = ec.Comment__c.substring(0, newCommentStartSize);
                newStartLocation = newPartialComment.length();
                eng.Last_Engagement_Comment_Part_1__c += newPartialComment;
                remainderComment = ec.Comment__c.substring(newStartLocation);
                if( remainderComment.length() <= 255)
                    eng.Last_Engagement_Comment_Part_2__c = remainderComment;
                else
                {
                    newPartialComment = remainderComment.substring(0, 255);
                    eng.Last_Engagement_Comment_Part_2__c = newPartialComment;
                    remainderComment = remainderComment.substring(255);
                    if(remainderComment.length() <= 255)
                        eng.Last_Engagement_Comment_Part_3__c = remainderComment;
                    else
                    {
                        newPartialComment = remainderComment.substring(0, 255);
                        eng.Last_Engagement_Comment_Part_3__c = newPartialComment;
                        remainderComment = remainderComment.substring(255);
                        if(remainderComment.length() <= 255)
                            eng.Last_Engagement_Comment_Part_4__c = remainderComment;
                        else
                            eng.Last_Engagement_Comment_Part_4__c = remainderComment.substring(0, 252) + '...'; //Truncate the reaminder of the comment
                    }
                }
            }
        }

        eng.Last_Engagement_Comment_Any_Part_1__c = '';  //Init fields to make sure they are blank from last usage
        eng.Last_Engagement_Comment_Any_Part_2__c = '';
        eng.Last_Engagement_Comment_Any_Part_3__c = '';
        eng.Last_Engagement_Comment_Any_Part_4__c = '';
        eng.Last_Engagement_Comment_Any_Part_1__c = createdDate + ' - ' + nameLog + ': ';

        newCommentStartSize = eng.Last_Engagement_Comment_Any_Part_1__c.length();
        if( currCommentSize + newCommentStartSize <= 255 )
            eng.Last_Engagement_Comment_Any_Part_1__c += ec.Comment__c;
        else
        {
            newPartialComment = ec.Comment__c.substring(0, newCommentStartSize);
            newStartLocation = newPartialComment.length();
            eng.Last_Engagement_Comment_Any_Part_1__c += newPartialComment;
            remainderComment = ec.Comment__c.substring(newStartLocation);
            if( remainderComment.length() <= 255)
                eng.Last_Engagement_Comment_Any_Part_2__c = remainderComment;
            else
            {
                newPartialComment = remainderComment.substring(0, 255);
                eng.Last_Engagement_Comment_Any_Part_2__c = newPartialComment;
                remainderComment = remainderComment.substring(255);
                if(remainderComment.length() <= 255)
                    eng.Last_Engagement_Comment_Any_Part_3__c = remainderComment;
                else
                {
                    newPartialComment = remainderComment.substring(0, 255);
                    eng.Last_Engagement_Comment_Any_Part_3__c = newPartialComment;
                    remainderComment = remainderComment.substring(255);
                    if(remainderComment.length() <= 255)
                        eng.Last_Engagement_Comment_Any_Part_4__c = remainderComment;
                    else
                        eng.Last_Engagement_Comment_Any_Part_4__c = remainderComment.substring(0, 252) + '...'; //Truncate the reaminder of the comment
                }
            }
        }
    }

    private static decimal IfNull(decimal s1,decimal s2) {
        decimal result = s1;
        if (s1 == null) { result = s2; }
        return result;
    }
}