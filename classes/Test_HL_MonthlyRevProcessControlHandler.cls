@isTest
private class Test_HL_MonthlyRevProcessControlHandler {
    @isTest static void TestUpdateRecord(){
        HL_General__c hg =  new HL_General__c();
        insert hg;

		Account clientAccount = (Account)HL_TestFactory.CreateSObject('Account', false);
		insert clientAccount;

		Account subjectAccount = (Account)HL_TestFactory.CreateSObject('Account', false);
		insert subjectAccount;

		Engagement__c eFAS = new Engagement__c(
                                                Name='Test FAS Engagement',
                                                Engagement_Number__c = '123456',
                                                Client__c = clientAccount.Id,
                                                Subject__c = subjectAccount.Id,
                                                Stage__c = 'Retained',
                                                Line_of_Business__c = 'FAS',
                                                Job_Type__c = 'Fairness',
                                                Primary_Office__c = 'NY'
        									  );
        insert eFAS;

        Monthly_Revenue_Process_Control__c mrpcNew = new Monthly_Revenue_Process_Control__c();
            
        Monthly_Revenue_Process_Control__c mrpc = new Monthly_Revenue_Process_Control__c(IsCurrent__c = TRUE,
		                                                      Current_Month__c = '02', Current_Year__c = '2015');
		insert mrpc;

		Revenue_Accrual__c raNew = new Revenue_Accrual__c(Engagement__c = eFAS.Id,
		                                                      Period_Accrued_Fees__c = 100000, Total_Estimated_Fee__c = 250000);
        insert raNew;

        hg.Revenue_Accrual_Locked__c = true;
        update hg;

        Test.startTest();

        //Test the Unlock and Clearing of the Current Revenue Accrual Fields
        mrpc.Revenue_Accruals_Locked__c = true;
        mrpc.Staff_Summary_Report_Sent__c = true;
        update mrpc;

        Test.stopTest();

        mrpc = [SELECT IsCurrent__c, Revenue_Accruals_Locked__c FROM Monthly_Revenue_Process_Control__c WHERE Id =: mrpc.Id];
        //Verify the current and locked fields are unchecked
        System.assertEquals(false, mrpc.IsCurrent__c);
        System.assertEquals(false, mrpc.Revenue_Accruals_Locked__c);
        //Verify that the next Monthly Revenue Process Control was created
        mrpcNew = [SELECT Current_Month__c, Current_Year__c FROM Monthly_Revenue_Process_Control__c WHERE IsCurrent__c = true];
        System.assertEquals('03', mrpcNew.Current_Month__c);
        System.assertEquals('2015', mrpcNew.Current_Year__c);
        //Verify the Current Revenue Accrual and Period Accrued Fee Fields on the Engagement have been cleared
        //The Current Revenue Accrual value should have been moved to the Latest Revenue Accrual value
        eFAS = [SELECT Current_Revenue_Accrual__c, Latest_Revenue_Accrual__c, Period_Accrued_Fees__c FROM Engagement__c WHERE Id =: eFAS.Id];
        System.assert(String.isBlank(eFAS.Current_Revenue_Accrual__c));
        System.assertEquals(null, eFAS.Period_Accrued_Fees__c);
        System.assertEquals(raNew.Id, eFAS.Latest_Revenue_Accrual__c);
    }
}