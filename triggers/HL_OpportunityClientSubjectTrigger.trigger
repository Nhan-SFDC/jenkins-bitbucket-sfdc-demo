trigger HL_OpportunityClientSubjectTrigger on Opportunity_Client_Subject__c (after insert, after update, after delete) {
    if(Trigger.IsAfter){
        HL_OpportunityClientSubjectHandler.UpdatePublicPrivate(!Trigger.IsDelete ? Trigger.New : Trigger.Old);        
    }
}