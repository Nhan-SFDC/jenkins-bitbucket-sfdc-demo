@isTest
public class HL_TestDataFactory {
	public static List<Account> AccountsList;
    public static List<Contact> HLContactsList;
    public static List<Contact> ExternalContactsList;
    
    @testSetup public static void TestDataSetup() {
    	AccountsList = new List<Account>();
        Account a1 = new Account(Name = 'ABC Company', AccountNumber = '123456');
        AccountsList.add(a1);
        Account a2 = new Account(Name = 'DEF Company', AccountNumber = '987654');
        AccountsList.add(a2);
        insert AccountsList;
        System.assertEquals(2, [SELECT Name FROM Account].size());        
        ExternalContactsList = new List<Contact>();
        for (Integer i = 0; i < 5; i++) {
            Contact c = new Contact(firstName = 'User' + i, lastName = 'Name' + i, RecordTypeId = '012i0000000tEhjAAE', AccountId = AccountsList.get(0).Id);
            ExternalContactsList.add(c);
        }
        for (Integer i = 5; i < 10; i++) {
            Contact c = new Contact(firstName = 'User' + i, lastName = 'Name' + i, RecordTypeId = '012i0000000tEhjAAE', AccountId = AccountsList.get(1).Id);
            ExternalContactsList.add(c);
        }
        insert ExternalContactsList;
        System.assertEquals(10, [SELECT Name FROM Contact].size());       
        HLContactsList = new List<Contact>{ new Contact(firstName = 'HL', lastName = 'Employee', RecordTypeId = '012i0000000tEheAAE', MailingCountry='United States') };
        HLContactsList.add (new Contact(firstName = 'HL', lastName = 'Employee', RecordTypeId = '012i0000000tEheAAE', MailingCountry='France'));        
        insert HLContactsList;
        System.assertEquals(2, [SELECT Name FROM Contact WHERE RecordType.DeveloperName='Houlihan_Employee'].size());    	
    }
    
}