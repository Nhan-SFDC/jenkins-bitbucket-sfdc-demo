public class HL_TimeRecordPeriodStaffMemberController extends HL_TimeRecordControllerBase {
    @AuraEnabled
    public static List<Time_Record_Period_Staff_Member__c> GetByPeriod(string category, Id timeRecordPeriodId){
        if(timeRecordPeriodId == null)
            timeRecordPeriodId = HL_TimeRecordPeriod.GetCurrentPeriod(category).Id;

        return HL_TimeRecordPeriodStaffMember.GetByPeriod(timeRecordPeriodId);
    }
    
    @AuraEnabled
    public static Time_Record_Period_Staff_Member__c GetCurrentRecord(string category){
        return HL_TimeRecordControllerBase.GetCurrentTimeRecordPeriodStaffMemberRecord(category);
    }

    //Gets the User's Category based on the group they are in
    @AuraEnabled
    public static String GetCategory(){
        return HL_Group.GetTimeTrackingGroup();
    }
}