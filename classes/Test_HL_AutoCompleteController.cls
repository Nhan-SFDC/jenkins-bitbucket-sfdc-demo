@isTest
private class Test_HL_AutoCompleteController {
    @isTest private static void TestBasicFunctionality(){
        //Setup Test Data
        Contact c = SL_TestSetupUtils.CreateContact('', 1, SL_TestSetupUtils.ContactType.EXTERNAL_CONTACT)[0];
        Contact e = SL_TestSetupUtils.CreateContact('', 1, SL_TestSetupUtils.ContactType.HOULIHAN_EMPLOYEE)[0];
        c.FirstName = e.FirstName = 'Test';
        c.LastName = e.LastName = 'Test';
        insert c;
        
        Test.startTest();
            //Test Single Term Search
            List<sObject> singleTermResults = HL_AutoCompleteController.GetSuggestions('Contact','External','Test','Title',1);
            //We should get back our test contact
            System.assert(singleTermResults.size() == 1);
            //Test Multi Term Search
            List<sObject> multiTermResults = HL_AutoCompleteController.GetSuggestions('Contact','External','Test Test','Title',1);
            //We should get back our test contact
            System.assert(multiTermResults.size() == 1);
            //Search for Employee as well for full coverage
            singleTermResults = HL_AutoCompleteController.GetSuggestions('Contact','Employee','Test','Title',1);
            //We should get back our test employee
            System.assert(multiTermResults.size() == 1);
        Test.stopTest();
    }
}