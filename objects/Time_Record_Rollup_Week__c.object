<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Time record weekly record time, used by the FR Group</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Comments__c</fullName>
        <externalId>false</externalId>
        <label>Comments</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Engagement__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Related engagement</description>
        <externalId>false</externalId>
        <label>Engagement</label>
        <referenceTo>Engagement__c</referenceTo>
        <relationshipLabel>Time Record Rollup Weeks</relationshipLabel>
        <relationshipName>Time_Record_Rollup_Weeks</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Hours_Worked_Weekday__c</fullName>
        <defaultValue>0.0</defaultValue>
        <description>Hours worked for the week for the project. Used in FR Time Tracking</description>
        <externalId>false</externalId>
        <label>Hours Worked Weekday</label>
        <precision>4</precision>
        <required>true</required>
        <scale>1</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Hours_Worked_Weekend__c</fullName>
        <defaultValue>0.0</defaultValue>
        <description>Hours worked for the week on the weekend. Used for FR Time Tracking</description>
        <externalId>false</externalId>
        <label>Hours Worked Weekend</label>
        <precision>4</precision>
        <required>true</required>
        <scale>1</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Opportunity</label>
        <referenceTo>Opportunity__c</referenceTo>
        <relationshipLabel>Time Record Rollup Weeks</relationshipLabel>
        <relationshipName>Time_Record_Rollup_Weeks</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Project_Name__c</fullName>
        <externalId>false</externalId>
        <formula>BLANKVALUE(BLANKVALUE(Opportunity__r.Name, Engagement__r.Name),Special_Project__r.Name)</formula>
        <label>Project Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Project_Type__c</fullName>
        <description>Presently this is used for reporting display</description>
        <externalId>false</externalId>
        <formula>IF(ISBLANK(Engagement__c), IF(ISBLANK( Opportunity__c), IF(TEXT(Special_Project__r.Location_Group__c) == &apos;Top&apos;, &apos;Special Project&apos;, &apos;*Marketing Initiative&apos;), &apos;Opportunity&apos;), &apos;Engagement&apos;)</formula>
        <label>Project Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Report_Formatted_Weekday_Hours__c</fullName>
        <description>Report formatted hours to remove decimal</description>
        <externalId>false</externalId>
        <formula>IF(Hours_Worked_Weekday__c == 0, null, Hours_Worked_Weekday__c)</formula>
        <label>Report Formatted Weekday Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Report_Formatted_Weekend_Hours__c</fullName>
        <description>Report formatted weekend hours to have zero decimals</description>
        <externalId>false</externalId>
        <formula>IF(Hours_Worked_Weekend__c  == 0.0, null, Hours_Worked_Weekend__c)</formula>
        <label>Report Formatted Weekend Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Report_Sort_Formatted_Project_Name__c</fullName>
        <description>Used to control sorting on the report</description>
        <externalId>false</externalId>
        <formula>CASE(TEXT(Special_Project__r.Location_Group__c),&apos;Top&apos;, &apos;*&apos; + Project_Name__c, &apos;Bottom&apos;, &apos;zz&apos; + Project_Name__c, Project_Name__c)</formula>
        <label>Reporting Sort Formatted Project Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Special_Project__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Special Project</label>
        <referenceTo>Special_Project__c</referenceTo>
        <relationshipLabel>Time Record Rollup Weeks</relationshipLabel>
        <relationshipName>Time_Record_Rollup_Weeks</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Staff_Member_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Time_Record_Period_Staff_Member__r.Contact__r.Contact_Full_Name__c</formula>
        <label>Staff Member Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Time_Record_Period_Staff_Member__c</fullName>
        <externalId>false</externalId>
        <label>Time Record Period Staff Member</label>
        <referenceTo>Time_Record_Period_Staff_Member__c</referenceTo>
        <relationshipLabel>Time Record Rollup Weeks</relationshipLabel>
        <relationshipName>Time_Record_Rollup_Weeks</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Total_Hours_Worked__c</fullName>
        <description>Total Hours Worked (Weekday + Weekend)</description>
        <externalId>false</externalId>
        <formula>Hours_Worked_Weekday__c +  Hours_Worked_Weekend__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Hours Worked</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Year__c</fullName>
        <externalId>false</externalId>
        <formula>Time_Record_Period_Staff_Member__r.Time_Record_Period__r.Year__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Year</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Time Record Rollup Week</label>
    <nameField>
        <displayFormat>TRRW-{00000000}</displayFormat>
        <label>Time Record Rollup Week Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Time Record Rollup Weeks</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
