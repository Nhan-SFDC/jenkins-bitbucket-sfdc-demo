@isTest
private class Test_HL_InternalTeamController {
    @isTest private static void TestBasicFunctionality() {
        //Create Test Data
        List<Contact> cList = SL_TestSetupUtils.CreateContact('', 6, SL_TestSetupUtils.ContactType.HOULIHAN_EMPLOYEE);
        for (Integer index = 0; index < cList.size(); index++)
            cList[index].User__c = UserInfo.getUserId();
        insert cList;
        Opportunity__c opp = SL_TestSetupUtils.CreateOpp('', 1)[0];
        insert opp;
        List<Engagement__c> engagementList = SL_TestSetupUtils.CreateEngagement('', 5);
        insert engagementList;
        List<Staff_Role__c> roleList = new List<Staff_Role__c> {new Staff_Role__c(Name = 'Initiator', Display_Order__c = 1, Opportunity__c = True, CF__c = True), new Staff_Role__c(Name = 'Analyst', Display_Order__c = 2, Opportunity__c = True, CF__c = True), new Staff_Role__c(Name = 'Associate', Display_Order__c = 3, Opportunity__c = True, CF__c = True)};
            insert roleList;
        Opportunity_Approval__c oa = SL_TestSetupUtils.CreateOA('', 1)[0];
        oa.Related_Opportunity__c = opp.id;
        insert oa;
        
        Test.startTest();
        
        //Add some members
        List<Opportunity_Internal_Team__c> oitList = new List<Opportunity_Internal_Team__c> {
            new Opportunity_Internal_Team__c(Opportunity__c = opp.Id, Contact__c = cList[0].Id, Start_Date__c = Date.today(), Staff_Role__c = roleList[0].Id, Office__c = 'LA'),
                new Opportunity_Internal_Team__c(Opportunity__c = opp.Id, Contact__c = cList[0].Id, Start_Date__c = Date.today(), Staff_Role__c = roleList[1].Id, Office__c = 'LA'),
                new Opportunity_Internal_Team__c(Opportunity__c = opp.Id, Contact__c = cList[1].Id, Start_Date__c = Date.today(), Staff_Role__c = roleList[0].Id, Office__c = 'CH')
                };
                    insert oitList;
        
        List<Engagement_Internal_Team__c> eitList = new List<Engagement_Internal_Team__c> {
            new Engagement_Internal_Team__c(Engagement__c = engagementList[0].Id, Contact__c = cList[0].Id, Start_Date__c = Date.today(), Staff_Role__c = roleList[1].Id),
                new Engagement_Internal_Team__c(Engagement__c = engagementList[1].Id, Contact__c = cList[0].Id, Start_Date__c = Date.today(), Staff_Role__c = roleList[2].Id),
                new Engagement_Internal_Team__c(Engagement__c = engagementList[2].Id, Contact__c = cList[1].Id, Start_Date__c = Date.today(), Staff_Role__c = roleList[0].Id),
                new Engagement_Internal_Team__c(Engagement__c = engagementList[1].Id, Contact__c = cList[2].Id, Start_Date__c = Date.today(), Staff_Role__c = roleList[1].Id)
                };
                    insert eitList;
        
        eitList[0].Staff_Role__c = roleList[0].id;
        eitList[1].End_Date__c = Date.today();
        eitList[2].Staff_Role__c = roleList[1].id;
        eitList[3].Staff_Role__c = roleList[2].id;
        eitList.add(new Engagement_Internal_Team__c(Engagement__c = engagementList[3].Id, Contact__c = cList[2].Id, Start_Date__c = Date.today(), Staff_Role__c = roleList[1].Id));
        eitList.add(new Engagement_Internal_Team__c(Engagement__c = engagementList[4].Id, Contact__c = cList[1].Id, Start_Date__c = Date.today(), Staff_Role__c = roleList[1].Id));
        
        upsert eitList;
        
        ApexPages.currentPage().getParameters().put('Id', opp.Id);
        HL_InternalTeamController itc = new HL_InternalTeamController();
        //Test Properties
        
        List<HL_InternalTeamRecord> team = itc.TeamRecords;
        String customFilter = itc.CustomFilter;
        //Test Page references
        itc.BackToEntity();
        itc.EditClicked();
        //Test Actions
        itc.SelectedStaffId = cList[2].id;
        itc.StaffMemberToAdd.RoleAssignments[0].Assigned = True;
        itc.SaveTeamEdits();
        ApexPages.currentPage().getParameters().put('Id', engagementList[0].Id);
        HL_InternalTeamController engITC = new HL_InternalTeamController();
        //Test Properties
        List<HL_InternalTeamRecord> engTeam = engITC.TeamRecords;
        //Test Page references
        engITC.BackToEntity();
        engITC.EditClicked();
        //Test Actions
        engITC.SelectedStaffId = cList[2].id;
        engITC.StaffMemberToAdd.RoleAssignments[0].Assigned = True;
        engITC.SaveTeamEdits();
        Contact cr = engITC.TeamRecords[0].ContactRecord;
        engITC.TeamRecords[0].RoleAssignments[0].Assigned = False;
        engITC.SaveTeamEdits();
        
        Test.stopTest();
        
        System.assertEquals(itc.TeamRecords.size(), 3);
        System.assertEquals(engITC.TeamRecords.size(), 1);
    }
}