global without sharing class HL_Activity implements Comparable{
    //Riva Sync Spacing Divider
    public static final string SYNC_SPACING = ('\n'.repeat(5));
    //Riva Sync Text Divider Section Heading
    public static final string SYNC_DIVIDER = 'External Attendees (will NOT automatically be included on Invite)';
    public String FormattedStart {get{
        return Activity.StartDateTime.format();
    }}
    public String FormattedEnd {get{
        return Activity.EndDateTime.format();
    }}
    public Event Activity {get; set;}
    public Id LinkId {get; set;}
    public String PrimaryAttendee {get; set;}
    public Boolean ViewAccess {get; set;}
    public Boolean ModifyAccess {get; set;}
    public String PrimaryContact {get; set;}
    public Id PrimaryContactId {get; set;}

    global Integer compareTo(Object o){
        HL_Activity ha = (HL_Activity)o;
        return (ha.Activity.StartDateTime > Activity.StartDateTime ? 1 : -1);
    }

}