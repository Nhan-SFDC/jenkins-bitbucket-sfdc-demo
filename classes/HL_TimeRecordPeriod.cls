public without sharing class HL_TimeRecordPeriod {

    public static Time_Record_Period__c GetByDate(string category, Date periodDate){
        if(String.isBlank(category))
            category = HL_Group.GetTimeTrackingGroup();

        List<Time_Record_Period__c> trpList = [SELECT Start_Date__c, End_Date__c, Is_Locked__c FROM Time_Record_Period__c WHERE Start_Date__c <=: periodDate AND End_Date__c >=: periodDate AND Period_Category__c =: category];

        if(trpList.size() == 0){
            Date start = category == 'FR' ? GetClosestMonday(periodDate) : GetClosestSunday(periodDate);
            trpList.add(new Time_Record_Period__c(Start_Date__c = start, End_Date__c = start.addDays(6), Period_Category__c = category));
            insert trpList;
        }

        //For FR We Still Want to Create the Current Week Record (Above), But Return the Previous Week Record as the Default
        if(category == 'FR'){
            periodDate = periodDate.addDays(-7);
            List<Time_Record_Period__c> trpPreviousList = [SELECT Start_Date__c, End_Date__c, Is_Locked__c FROM Time_Record_Period__c WHERE Start_Date__c <=: periodDate AND End_Date__c >=: periodDate AND Period_Category__c =: category];
            if(trpPreviousList.size() > 0)
                trpList = trpPreviousList;
        }

        return trpList[0];
    }

    public static Time_Record_Period__c GetCurrentPeriod(string category){
        return GetByDate(category, Date.today());
    }

    public static List<Time_Record_Period__c> GetPeriods(string category){
        List<Time_Record_Period__c> trpList = [SELECT Start_Date__c, End_Date__c, Is_Locked__c FROM Time_Record_Period__c WHERE Period_Category__c =: category ORDER BY Start_Date__c DESC];
        if(trpList.size() == 0)
            trpList.add(GetCurrentPeriod(category));
        return trpList;
    }

    public static Time_Record_Period__c GetPeriod(Id id){
        return [SELECT Start_Date__c, End_Date__c, Is_Locked__c FROM Time_Record_Period__c WHERE Id=:id];
    }

    private static Date GetClosestMonday(Date startDate){
        Date monday = Date.newInstance(1900, 1, 1);
        while(Math.mod(monday.daysBetween(startDate), 7) != 0)
            startDate = startDate.addDays(-1);
        return startDate;
    }

    private static Date GetClosestSunday(Date startDate){
        Date sunday = Date.newInstance(1900, 1, 7);
        while(Math.mod(sunday.daysBetween(startDate), 7) != 0)
            startDate = startDate.addDays(-1);
        return startDate;
    }
}