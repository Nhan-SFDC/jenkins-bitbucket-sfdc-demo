public with sharing class HL_OpportunityHandler {
    public static boolean ExecuteOnce = true;
    public static boolean IsTest = false;
    private boolean IsExecuting = false;
    private integer BatchSize = 0;

    public HL_OpportunityHandler(boolean executing, integer size)
    {
        IsExecuting = executing;
        BatchSize = size;
    }

    public void OnBeforeInsert(List<Opportunity__c> opps){
        User UserOppData = [SELECT Office__c, DefaultCurrencyIsoCode FROM User WHERE id = :UserInfo.getUserId()];
        Map<String,Id> currencyToRateMap = HL_ExchangeRate.GetCurrencyIsoCodeMap();
        //Map of Id to the Code
        Map<String, Id> jobTypeToCodeMap = HL_JobType.GetJobCodeMap();
        for(Opportunity__c Opp : opps){
            if(Opp.Primary_Office__c == NULL){
                Opp.Primary_Office__c = UserOppData.Office__c;
            }
            if(UserOppData.DefaultCurrencyIsoCode == 'USD')
                Opp.Total_Debt_Currency__c = 'USD - U.S. Dollar';
            if(UserOppData.DefaultCurrencyIsoCode == 'AUD')
                Opp.Total_Debt_Currency__c = 'AUD - Australian Dollar';
            if(UserOppData.DefaultCurrencyIsoCode == 'EUR')
                Opp.Total_Debt_Currency__c = 'EUR - Euro';
            if(UserOppData.DefaultCurrencyIsoCode == 'GBP')
                Opp.Total_Debt_Currency__c = 'GBP - British Pound';
            if(UserOppData.DefaultCurrencyIsoCode == 'HKD')
                Opp.Total_Debt_Currency__c = 'HKD - Hong Kong Dollar';
            if(UserOppData.DefaultCurrencyIsoCode == 'JPY')
                Opp.Total_Debt_Currency__c = 'JPY - Japanese Yen';
            if(currencyToRateMap.get(Opp.CurrencyIsoCode) <> null)
                Opp.Exchange_Rate__c = currencyToRateMap.get(Opp.CurrencyIsoCode);
            if(!String.isBlank(Opp.Job_Type__c))
                    Opp.Related_Job_Type__c = jobTypeToCodeMap.get(Opp.Job_Type__c);
        }
    }

    public void OnBeforeUpdate(List<Opportunity__c> oldRecords, Map<Id, Opportunity__c> newRecordsMap){
        //Store the Changed Opportunity DND Records to Update the Related Engagements
        Map<Id, String> oppDNDMap = new Map<Id, String>();
        //Store the Engagements to Update the DND Status On
        List<Engagement__c> engagementUpdateList = new List<Engagement__c>();
        //Store the Changed Exchange Rate Opportunities
        Map<String,Id> currencyToRateMap = HL_ExchangeRate.GetCurrencyIsoCodeMap();
        //Map of Id to the Code
        Map<String, Id> jobTypeToCodeMap = HL_JobType.GetJobCodeMap();
        for(Opportunity__c ctOld : oldRecords)
        {
            Opportunity__c newOpp = ((Opportunity__c)newRecordsMap.get(ctOld.Id));
            if(ctOld.DND_Status__c <> newOpp.DND_Status__c)
                oppDNDMap.put(ctOld.Id, newRecordsMap.get(ctOld.Id).DND_Status__c);
            if(ctOld.CurrencyIsoCode <> newOpp.CurrencyIsoCode){
                if(currencyToRateMap.get(newOpp.CurrencyIsoCode) <> null)
                    newOpp.Exchange_Rate__c = currencyToRateMap.get(newOpp.CurrencyIsoCode);
            }
            if(!String.isBlank(newOpp.Job_Type__c))
                newOpp.Related_Job_Type__c = jobTypeToCodeMap.get(newOpp.Job_Type__c);
            else
                newOpp.Related_Job_Type__c = null;
        }

        if(oppDNDMap.size() > 0)
        {
            for(Engagement__c e : [SELECT Name, Name1__c, Eng_Name_Store__c, Opportunity__c, DND_Status__c
                                   FROM Engagement__c
                                   WHERE Opportunity__c IN : oppDNDMap.keySet()]){
                e.DND_Status__c = oppDNDMap.get(e.Opportunity__c);

                if(e.DND_Status__c == 'Add Requested')
                    e.Eng_Name_Store__c = e.Name;

                if(e.DND_Status__c == 'APPROVED')
                    e.Name = 'DND - ' + e.Id;
                else{
                    if(e.DND_Status__c != 'Removal Requested')
                        e.Name = e.Eng_Name_Store__c;
                }
                engagementUpdateList.add(e);
            }
            if(engagementUpdateList.size() > 0)
                update engagementUpdateList;
        }

        //Update Comments
        UpdateForComments(newRecordsMap.values());
    }

    public void OnAfterInsert(Map<Id, Opportunity__c> newOpportunityMap)
    {
        SL_RelatedOppEgmtHelper.CreateInitialRelatedJoiners(newOpportunityMap, 'Opportunity_Client_Subject__c');
    }

    public void OnAfterUpdate(Map<Id, Opportunity__c> newOpportunityMap, Map<Id, Opportunity__c> oldOpportunityMap)
    {
        if(ExecuteOnce){
            SL_RelatedOppEgmtHelper.UpdateRelatedJoiners(newOpportunityMap, oldOpportunityMap, 'Opportunity_Client_Subject__c');
            ExecuteOnce = false;
        }

        ManageDNDSharingRule(newOpportunityMap, oldOpportunityMap);
    }

    private void ManageDNDSharingRule(Map<Id, Opportunity__c> newOpportunityMap, Map<Id, Opportunity__c> oldOpportunityMap)
    {
        if(!SL_CheckRecursive.skipOnConvert){
            Set<String> opportunityIdSet = new Set<String>();

            //Iterating over Opportunity__c to get only those Opportunity id whose name is updated.
            for(Opportunity__c objOpportunity : newOpportunityMap.values())
            {
                if(objOpportunity.Name == null || oldOpportunityMap.get(objOpportunity.Id).Name == null) continue;
                if(objOpportunity.Name != oldOpportunityMap.get(objOpportunity.Id).Name
                   && (oldOpportunityMap.get(objOpportunity.Id).Name.startsWithIgnoreCase('DND')) || objOpportunity.Name.startsWithIgnoreCase('DND'))
                {
                    opportunityIdSet.add(objOpportunity.Id);
                }
            }

            //Calling the method to create the sharing rule according to the Opportunity__c names
            if(!opportunityIdSet.isEmpty())
                SL_ManageSharingRules.deleteSharesOnChangeParentName(opportunityIdSet, 'Opportunity__c');
      }
    }


    private void UpdateForComments(List<Opportunity__c> opps){
        List<Opportunity_Comment__c> commentList = new List<Opportunity_Comment__c>();

        for(Opportunity__c opp: opps){

            //Check to see if the new comment has a value or not
            if(opp.Opportunity_Comments__c != null && opp.Opportunity_Comments__c != '')
            {
                //If a value is found, create a new Engagement_Comments Object
                Opportunity_Comment__c oppComment = new Opportunity_Comment__c();

                oppComment.Comment__c = opp.Opportunity_Comments__c;
                oppComment.Comment_Type__c = 'Internal';
                oppComment.Line_of_Business__c = opp.Line_of_Business__c;
                oppComment.Related_Opportunity__c = opp.Id;
                oppComment.CurrencyIsoCode = opp.CurrencyIsoCode;
                commentList.add(oppComment);
                FormatLastComment(opp, oppComment, true);
                //Remove the value for the new comments field so it is never saved to the object and will be blank the next time Pipeline Manager is displayed
                opp.Opportunity_Comments__c = null;
            }
        }

        if(commentList.size() > 0){
            HL_TriggerContextUtility.SetFirstRunFalse();
            insert commentList;
        }
    }

    public static void FormatLastComment(Opportunity__c opp, Opportunity_Comment__c oc, boolean openToAll){
        integer newStartLocation,
                currCommentSize = oc.Comment__c.length(),
                newCommentStartSize;
        string newPartialComment, remainderComment,
               firstName = oc.CreatedBy == null ? UserInfo.getFirstName() : oc.CreatedBy.FirstName,
               lastName = oc.CreatedBy == null ? UserInfo.getLastName() : oc.CreatedBy.LastName,
               createdDate = (oc.CreatedDate == null ? Date.today() : Date.valueOf(oc.CreatedDate)).format();
        string nameLog = (String.isBlank(firstName) ? '' : firstName.subString(0,1)) + lastName;

        //Update the last comments field as well (Split into 4 parts so the formula field can display the correct values)
        if(openToAll)
        {
            opp.Last_Opportunity_Comment_Part_1__c = '';  //Init fields to make sure they are blank from last usage
            opp.Last_Opportunity_Comment_Part_2__c = '';
            opp.Last_Opportunity_Comment_Part_3__c = '';
            opp.Last_Opportunity_Comment_Part_4__c = '';
            opp.Last_Opportunity_Comment_Part_1__c = createdDate + ' - ' + nameLog + ': ';
            newCommentStartSize = opp.Last_Opportunity_Comment_Part_1__c.length();
            if( currCommentSize + newCommentStartSize <= 255 )
                opp.Last_Opportunity_Comment_Part_1__c += oc.Comment__c;
            else
            {
                newPartialComment = oc.Comment__c.substring(0, 255 - newCommentStartSize);
                newStartLocation = newPartialComment.length();
                opp.Last_Opportunity_Comment_Part_1__c += newPartialComment;
                remainderComment = oc.Comment__c.substring(newStartLocation);
                if( remainderComment.length() <= 255)
                    opp.Last_Opportunity_Comment_Part_2__c = remainderComment;
                else
                {
                    newPartialComment = remainderComment.substring(0, 255);
                    opp.Last_Opportunity_Comment_Part_2__c = newPartialComment;
                    remainderComment = remainderComment.substring(255);
                    if(remainderComment.length() <= 255)
                        opp.Last_Opportunity_Comment_Part_3__c = remainderComment;
                    else
                    {
                        newPartialComment = remainderComment.substring(0, 255);
                        opp.Last_Opportunity_Comment_Part_3__c = newPartialComment;
                        remainderComment = remainderComment.substring(255);
                        if(remainderComment.length() <= 255)
                            opp.Last_Opportunity_Comment_Part_4__c = remainderComment;
                        else
                            opp.Last_Opportunity_Comment_Part_4__c = remainderComment.substring(0, 252) + '...'; //Truncate the reaminder of the comment
                    }
                }
            }
        }

        opp.Last_Opportunity_Comment_Any_Part_1__c = '';  //Init fields to make sure they are blank from last usage
        opp.Last_Opportunity_Comment_Any_Part_2__c = '';
        opp.Last_Opportunity_Comment_Any_Part_3__c = '';
        opp.Last_Opportunity_Comment_Any_Part_4__c = '';
        opp.Last_Opportunity_Comment_Any_Part_1__c = createdDate + ' - ' + nameLog + ': ';
        newCommentStartSize = opp.Last_Opportunity_Comment_Any_Part_1__c.length();
        if( currCommentSize + newCommentStartSize <= 255 )
             opp.Last_Opportunity_Comment_Any_Part_1__c += oc.Comment__c;
        else
        {
            newPartialComment = oc.Comment__c.substring(0, 255 - newCommentStartSize);
            newStartLocation = newPartialComment.length();
            opp.Last_Opportunity_Comment_Any_Part_1__c += newPartialComment;
            remainderComment = oc.Comment__c.substring(newStartLocation);
            if( remainderComment.length() <= 255)
                 opp.Last_Opportunity_Comment_Any_Part_2__c = remainderComment;
            else
            {
                newPartialComment = remainderComment.substring(0, 255);
                opp.Last_Opportunity_Comment_Any_Part_2__c = newPartialComment;
                remainderComment = remainderComment.substring(255);
                if(remainderComment.length() <= 255)
                    opp.Last_Opportunity_Comment_Any_Part_3__c = remainderComment;
                else
                {
                    newPartialComment = remainderComment.substring(0, 255);
                    opp.Last_Opportunity_Comment_Any_Part_3__c = newPartialComment;
                    remainderComment = remainderComment.substring(255);
                    if(remainderComment.length() <= 255)
                         opp.Last_Opportunity_Comment_Any_Part_4__c = remainderComment;
                    else
                        opp.Last_Opportunity_Comment_Any_Part_4__c = remainderComment.substring(0, 252) + '...'; //Truncate the reaminder of the comment
                }
            }
        }
    }
}