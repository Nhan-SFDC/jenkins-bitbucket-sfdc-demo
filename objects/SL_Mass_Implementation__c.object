<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Main Object - main settings for Mass Add/Edit component</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>ExtID__c</fullName>
        <externalId>true</externalId>
        <label>ExtID</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Get_Element_from_List_Tab__c</fullName>
        <description>This value is used for &apos;Add Multi&apos; on List Search Code</description>
        <externalId>false</externalId>
        <inlineHelpText>This value is used for &apos;Add Multi&apos; on List Search Code</inlineHelpText>
        <label>Get Element From List Tab Title</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Lookup_Title_Get_List_Page__c</fullName>
        <externalId>false</externalId>
        <label>Lookup Title Get List Page</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Production_SF_ID__c</fullName>
        <externalId>true</externalId>
        <label>Production SF ID</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Row_Qty_Values__c</fullName>
        <defaultValue>&apos;10,20,30,50,100&apos;</defaultValue>
        <externalId>false</externalId>
        <label>Row Qty Values</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Search_Object_Title__c</fullName>
        <externalId>false</externalId>
        <label>Search Object Title</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Search_Page_Title__c</fullName>
        <description>This value is used for &apos;Add Multi&apos; on List Search Code</description>
        <externalId>false</externalId>
        <inlineHelpText>This value is used for &apos;Add Multi&apos; on List Search Code</inlineHelpText>
        <label>Search Page Title</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Show_Views_on_MassAdd_Page__c</fullName>
        <defaultValue>true</defaultValue>
        <description>This param allows hide/show View&apos;s line above Search Result table</description>
        <externalId>false</externalId>
        <label>Show Views on MassAdd Page</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Views_on_MassEdit_Page__c</fullName>
        <defaultValue>true</defaultValue>
        <description>This param allows hide/show View&apos;s line above Record Edit table</description>
        <externalId>false</externalId>
        <label>Show Views on MassEdit Page</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Use_Step2_Multi_Add_Form__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Use Step2 Multi Add Form</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Using_Exists_Impl__c</fullName>
        <externalId>false</externalId>
        <label>Using Exists Impl</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>parentObjectName__c</fullName>
        <externalId>false</externalId>
        <label>Parent Object Name</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>searchObjectName__c</fullName>
        <externalId>false</externalId>
        <label>Search Object Name</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>targetObjectName__c</fullName>
        <externalId>false</externalId>
        <label>Target Object Name</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>targetObjectParentFieldName__c</fullName>
        <externalId>false</externalId>
        <label>Target Object Parent Field Name</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>targetObjectSearchFieldName__c</fullName>
        <externalId>false</externalId>
        <label>Target Object Search Field Name</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>ME Core: Implementation</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Implementations</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <webLinks>
        <fullName>SL_LIB16_Instructions_For_Buttons_Creation</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>Instructions For Buttons Creation</masterLabel>
        <openType>sidebar</openType>
        <protected>false</protected>
        <url>www.google.com</url>
    </webLinks>
    <webLinks>
        <fullName>Setup_Get_Lists</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>Setup Get Lists</masterLabel>
        <openType>sidebar</openType>
        <protected>false</protected>
        <url>www.google.com</url>
    </webLinks>
    <webLinks>
        <fullName>Setup_Search_Filter</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>Setup Search Filter</masterLabel>
        <openType>sidebar</openType>
        <protected>false</protected>
        <url>/apex/SL_LIB16_FilterFieldSetup?id={!SL_Mass_Implementation__c.Id}&amp;type=Search</url>
    </webLinks>
    <webLinks>
        <fullName>Setup_Target_Filter</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>Setup Target Filter</masterLabel>
        <openType>sidebar</openType>
        <protected>false</protected>
        <url>/apex/SL_LIB16_FilterFieldSetup?id={!SL_Mass_Implementation__c.Id}&amp;type=Target</url>
    </webLinks>
</CustomObject>
