<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <comment>Edit Overridden</comment>
        <content>HL_GiftEdit</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <content>SL_GiftPreApproval</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>SEC Tracking of gifts to external Contacts</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Approval_Comment__c</fullName>
        <description>Approver Comment</description>
        <externalId>false</externalId>
        <inlineHelpText>Approver Comment</inlineHelpText>
        <label>Approval Comment</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Approval_Number__c</fullName>
        <description>Approval number for external vendor</description>
        <displayFormat>AP{0000}</displayFormat>
        <externalId>false</externalId>
        <inlineHelpText>This is auto-generated in the format AP0000</inlineHelpText>
        <label>Approval Number</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>AutoNumber</type>
    </fields>
    <fields>
        <fullName>Approve_Date__c</fullName>
        <description>Date gift was approved/denied</description>
        <externalId>false</externalId>
        <inlineHelpText>Date gift was approved/denied</inlineHelpText>
        <label>Approve Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Approved_By__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Approver</description>
        <externalId>false</externalId>
        <inlineHelpText>Approver</inlineHelpText>
        <label>Approved By</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Approved_By</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Approved__c</fullName>
        <description>Whether the gift is approved</description>
        <externalId>false</externalId>
        <inlineHelpText>Whether the gift is approved</inlineHelpText>
        <label>Approved</label>
        <picklist>
            <picklistValues>
                <fullName>Pending</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Approved</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Denied</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Batch_Guid__c</fullName>
        <description>Batch Guid used to group the gifts back to their original submission batch</description>
        <externalId>false</externalId>
        <label>Batch Guid</label>
        <length>36</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Business_Unit__c</fullName>
        <description>Line of Business</description>
        <externalId>false</externalId>
        <inlineHelpText>Line of Business to be billed</inlineHelpText>
        <label>Business Unit</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Client_Billable__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Is the Client Billable?</description>
        <externalId>false</externalId>
        <inlineHelpText>Is the Client Billable for this gift?</inlineHelpText>
        <label>Is Client Billable?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Comment__c</fullName>
        <description>Comments for the gift</description>
        <externalId>false</externalId>
        <inlineHelpText>Comments for the gift</inlineHelpText>
        <label>Comment</label>
        <length>2000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Currency__c</fullName>
        <description>Currency that the gift will be purchased in.</description>
        <externalId>false</externalId>
        <inlineHelpText>Currency that the gift will be purchased in.</inlineHelpText>
        <label>Currency</label>
        <picklist>
            <picklistValues>
                <fullName>U.S. Dollar</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Euro (not in France)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Euro (in France)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>British Pound</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Hong Kong Dollar</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Australian Dollar</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Desired_Date__c</fullName>
        <defaultValue>Today()</defaultValue>
        <description>Date the gift will be purchased by.</description>
        <externalId>false</externalId>
        <inlineHelpText>Date the gift will be purchased by.</inlineHelpText>
        <label>Desired Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ER_Num__c</fullName>
        <description>Extensity ER number</description>
        <externalId>false</externalId>
        <inlineHelpText>Extensity ER number</inlineHelpText>
        <label>ER Num</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ER_Submit_Date__c</fullName>
        <description>Date of the ER number</description>
        <externalId>false</externalId>
        <inlineHelpText>Date of the ER number</inlineHelpText>
        <label>ER Submit Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Engagement__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Engagement</label>
        <referenceTo>Engagement__c</referenceTo>
        <relationshipName>Gifts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Gift_Next_Year_Value__c</fullName>
        <defaultValue>0.0</defaultValue>
        <description>Next Year Gift Value</description>
        <externalId>false</externalId>
        <label>Gift Next Year Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Gift_Type__c</fullName>
        <description>Type of gift</description>
        <externalId>false</externalId>
        <inlineHelpText>Type of gift</inlineHelpText>
        <label>Gift Type</label>
        <picklist>
            <picklistValues>
                <fullName>Charitable Contribution</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Closing Party Client Gifts</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Gifts: Customers</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Gifts: Customers Europe</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Gift_Value_Distributed__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Field for Gift value distribution</description>
        <externalId>false</externalId>
        <inlineHelpText>Gift value will be distributed between recipients if checked.</inlineHelpText>
        <label>Divide Gift Value Among All Recipients?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Gift_Value__c</fullName>
        <description>The gift&apos;s market value in the specified currency excluding the cost of tax, shipping, and engraving.</description>
        <externalId>false</externalId>
        <inlineHelpText>The gift&apos;s market value in the specified currency excluding the cost of tax, shipping, and engraving.</inlineHelpText>
        <label>Gift Value</label>
        <precision>6</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>HL_Relationship__c</fullName>
        <externalId>false</externalId>
        <label>HL Relationship</label>
        <picklist>
            <picklistValues>
                <fullName>Client</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Prospect</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Industry__c</fullName>
        <description>Industry to bill against</description>
        <externalId>false</externalId>
        <inlineHelpText>Industry to bill against</inlineHelpText>
        <label>Industry Group</label>
        <picklist>
            <picklistValues>
                <fullName>ADG - Aerospace Defense and Government</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BUS - Business Services</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CFR - Consumer Food  &amp; Retail</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CORP - Corporate</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>FIG - Financial Institutions</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>FT - Financial Technology</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>GEN - General</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>HC - Healthcare</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>IFA - Illiquid Financial Assets</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>IG - Industrials</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>M&amp;T - Media &amp; Telecom</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NRG - Energy</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>OTHER - Other</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>POW - Power &amp; Utilities</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>RE - Real Estate Lodging and Leisure</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>SOV - Sovereigns</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TECH - Technology</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TRANS - Transportation &amp; Logistics</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Pre_Approval__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Is this a Pre-Approval?</description>
        <externalId>false</externalId>
        <inlineHelpText>Is this a Pre-Approval?</inlineHelpText>
        <label>Is Pre-Approval?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <description>Product line to bill against</description>
        <externalId>false</externalId>
        <inlineHelpText>Product line to bill against</inlineHelpText>
        <label>Product Line</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Reason_For_Gift__c</fullName>
        <description>Reason for the gift</description>
        <externalId>false</externalId>
        <inlineHelpText>Reason for the gift</inlineHelpText>
        <label>Reason For Gift</label>
        <length>2000</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Recipient_Company_Name__c</fullName>
        <description>Recipient Company (Account) Name</description>
        <externalId>false</externalId>
        <formula>Recipient__r.Account.Name</formula>
        <label>Recipient Company Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Recipient_Name__c</fullName>
        <description>Recipient Name from Contact</description>
        <externalId>false</externalId>
        <formula>Recipient__r.Contact_Full_Name__c</formula>
        <label>Recipient Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Recipient__c</fullName>
        <description>External Contact to receive gift</description>
        <externalId>false</externalId>
        <inlineHelpText>External Contact to receive gift</inlineHelpText>
        <label>Recipient For Gift</label>
        <referenceTo>Contact</referenceTo>
        <relationshipName>Gifts</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Reimbursement__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Reimbursement requested (possible dupe with Client Billable?)</description>
        <externalId>false</externalId>
        <inlineHelpText>Reimbursement requested (possible dupe with Client Billable?)</inlineHelpText>
        <label>Reimbursement</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Submitted_By_Name__c</fullName>
        <description>Submitter Name</description>
        <externalId>false</externalId>
        <formula>Submitted_By__r.Contact_Full_Name__c</formula>
        <label>Submitted By Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Submitted_By__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Submitted By Lookup to Contact</description>
        <externalId>false</externalId>
        <label>Submitted By</label>
        <referenceTo>Contact</referenceTo>
        <relationshipName>Gift_Requests</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Submitted_For_Name__c</fullName>
        <description>Submitted for name</description>
        <externalId>false</externalId>
        <formula>Submitted_For__r.Contact_Full_Name__c</formula>
        <label>Submitted For Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Submitted_For__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>HL Staff member giving gift</description>
        <externalId>false</externalId>
        <inlineHelpText>HL Staff member giving gift</inlineHelpText>
        <label>Submitted For</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Must be submitted for a Houlihan Employee.</errorMessage>
            <filterItems>
                <field>Contact.RecordTypeId</field>
                <operation>equals</operation>
                <value>Houlihan Employee</value>
            </filterItems>
            <infoMessage>Shows only Houlihan Employees.</infoMessage>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Contact</referenceTo>
        <relationshipName>Submitted_For</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Vendor__c</fullName>
        <description>Vendor to be used for gift.</description>
        <externalId>false</externalId>
        <inlineHelpText>Vendor to be used for gift.</inlineHelpText>
        <label>Vendor</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>fApproval_Number__c</fullName>
        <externalId>false</externalId>
        <formula>IF( ISPICKVAL(Approved__c, &apos;Pending&apos;), &apos;Pending&apos;, IF( ISPICKVAL(Approved__c, &apos;Denied&apos;), &apos;Denied&apos;, Approval_Number__c))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Approval Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Gift Request</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Approval_Number__c</columns>
        <columns>Submitted_For__c</columns>
        <columns>Recipient__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Gift Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Gift Requests</pluralLabel>
    <searchLayouts>
        <excludedStandardButtons>New</excludedStandardButtons>
        <searchResultsAdditionalFields>Recipient__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Submitted_For__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Desired_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Gift_Value__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>DesiredDateFieldValidation</fullName>
        <active>true</active>
        <errorConditionFormula>Desired_Date__c - TODAY() &gt;= 365</errorConditionFormula>
        <errorMessage>Desired Date Should not be greater than one Year.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>DesiredDateValidation</fullName>
        <active>false</active>
        <description>Please enter a date that is no more than one year from today. Dates that are more than a year in the future are not valid.</description>
        <errorConditionFormula>YEAR(Desired_Date__c) &gt;  (YEAR(TODAY()) + 1)</errorConditionFormula>
        <errorMessage>Please enter a date that is no more than one year from today. Dates that are more than a year in the future are not valid.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>GiftValueValidation</fullName>
        <active>false</active>
        <errorConditionFormula>Gift_Value__c &lt;= 0</errorConditionFormula>
        <errorMessage>Gift Value Can not be negative or 0.</errorMessage>
    </validationRules>
</CustomObject>
