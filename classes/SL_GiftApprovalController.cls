public with sharing class SL_GiftApprovalController {
    public List<SL_Gift> GiftList {get; set;}
    public String ViewType {get; set;}
    public Integer YearFilter {get; set;}
    public String NameFilter {get; set;}
    public String ApprovalComment {get; set;}
    public String NewSort {get; set;}
    public String SortField {get{return sortField == null ? 'GiftName' : sortField;} set{
        ToggleDirection(value);
        SortField = value;
    }}
    public String SortDirection {get{return sortDirection == null ? 'ASC' : sortDirection;} set;}
    public Map<String, Double> ConversionRates;
    public SL_GiftApprovalController() {     
        ViewType = 'Pending';
        YearFilter = Date.Today().Year();
        NameFilter = '';
        ConversionRates = HL_GiftUtil.GetConversionRates();
        searchGifts();
        
    }
    public void ToggleDirection(string newSort){
        SortDirection = (SortField == newSort) ? (SortDirection == 'DESC' ? 'ASC' : 'DESC') : 'ASC';
    }
    public void ApplySort()
    {
       SortField = NewSort;
       searchGifts();
     }
    public List<SelectOption> GetApprovalOptions() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Pending','Pending')); 
        options.add(new SelectOption('Approved','Approved')); 
        options.add(new SelectOption('Denied','Denied'));      
        return options; 
    }
    public List<SelectOption> GetYearFilterOptions() {
        List<SelectOption> years = new List<SelectOption>();
        Integer curYear = Date.Today().Year();
        years.add(new SelectOption(String.valueOf(curYear+1),String.valueOf(curYear+1)));
        for (Integer ii = 0; ii < 5; ii++){
            years.add(new SelectOption(String.valueOf(curYear-ii),String.valueOf(curYear-ii)));
        }

        return years;
    }
    public PageReference SearchGifts() {
        GiftList = new List<SL_Gift>();
        String filter = NameFilter+'%';
        List<SL_GiftRecipient> withGifts = new List<SL_GiftRecipient>();
        Map<Id, Contact> idToContact = new Map<Id, Contact>();
        for(Contact contact: [SELECT Id,
                                     Name, 
                                     Account.Name,
                                     MailingCountry,
                                     Office__c,
                                     (SELECT Id,
                                             Name,
                                             CreatedDate,
                                             Gift_Value__c,
                                             Approved__c,
                                             Approve_Date__c,
                                             Desired_Date__c,
                                             Recipient__r.Name,
                                             Recipient_Company_Name__c,
                                             Submitted_For__r.Name,
                                             Submitted_By__r.Name,
                                             Submitted_By__r.Office__c,
                                             Submitted_For__r.MailingCountry,
                                             Currency__c,
                                             CurrencyIsoCode
                                     FROM Gifts__r 
                                     WHERE CALENDAR_YEAR(Desired_Date__c)=:YearFilter 
                                      ) 
                             FROM Contact 
                             WHERE Id IN (SELECT Recipient__c 
                                         FROM Gift__c 
                                         WHERE Gift_Value__c > 0) 
                             AND Lastname 
                             LIKE :filter]) { 
            idToContact.put(contact.Id, contact);
        }
        Map<Id, Decimal> idToYTD = HL_GiftUtil.GetPrevYTDValue(idToContact.keySet());
        Map<Id, Decimal> idToNextYearYTD = HL_GiftUtil.GetPrevYTDNextYearValue(idToContact.keySet());

        for(Id contactId: idToContact.keySet()) {
            if (!idToContact.get(contactId).Gifts__r.isEmpty()){
                SL_GiftRecipient recipient = new SL_GiftRecipient(idToContact.get(contactId), 
                                                    idToYTD.containsKey(contactId) ? idToYTD.get(contactId) : 0.0,
                                                    idToNextYearYTD.containsKey(contactId) ? idToNextYearYTD.get(contactId) : 0.0,
                                                    ConversionRates,idToContact.get(contactId).Gifts__r[0].Desired_Date__c, idToContact.get(contactId).Gifts__r[0].CurrencyIsoCode);
                Boolean anyUSABasedSubmitters = false;
                for (Gift__c previousGift: idToContact.get(contactId).Gifts__r){
                    if (previousGift.Submitted_For__r.MailingCountry=='United States'){
                        anyUSABasedSubmitters = true;
                    }
                } 
                recipient.IsUSA = idToContact.get(contactId).MailingCountry == 'United States' || anyUSABasedSubmitters;
                recipient.IsFrance = idToContact.get(contactId).MailingCountry == 'France';
                withGifts.add(recipient);                                
            }
        }

        for(SL_GiftRecipient recipient: withGifts) {
            if (!recipient.recipient.Gifts__r.isEmpty()) {
                for(Gift__c eachGift: recipient.recipient.Gifts__r) {
                    if (eachGift.Approved__c == ViewType){
                        Decimal PrevYTD = 0.0;
                        if(eachGift.Desired_Date__c.Year() > System.Today().Year()){
                            PrevYTD = recipient.GiftPrevNextYearYTD;
                            }else{
                            PrevYTD = recipient.GiftPrevYTD;
                                }
                        Decimal exchangeRate =   eachGift.CurrencyIsoCode != null 
                                                && ConversionRates.containsKey(eachGift.CurrencyIsoCode) ?
                                                    (Decimal) ConversionRates.get(eachGift.CurrencyIsoCode) :
                                                    1.0;                        
                        if(ViewType == 'Pending'){                            
                            GiftList.add(
                            new SL_Gift(
                                eachGift, 
                                (PrevYTD*exchangeRate + eachGift.Gift_Value__c).setScale(1),
                                PrevYTD,
                                recipient
                            )
                        );
                        }else{
                        GiftList.add(
                            new SL_Gift(
                                eachGift, 
                                PrevYTD+(eachGift.Gift_Value__c!=null ? (eachGift.Gift_Value__c/exchangeRate) : 0),
                                PrevYTD,
                                recipient
                            )
                        );
                    }
                    }
                }
            }
        }
        SL_Gift.SortField = SortField;
        SL_Gift.SortDirection = SortDirection;
        GiftList.sort();
        return null;
    }
    public PageReference ApproveSelectedGifts() {
        ProcessSelectedGifts('Approve');
        return null;
    }
    public PageReference DenySelectedGifts() {
        ProcessSelectedGifts('Deny');
        return null;
    }
    public void ProcessSelectedGifts(string action) {
        try { 
            List<Gift__c> selectedGifts = new List<Gift__c>();
            Map<SL_GiftRecipient, Decimal> recipientToYTD = new Map<SL_GiftRecipient, Decimal>();
            Set <SL_GiftRecipient> recipientsOver = new Set<SL_GiftRecipient>();
            for(SL_Gift Gift: GiftList) {
                if(Gift.selected == true) {
                    selectedGifts.add(Gift.gift);
                    if (!recipientToYTD.keyset().contains(Gift.recipient)){
                        recipientToYTD.put(Gift.recipient, Gift.GiftYTD);
                    }
                    else{
                        recipientToYTD.put(Gift.recipient, recipientToYTD.get(Gift.recipient)+Gift.getGiftValue());
                    }
                    if(Gift.recipient.IsValuePassedOver(recipientToYTD.get(Gift.recipient))){
                        recipientsOver.add(Gift.recipient);
                    }
                }
            }

            if (selectedGifts.isEmpty()) {
                throw new SL_GiftLogException('You must select at least one gift to '+action.toLowercase()+'.');
            }
            else if (!recipientsOver.isEmpty()&&action=='Approve'&&ApprovalComment==''){
                String validationError = 'You MUST enter an Approval Comment to exceed the yearly limit.  Recipients exceeding yearly limit: ';
                List <String> errorMessages = new List<String>();
                for (SL_GiftRecipient exceeder: recipientsOver){
                    errorMessages.add(exceeder.recipient.Name + ' - USD ' + recipientToYTD.get(exceeder).setScale(1, RoundingMode.HALF_UP));
                }
                validationError += String.join(errorMessages, ', ');
                throw new SL_GiftLogException(validationError);
            }

            for(Gift__c gift: selectedGifts) {
                gift.Approved__c = action == 'Approve' ? 'Approved' : 'Denied';
                gift.Approved_By__c = UserInfo.getUserId();
                gift.Approve_Date__c = datetime.now();
                gift.Approval_Comment__c = ApprovalComment;
            }
            Database.SaveResult[] MySaveResult = Database.Update(selectedGifts, false);
            Integer giftNumber=0;
            for (Database.SaveResult sr : MySaveResult) {
                if(!sr.isSuccess()) {
                    for(Database.Error err : sr.getErrors()) {
                        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL,
                            'Cannot '+ action.toLowercase() + ' ' + selectedGifts[giftNumber].Name + ' for ' 
                            + selectedGifts[giftNumber].Recipient__r.Name + ': ' + err.getMessage());
                        ApexPages.addMessage(msg);  
                    }
                }
            giftNumber++;
            }    
        }    
        catch (SL_GiftLogException e){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL,e.getMessage());
            ApexPages.addMessage(msg);
        }
        searchGifts();
    }
}