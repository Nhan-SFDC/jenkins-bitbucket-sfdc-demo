public class HL_CoverageTeamHandler {
	public static void UpdateCoverageTeamAggregate(List<Coverage_Team__c> ctList){
        //Create Set of Company Ids
        Set<Id> cs = new Set<Id>();
        for(Coverage_Team__c c : ctList)
            cs.add(c.Company__c);
        //Get companies that may need to be updated
        List<Account> acctsToUpdate = [SELECT Id, Coverage_Team_Aggregate__c, Primary_Coverage_Officer__c, Primary_FS_Coverage_Officer__c FROM Account WHERE Id IN : cs];

        //Create a map of company with team member ids and a map with a list of coverage ordered
        Map<Id,Set<String>> acctTeamMap = new Map<Id,Set<String>>();
        Map<Id,Id> coMap = new Map<Id,Id>();
        Map<Id,Id> coMapFS = new Map<Id,Id>();
        //Loop through the FSCG Coverage (These are ranked ahead of Standard Coverage to store primary at Company (Account) Level)
        //Also need to include in aggregate field result
        for(Coverage_Team__c ct : [SELECT Company__c, Officer__c, Officer__r.User__c FROM Coverage_Team__c WHERE Company__c IN : cs AND RecordType.DeveloperName =: 'FS_Coverage_Team_Member' AND Coverage_Team_Status__c = 'Active' ORDER BY Coverage_Level__c ASC, Tier__c ASC]){
            if(acctTeamMap.get(ct.Company__c) == null){
                acctTeamMap.put(ct.Company__c, new Set<String>{ct.Officer__r.User__c});
                coMap.put(ct.Company__c, ct.Officer__c); //We want to only put first one here so we don't have to check in else
                coMapFS.put(ct.Company__c, ct.Officer__c); //put first FS officer into separate map
            }
            else
            {
                Set<String> existingIds = (Set<String>)acctTeamMap.get(ct.Company__c);
                existingIds.add(ct.Officer__r.User__c);
                acctTeamMap.put(ct.Company__c, existingIds);
            }
        }
        
        //Loop through the Standard Coverage (similar to above)
        for(Coverage_Team__c ct : [SELECT Company__c, Officer__c, Officer__r.User__c FROM Coverage_Team__c WHERE Company__c IN : cs AND RecordType.DeveloperName =: 'Standard_Coverage_Team_Member' AND Coverage_Team_Status__c = 'Active' ORDER BY Coverage_Level__c DESC, Tier__c ASC])
        {
            if(acctTeamMap.get(ct.Company__c) == null){
                acctTeamMap.put(ct.Company__c, new Set<String>{ct.Officer__r.User__c});
                coMap.put(ct.Company__c, ct.Officer__c); //We want to only put first one here so we don't have to check in else
            }
            else
            {
                Set<String> existingIds = (Set<String>)acctTeamMap.get(ct.Company__c);
                existingIds.add(ct.Officer__r.User__c);
                acctTeamMap.put(ct.Company__c, existingIds);
            }
        }

        
        for(Account a: acctsToUpdate){
            a.Coverage_Team_Aggregate__c = null;
            if(acctTeamMap.get(a.Id) <> null)
            {
                for(String id : (Set<String>)acctTeamMap.get(a.Id)){
                    if(String.isBlank(a.Coverage_Team_Aggregate__c) || a.Coverage_Team_Aggregate__c.length() < 230)
                    	a.Coverage_Team_Aggregate__c = (String.isBlank(a.Coverage_Team_Aggregate__c) ? id : (a.Coverage_Team_Aggregate__c.contains(id) ? a.Coverage_Team_Aggregate__c : a.Coverage_Team_Aggregate__c + '|' + id));
                }
                if(coMap.get(a.Id) <> null)
                    a.Primary_Coverage_Officer__c = (Id)coMap.get(a.Id);
                if(coMapFS.get(a.Id) <> null){
                    a.Primary_FS_Coverage_Officer__c = (Id)coMapFS.get(a.Id);
                }
                else{
                    a.Primary_FS_Coverage_Officer__c = null;
                }
            }
            else{
                a.Coverage_Team_Aggregate__c = null;
                a.Primary_Coverage_Officer__c = null;
                a.Primary_FS_Coverage_Officer__c = null;
            }
        }
        
        if(acctsToUpdate.size() > 0)
            update acctsToUpdate;
    }
}