@isTest
private class Test_HL_EngagementViewController {
    @isTest private static void TestBasicFunctionality(){
        //Setup Test Data
        Engagement__c e = SL_TestSetupUtils.CreateEngagement('', 1)[0];
        
        ApexPages.StandardController sc = new ApexPages.StandardController(e);
        HL_EngagementViewController con = new HL_EngagementViewController(sc);
        Boolean hasCommentsAccess = con.AddCommentsAccess;
        
        System.assert(!ApexPages.hasMessages(ApexPages.Severity.ERROR));
    }
}