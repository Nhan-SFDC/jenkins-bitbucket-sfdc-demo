@isTest
private class Test_HL_RevenueAccrualHandler {
    @isTest
    private static void TestInsertRevenueAccrualRecord()
	{
		Account clientAccount = (Account)HL_TestFactory.CreateSObject('Account', false);
		insert clientAccount;

		Account subjectAccount = (Account)HL_TestFactory.CreateSObject('Account', false);
		insert subjectAccount;

		Engagement__c eFAS = new Engagement__c(
                                                Name='Test FAS Engagement',
            									Engagement_Number__c = '123456',
                                                Client__c = clientAccount.Id,
                                                Subject__c = subjectAccount.Id,
                                                Stage__c = 'Retained',
                                                Line_of_Business__c = 'FAS',
                                                Job_Type__c = 'Fairness',
                                                Primary_Office__c = 'NY'
        									  );
        insert eFAS;

        Monthly_Revenue_Process_Control__c mrpcNew = new Monthly_Revenue_Process_Control__c(IsCurrent__c = TRUE,
		                                                      Current_Month__c = '02', Current_Year__c = '2015');
		insert mrpcNew;

		Revenue_Accrual__c revenueAccrualNew = new Revenue_Accrual__c(Engagement__c = eFAS.Id,
		                                                      Period_Accrued_Fees__c = 100000, Total_Estimated_Fee__c = 250000);

        Test.startTest();

        insert revenueAccrualNew;

		Test.stopTest();

		// Confirm that Revenue Accrual record was created successfully
		System.assertEquals([Select Id From Revenue_Accrual__c where Id = :RevenueAccrualNew.Id].Size(), 1);

        //Confirm the Current Revenue Accrual field on the engagement was populated
        eFAS = [SELECT Current_Revenue_Accrual__c FROM Engagement__c Where Id =: eFAS.Id];
        System.assert(!String.isBlank(eFAS.Current_Revenue_Accrual__c));
	}

    @isTest
    private static void TestUpdateRevenueAccrualRecord()
	{
		Account clientAccount = (Account)HL_TestFactory.CreateSObject('Account', false);
		insert clientAccount;

		Account subjectAccount = (Account)HL_TestFactory.CreateSObject('Account', false);
		insert subjectAccount;

        //FAS Engagement
		Engagement__c eFAS = new Engagement__c(
                                                Name='Test FAS Engagement',
            									Engagement_Number__c = '654321',
                                                Client__c = clientAccount.Id,
                                                Subject__c = subjectAccount.Id,
                                                Stage__c = 'Retained',
                                                Line_of_Business__c = 'FAS',
                                                Job_Type__c = 'Fairness',
                                                Primary_Office__c = 'NY'
        									  );
		insert eFAS;
        //Non-FAS Engagement - the other lines all follow the same basic rules
		Engagement__c eCF = new Engagement__c(
                                                Name='Test CF Engagement',
            									Engagement_Number__c = '765432',
                                                Client__c = clientAccount.Id,
                                                Subject__c = subjectAccount.Id,
                                                Stage__c = 'Active',
                                                Line_of_Business__c = 'CF',
                                                Job_Type__c = 'Sellside',
                                                Primary_Office__c = 'NY'
        					);
		insert eCF;

        Monthly_Revenue_Process_Control__c mrpcNew = new Monthly_Revenue_Process_Control__c(IsCurrent__c = TRUE,
		                                                      Current_Month__c = '02', Current_Year__c = '2015');
		insert mrpcNew;

		Revenue_Accrual__c raFAS = new Revenue_Accrual__c(Engagement__c = eFAS.Id,
		                                                      Period_Accrued_Fees__c = 100000, Total_Estimated_Fee__c = 250000);
        insert raFAS;

        Revenue_Accrual__c raNonFAS = new Revenue_Accrual__c(Engagement__c = eCF.Id,
		                                                      Period_Accrued_Fees__c = 100000, Total_Estimated_Fee__c = 250000);
        insert raNonFAS;

        Test.startTest();

		//Changes to Total Estimated Fee for FAS should be replicated to the Engagement
		SL_Statics.bypassRevenueAccrualTrigger = false;
		raFAS.Total_Estimated_Fee__c = raFAS.Total_Estimated_Fee__c * 2;
		update raFAS;
		//Changes to the Period Accrued Fee for Non FAS should be replicated to the Engagement
		SL_Statics.bypassRevenueAccrualTrigger = false;
		raNonFAS.Period_Accrued_Fees__c = raNonFAS.Period_Accrued_Fees__c * 2;
		update raNonFAS;

		Test.stopTest();

		//Confirm the Total Estimated Fee was Updated on the FAS Engagement
		eFAS = [SELECT Total_Estimated_Fee__c FROM Engagement__c WHERE Id =: eFAS.Id];
        System.assertEquals(raFAS.Total_Estimated_Fee__c, eFAS.Total_Estimated_Fee__c);

        //Confirm the Period Accrued Fee was Updated on the Non-FAS Engagement
        eCF = [SELECT Period_Accrued_Fees__c FROM Engagement__c WHERE Id =: eCF.Id];
        System.assertEquals(raNonFAS.Period_Accrued_Fees__c, eCF.Period_Accrued_Fees__c);
	}
}