({
    deleteActivityRecord: function(component, helper, recordId){
        helper.callServer(component, "c.DeleteRecord", function(response) {
            helper.fireSaveSuccess(component, "DeleteActivity");
        }, {"recordId": recordId}); 
    },
    fireSaveSuccess: function(component, saveOperation) {
        var saveSuccessEvent = component.getEvent("saveSuccessEvent");
        
        saveSuccessEvent.setParams({
            "saveOperation": saveOperation
        });
        
        saveSuccessEvent.fire();
    },
    getRowJSON: function(component) {
        var rowJSON = [];
        var rowRecord = component.get("v.rowRecord");
        var dateRecord = component.get("v.dateRecord");
        var activityRecord = component.get("v.activityRecord");
        var selectedProject = component.get("v.selectedProject");
        var requireActivityTypes = selectedProject.Type != "Special_Project__c";
        
        if (rowRecord && dateRecord && activityRecord && selectedProject &&
            activityRecord.Hours > 0 && (!requireActivityTypes || activityRecord.ActivityType)) {
            
            //If the record exists (edit), verify some value has changed
            if (activityRecord.Id == null ||
                activityRecord.Id != null && (
                    activityRecord.Hours != activityRecord.OriginalHours ||
                    (activityRecord.ActivityType && activityRecord.ActivityType != activityRecord.OriginalActivityType) ||
                    (activityRecord.Comments && activityRecord.Comments != activityRecord.OriginalComments)
                )) {
                
                rowJSON.push({
                    "sobjectType": "Time_Record__c",
                    "Id": activityRecord.Id,
                    "Activity_Date__c": dateRecord.ActivityDate,
                    "Activity_Type__c": (selectedProject.Type == "Special_Project__c" ? "" : activityRecord.ActivityType),
                    "Comments__c": activityRecord.Comments,
                    "Hourly_Rate__c": 0,
                    "Hours_Worked__c": activityRecord.Hours,
                    "Engagement__c": (selectedProject.Type == "Engagement__c" ? selectedProject.Id : null),
                    "Opportunity__c": (selectedProject.Type == "Opportunity__c" ? selectedProject.Id : null),
                    "Special_Project__c": (selectedProject.Type == "Special_Project__c" ? selectedProject.Id : null),
                    "Recording_Status__c": "Complete",
                    "Time_Record_Period_Staff_Member__c": rowRecord.Time_Record_Period_Staff_Member__c
                });
            }
        }
        
        return rowJSON;
    },
    onActivityRecordChanged: function(component, helper) {
        //Currently setup to save upon each entry change
        var activityRecord = component.get("v.activityRecord");
        if (activityRecord && helper.validateValues(component)) {
            var rowRecord = component.get("v.rowRecord");
            var timeRecordPeriodStaffMember = component.get("v.timeRecordPeriodStaffMember");
            var action = activityRecord.Id ? "c.UpdateRecords" : "c.InsertRecords";
            var rowJSON = helper.getRowJSON(component);
            
            if(rowJSON.length > 0){
                helper.callServer(component, action, function(response) {
                    if (response && response.length > 0) {
                        activityRecord.Id = response[0].Id;
                        component.set("v.activityRecord", activityRecord);
                        if (rowRecord.IsNewRow)
                            helper.fireSaveSuccess(component, "NewRow");
                        else
                            helper.fireSaveSuccess(component, "NewActivity");
                    }
                }, {
                    "timeRecords": rowJSON
                });
            }
        }
    },
    validateValues: function(component) {
        var selectedProject = component.get("v.selectedProject");
        var activityRecord = component.get("v.activityRecord");
        //Display any error messages before save attempt
        var isValid = true;
        var msg = "";
        var requireActivityTypes = selectedProject && selectedProject.Type != "Special_Project__c";
        
        //Validate a project was selected
        if(!selectedProject)
            msg += (msg === "" ? "" : "\r\n") + " Project Selection Needed";
        
        //Validate numeric hourly entries
        if (activityRecord.Hours && (!requireActivityTypes || activityRecord.ActivityType) && (isNaN(activityRecord.Hours) || activityRecord.Hours <= 0))
            msg += (msg === "" ? "" : "\r\n") + " Invalid Hour Entry - Must be a Positive Number";
        
        if (msg != "")
            isValid = false;
        
        return isValid;
    }
})