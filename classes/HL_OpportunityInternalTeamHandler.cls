public with sharing class HL_OpportunityInternalTeamHandler {
    /* Start - Variable */
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    /* End - Variable */

    /* Start - Constructor */
    public HL_OpportunityInternalTeamHandler(boolean isExecuting, integer size) 
    {
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    /* End - Constructor */

    /*
        @MethodName         : onAfterInsert
        @param              : List of Opportunity_Internal_Team__c
        @Description        : This function will be called on after insert of the Opportunity_Internal_Team__c records
    */
    public void onAfterInsert(List<Opportunity_Internal_Team__c> oitList)
    {
        UpdateEngagementTeamAssembled(oitList);
        UpdateInternalTeamAggregate(oitList);
        SL_ManageSharingRules.manageSharingRules(oitList, 'Opportunity__c', 'Opportunity_Internal_Team__c', 'insert');                                                                                                               
    }
    /* End */
    
    /*
        @MethodName         : onAfterUpdate
        @param              : Old and new map of Opportunity_Internal_Team__c
        @Description        : This function will be called on after update of the Opportunity_Internal_Team__c records
    */
    public void onAfterUpdate(Map<Id, Opportunity_Internal_Team__c> mapOldOIT, Map<Id, Opportunity_Internal_Team__c> mapNewOIT, List<Opportunity_Internal_Team__c> oitList)
    {   
        UpdateEngagementTeamAssembled(oitList);
        UpdateInternalTeamAggregate(oitList);
        SL_ManageSharingRules.createDeleteShareOnUpdate(mapOldOIT, mapNewOIT, 'Opportunity__c', 'Opportunity_Internal_Team__c');
    }
    /* End */
    
    /*
        @MethodName         : onAfterDelete
        @param              : map of Opportunity_Internal_Team__c
        @Description        : This function will be called on after delete of the Opportunity_Internal_Team__c records
                              to delete the Opportunity share records related to Opportunity_Internal_Team__c   
    */
    public void onAfterDelete(Map<Id, Opportunity_Internal_Team__c> mapOITOld, List<Opportunity_Internal_Team__c> oitList)
    {
        UpdateEngagementTeamAssembled(oitList);
        UpdateInternalTeamAggregate(oitList);
        SL_ManageSharingRules.manageSharingRules(mapOITOld.values(), 'Opportunity__c', 'Opportunity_Internal_Team__c', 'delete');
    }
    /* End */

    public static void UpdateEngagementTeamAssembled(List<Opportunity_Internal_Team__c> oitList){
        //Create Set of Opportunity Ids
        Set<Id> opps = new Set<Id>();
        List<String> engagementTeamRoles = new List<String> {'Principal','Seller','Manager','Initiator'};
        for(Opportunity_Internal_Team__c oit : oitList)
            opps.add(oit.Opportunity__c);
        
        //Get opportunities that may need to be updated
        List<Opportunity__c> possibleOpps = [SELECT Id, Engagement_Team_Assembled__c FROM Opportunity__c WHERE Id IN : opps];
        List<Opportunity_Internal_Team__c> existingEngagementRoles = [SELECT Opportunity__c, Staff_Role__r.Name FROM Opportunity_Internal_Team__c WHERE Opportunity__c IN : opps AND Staff_Role__r.Name IN : engagementTeamRoles AND (End_Date__c = null OR End_Date__c > : date.today())];
        //Create a Map of existing roles to validate against
        Map<String, Id> er = new Map<String,Id>();
        for(Opportunity_Internal_Team__c oit:existingEngagementRoles){
            if(er.get(oit.Opportunity__c + ':' + oit.Staff_Role__r.Name) == null)
                er.put(oit.Opportunity__c + ':' + oit.Staff_Role__r.Name, oit.Opportunity__c);
        }
        
        //Find which opps we need to update and then update
        List<Opportunity__c> oppsToUpdate = new List<Opportunity__c>();
        
        for(Opportunity__c opp : possibleOpps)
        {
            integer roleCount = 0;
            for(String r : engagementTeamRoles)
            {
                if(er.get(opp.Id + ':' + r) <> null)
                    roleCount++;
            }
            
            //Determine if opp needs to be updated
            if((opp.Engagement_Team_Assembled__c && roleCount <> engagementTeamRoles.size()) || (!opp.Engagement_Team_Assembled__c && roleCount >= engagementTeamRoles.size()))
            {
                 opp.Engagement_Team_Assembled__c = !opp.Engagement_Team_Assembled__c;
                 oppsToUpdate.add(opp);
            }
               
        }
        
        if(oppsToUpdate.size() > 0)
            update oppsToUpdate;

    }
    
    public static void UpdateInternalTeamAggregate(List<Opportunity_Internal_Team__c> oitList){
        //Oppt To Update
        List<Opportunity__c> oToUpdate = new List<Opportunity__c>();
        //Create Set of Opportunity Ids
        Set<Id> opps = new Set<Id>();
        for(Opportunity_Internal_Team__c oit : oitList)
            opps.add(oit.Opportunity__c);
        //Create a map of opportunity|role with team member ids
        Map<String,Set<String>> oppTeamMap = new Map<String,Set<String>>();
        for(Opportunity_Internal_Team__c o : [SELECT Opportunity__c, Contact__r.User__c, Staff_Role__r.Name FROM Opportunity_Internal_Team__c WHERE Opportunity__c IN : opps AND End_Date__c = null]){
                String key = o.Opportunity__c + '_' + o.Staff_Role__r.Name;
                if(oppTeamMap.get(key) == null)
                    oppTeamMap.put(key, new Set<String>{o.Contact__r.User__c});
                else
                {
                    Set<String> existingIds = (Set<String>)oppTeamMap.get(key);
                    existingIds.add(o.Contact__r.User__c);
                    oppTeamMap.put(o.Opportunity__c, existingIds);
                }
        }
        
        //Get opportunities that may need to be updated
        for(Opportunity__c o : [SELECT Id FROM Opportunity__c WHERE Id IN : opps])
        {
                o.z_Admin_Intern_Aggregate__c = PopulateAggregateField(o.Id + '_Intern', oppTeamMap);
                o.z_Analyst_Aggregate__c = PopulateAggregateField(o.Id + '_Analyst', oppTeamMap);
                if(o.z_Analyst_Aggregate__c.Length() > 255)
                {
                    o.z_Analyst_Aggregate2__c = o.z_Analyst_Aggregate__c.subString(247);
                    o.z_Analyst_Aggregate__c = o.z_Analyst_Aggregate__c.subString(0,246);
                }
                o.z_Associate_Aggregate__c = PopulateAggregateField(o.Id + '_Associate', oppTeamMap);
                if(o.z_Associate_Aggregate__c.Length() > 255){
                    o.z_Associate_Aggregate2__c = o.z_Associate_Aggregate__c.subString(247);
                    o.z_Associate_Aggregate__c = o.z_Associate_Aggregate__c.subString(0,246);
                }
                o.z_Final_Rev_Aggregate__c = PopulateAggregateField(o.Id + '_Final Reviewer', oppTeamMap);
                o.z_Initiator_Aggregate__c= PopulateAggregateField(o.Id + '_Initiator', oppTeamMap);
                if(o.z_Initiator_Aggregate__c.Length() > 255){
                    o.z_Initiator_Aggregate2__c = o.z_Initiator_Aggregate__c.subString(247);
                    o.z_Initiator_Aggregate__c = o.z_Initiator_Aggregate__c.subString(0,246);
                }
                o.z_Manager_Aggregate__c = PopulateAggregateField(o.Id + '_Manager', oppTeamMap);
                o.z_Marketing_Aggregate__c = PopulateAggregateField(o.Id + '_Marketing Team', oppTeamMap);
                if(o.z_Marketing_Aggregate__c.Length() > 255){
                    o.z_Marketing_Aggregate2__c = o.z_Marketing_Aggregate__c.subString(247);
                    if(o.z_Marketing_Aggregate2__c.Length() > 255){
                        o.z_Marketing_Aggregate3__c = o.z_Marketing_Aggregate2__c.subString(247);
                        o.z_Marketing_Aggregate2__c = o.z_Marketing_Aggregate2__c.subString(0,246);
                    }
                    o.z_Marketing_Aggregate__c = o.z_Marketing_Aggregate__c.subString(0,246);
                }
                o.z_NonRegistered_Aggregate__c = PopulateAggregateField(o.Id + '_Non-Registered', oppTeamMap);
                o.z_PE_HF_Aggregate__c = PopulateAggregateField(o.Id + '_PE/Hedge Fund', oppTeamMap);
                o.z_Prelim_Rev_Aggregate__c = PopulateAggregateField(o.Id + '_Prelim Reviewer', oppTeamMap);
                o.z_Pricing_Aggregate__c = PopulateAggregateField(o.Id + '_Pricing Committee Approver', oppTeamMap);
                o.z_Principal_Aggregate__c = PopulateAggregateField(o.Id + '_Principal', oppTeamMap);
                o.z_Public_Aggregate__c = PopulateAggregateField(o.Id + '_Public Person', oppTeamMap);
                o.z_Reviewer_Aggregate__c = PopulateAggregateField(o.Id + '_Reviewer', oppTeamMap);
                o.z_Seller_Aggregate__c = PopulateAggregateField(o.Id + '_Seller', oppTeamMap);
                o.z_Specialty_Aggregate__c = PopulateAggregateField(o.Id + '_Ind/Prod/Geog Person', oppTeamMap);
            	oToUpdate.add(o);
        }
        if(oToUpdate.size() > 0)
            update oToUpdate;
    }

    private static String PopulateAggregateField(String mapKey, Map<String,Set<String>> teamMap){
        String fieldValue = '';
        if(teamMap.get(mapKey) <> null){
            for(String id : (Set<String>)teamMap.get(mapKey)){
                if(id <> null)
                	fieldValue = String.isBlank(fieldValue) ? id : (fieldValue.contains(id) ? fieldValue : fieldValue + '|' + id);
            }
        }
        else
            fieldValue = '';
        
        return fieldValue;
    }
}