({
    onProjectSelected: function(component, event, helper) {
        var selectedProjectId = component.get("v.selectedProjectId");
        var selectedId = event.getParam("selectedId");
        selectedProjectId = selectedId;
        component.set("v.selectedProjectId", selectedId);
        this.handleProjectRequirements(component, selectedId);
    },
    intervalCallback: function(component, event, helper) {
        //We want to issue a save every 10th of an Hour (6 Minutes)
        if (clock) {
            var minutes = clock.getTime().getMinutes(false);
            var seconds = clock.getTime().getSeconds(true);
            if (minutes > 0 && minutes % 6 == 0 && seconds === 0)
                component.helper.saveTimeRecord(component, event, helper, "In Progress");
        }
    },
    getObjectType: function(id) {
        return id.startsWith('a0I') ? "Opportunity__c" : id.startsWith('a09') ? "Engagement__c" : "Special_Project__c";
    },
    getPendingRecords: function(component, event, helper) {
        helper.callServer(component, "c.GetPendingRecords", function(response) {
            if (response && response.length > 0) {
                var timeRecord = response[0];
                component.set("v.hasPendingRecord", true);
                component.set("v.timeRecord", timeRecord);
                component.set("v.selectedProjectId", timeRecord.Opportunity__c ? timeRecord.Opportunity__c : timeRecord.Engagement__c ? timeRecord.Engagement__c : timeRecord.Special_Project__c);
            } else
                component.set("v.hasPendingRecord", false);
        });
    },
    handleProjectRequirements: function(component) {
        var selectedId = component.get("v.selectedProjectId");
        var requireComments = false;
        var projectSelections = component.get("v.projectSelections");
        var isSpecialProject = false;
        projectSelections.filter(function(p) {
            if (p.Id === selectedId) {
                isSpecialProject = p.Type === "Special_Project__c";
                requireComments = p.RequireComments;
            }
        });
        component.set('v.requireActivityTypes', !isSpecialProject);
        component.set('v.requireComments', requireComments);
    },
    instantiateClock: function(component, event, helper) {
        clock = new FlipClock($j('.clock'), {
            callbacks: {
                interval: function() {
                    helper.intervalCallback(component, event, helper);
                }
            }
        });
    },
    toggleTrackingFields: function(component, event, helper, toggle) {
        var activityType = component.find("activityType");
        component.set("v.isProjectSelectionDisabled", !toggle);
        if (activityType)
            activityType.set("v.disabled", !toggle);
    },
    onPause: function(component, event, helper) {
        clock.stop();
    },
    onFinish: function(component, event, helper) {
        helper.saveTimeRecord(component, event, helper, 'Complete');
    },
    onResume: function(component, event, helper) {
        var timeRecord = component.get("v.timeRecord");
        //Occasionally the Activity Types load late so reset here in case
        component.set("v.timeRecord", timeRecord);

        helper.instantiateClock(component, event, helper);

        clock.getTime().addSeconds(timeRecord.Hours_Worked__c * 60 * 60);

        component.set("v.hasPendingRecord", false);

        helper.toggleTrackingFields(component, event, helper, false);
    },
    onSaveSuccess: function(component, event, helper, recordingStatus) {
        if (recordingStatus === "Complete" || recordingStatus === "CompleteAndResume") {
            clock.reset();
            if (recordingStatus === "Complete") {
                clock.stop();
                component.set("v.selectedProjectId", null);
                component.set("v.timeRecord", {
                    "sobjectType": "Time_Record__c"
                });
                helper.toggleTrackingFields(component, event, helper, true);
            } else {
                helper.instantiateClock(component, event, helper);
                helper.saveTimeRecord(component, event, helper, "In Progress");
            }
        }
    },
    onStart: function(component, event, helper) {
        if (helper.validateStart(component, event, helper)) {
            if (clock)
                clock.start();
            else {
                helper.instantiateClock(component, event, helper);
                helper.saveTimeRecord(component, event, helper, "In Progress");
            }

            helper.toggleTrackingFields(component, event, helper, false);
        }
    },
    round: function(value, precision) {
        var multiplier = Math.pow(10, precision || 0);
        return Math.round(value * multiplier) / multiplier;
    },
    saveTimeRecord: function(component, event, helper, recordingStatus) {
        var today = new Date();
        var selectedProjectId = component.get("v.selectedProjectId");
        var timeRecord = component.get("v.timeRecord");
        var category = component.get("v.category");
        var recordDate = today.getFullYear() + "-" + ("0" + (today.getMonth() + 1)).slice(-2) + "-" + ("0" + today.getDate()).slice(-2);
        var minutes = clock.getTime().getMinutes(false);
        var seconds = clock.getTime().getSeconds(true);
        var objectType = helper.getObjectType(selectedProjectId);

        timeRecord.Opportunity__c = (objectType == "Opportunity__c" ? selectedProjectId : null);
        timeRecord.Engagement__c = (objectType == "Engagement__c" ? selectedProjectId : null);
        timeRecord.Special_Project__c = (objectType == "Special_Project__c" ? selectedProjectId : null);
        timeRecord.Hours_Worked__c = helper.round(minutes / 60.0, 1);
        timeRecord.Seconds_Worked_Actual__c = seconds;
        timeRecord.Recording_Status__c = recordingStatus;

        if (timeRecord.Id && timeRecord.Id != null) {
            //Check if Date has Changed (If So, Save off Yesterday's Record and Start New)
            if (recordDate == timeRecord.Activity_Date__c.slice(0, 10)) {
                helper.callServer(component, "c.UpdateRecord", function(response) {
                    if (response && response.Id != null) {
                        component.set("v.timeRecord", response);
                        component.set("v.showError", false);
                        helper.onSaveSuccess(component, event, helper, recordingStatus);
                    } else
                        throw new Error("Unable to Update Time Record");
                }, {
                    "tr": timeRecord
                });
            } else {
                //Time Record has crossed midnight
                timeRecord.Recording_Status__c = 'Complete';
                helper.callServer(component, "c.UpdateRecord", function(response) {
                    if (response && response.Id != null) {
                        timeRecord.Id = null;
                        timeRecord.sobjectType = "Time_Record__c";
                        timeRecord.Recording_Status__c = "Pending";

                        component.set("v.timeRecord", timeRecord);
                        component.set("v.showError", false);

                        helper.onSaveSuccess(component, event, helper, "CompleteAndResume");
                    } else
                        console.log(response);
                }, {
                    "tr": timeRecord
                });
            }
        } else {
            helper.callServer(component, "c.InsertRecord", function(response) {
                if (response && response.Id != null) {
                    component.set("v.timeRecord", response);
                    component.set("v.showError", false);
                    helper.onSaveSuccess(component, event, helper, recordingStatus);
                } else
                    throw new Error("Unable to Insert Time Record");;
            }, {
                "tr": timeRecord,
                "recordDate": recordDate,
                "category": category
            });
        }
    },
    updateTime: function(component, event, helper) {
        var updateMinutes = component.find("updateMinutes");
        var minutes = updateMinutes.get("v.value");

        if (!isNaN(minutes)) {
            minutes = minutes * 60;
            //Validate we are not hitting negative time
            var elapsed = clock.getTime().getSeconds(false);
            if (elapsed + minutes >= 0) {
                clock.getTime().addSeconds(minutes);
                updateMinutes.set("v.value", null);
            } else
                alert("Invalid - Exceeds Elapsed Time");
        } else
            alert("Invalid Number");
    },
    validateStart: function(component, event, helper) {
        var selectedProjectId = component.get("v.selectedProjectId");
        var requireActivityTypes = component.get("v.requireActivityTypes");
        var requireComments = component.get("v.requireComments");
        var errorMsg = "";

        if (!selectedProjectId || selectedProjectId == "")
            errorMsg += "\r\nProject Selection Required";
        if (requireActivityTypes) {
            var activityType = component.find("activityType").get("v.value");
            if (!activityType || activityType == "")
                errorMsg += "\r\nActivity Required"
        }

        if (errorMsg != "") {
            isValid = false;
            component.set("v.showError", true);
            component.find("errorText").set("v.value", " " + errorMsg);
        } else
            component.set("v.showError", false);

        return errorMsg === "";
    }
})