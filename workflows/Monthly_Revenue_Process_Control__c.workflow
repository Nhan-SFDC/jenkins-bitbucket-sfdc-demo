<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Revenue_Accrual_Locked</fullName>
        <ccEmails>jliu@hl.com</ccEmails>
        <description>Revenue Accrual Locked</description>
        <protected>false</protected>
        <recipients>
            <recipient>Monthly_Revenue_Process_Notification</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Monthly_Revenue_Process/Revenue_Locked</template>
    </alerts>
    <alerts>
        <fullName>Revenue_Accrual_Unlocked</fullName>
        <ccEmails>jliu@hl.com</ccEmails>
        <description>Revenue Accrual Unlocked</description>
        <protected>false</protected>
        <recipients>
            <recipient>Monthly_Revenue_Process_Notification</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Monthly_Revenue_Process/Revenue_Unlocked</template>
    </alerts>
    <rules>
        <fullName>Notify Revenue Locked</fullName>
        <actions>
            <name>Revenue_Accrual_Locked</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Revenue_Accruals_Locked__c = true &amp;&amp; $Setup.HL_General__c.Revenue_Accrual_Locked__c = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Revenue Unlocked</fullName>
        <actions>
            <name>Revenue_Accrual_Unlocked</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>$Setup.HL_General__c.Revenue_Accrual_Locked__c = false</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
