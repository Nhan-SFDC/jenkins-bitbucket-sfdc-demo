public class HL_ContactNewController {
    public Integer Action{get; set;}
    public enum NewAction{
        Standard,
        Quick
    }
    public ApexPages.StandardController Con {get; set;}
    public Account ExistingAccount {get; set;}
    public Contact NewContact {get; set;}
    public HL_ContactNewController(ApexPages.StandardController controller){
        Map<String, String> q = ApexPages.currentPage().getParameters();
        if(q.containsKey('accId'))
        	ExistingAccount = HL_Company.GetById(q.get('accId'));
        if(q.containsKey('action'))
          Action = Integer.valueOf(q.get('action'));
        if(controller.getRecord() <> null)
            NewContact = (Contact)controller.getRecord();
        Con = controller;
    }
    public PageReference SaveAndView(){
        if(SaveRecord())
        	return Action <> 1 ? new PageReference('/' + NewContact.Id) : null;
        else
            return null;
    }
    public Boolean SaveRecord(){
        try{
            NewContact = (Contact)Con.getRecord();
            insert NewContact;
            return true;
        }
        catch(DmlException ex){
            HL_PageUtility.ShowError(ex);
            return false;
        }
    }
    public PageReference RedirectCheck(){
        Map<String, String> q = ApexPages.currentPage().getParameters();
        if(q.containsKey('RecordType') && q.get('RecordType') == HL_Utility.GetRecordTypeId('Houlihan Employee'))
            return null;
        else 
            return null;
    }
}