trigger HL_TimeRecordTrigger on Time_Record__c (after insert, after update, after delete) {
	HL_TimeRecordHandler handler = new HL_TimeRecordHandler(Trigger.isExecuting, Trigger.size);

    if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
            handler.OnAfterInsert(Trigger.new);
        else if(Trigger.isUpdate)
            handler.OnAfterUpdate(Trigger.new, Trigger.oldMap, Trigger.newMap);
        else if(Trigger.isDelete){
            handler.OnAfterDelete(Trigger.old);
        }
    }
}