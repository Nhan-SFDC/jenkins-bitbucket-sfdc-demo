<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Used to store users&apos; selections of which columns to display in the pipeline management page.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Engagement_Columns__c</fullName>
        <externalId>false</externalId>
        <label>Engagement Columns</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Engagement_Industry_Filter__c</fullName>
        <defaultValue>&quot;All&quot;</defaultValue>
        <description>Last Industry Filter Selected</description>
        <externalId>false</externalId>
        <label>Engagement Industry Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement_Office_Filter__c</fullName>
        <defaultValue>&quot;All&quot;</defaultValue>
        <description>Last Engagement Office Filter Selection</description>
        <externalId>false</externalId>
        <label>Engagement Office Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement_Record_Type_Filter__c</fullName>
        <defaultValue>&quot;All&quot;</defaultValue>
        <description>Last Engagement Record Type Filter Selection</description>
        <externalId>false</externalId>
        <label>Engagement Record Type Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement_Show_Records_Filter__c</fullName>
        <defaultValue>&quot;TeamEngagements&quot;</defaultValue>
        <description>Last Engagement Show Records Filter Selection</description>
        <externalId>false</externalId>
        <label>Engagement Show Records Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement_Sort_Direction__c</fullName>
        <defaultValue>&quot;ASC&quot;</defaultValue>
        <description>Last sort direction of the Engagement Manager</description>
        <externalId>false</externalId>
        <label>Engagement Sort Direction</label>
        <length>5</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement_Sort_Field__c</fullName>
        <description>Last Engagement Field Sorted On</description>
        <externalId>false</externalId>
        <label>Engagement Sort Field</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement_Staff_Id_Filter__c</fullName>
        <description>Last Engagement Staff Id Filter</description>
        <externalId>false</externalId>
        <label>Engagement Staff Id Filter</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement_Staff_Industry_Filter__c</fullName>
        <defaultValue>&quot;All&quot;</defaultValue>
        <description>Last Staff Industry Filter Selected</description>
        <externalId>false</externalId>
        <label>Engagement Staff Industry Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement_Staff_Name_Filter__c</fullName>
        <description>Last Engagement Staff Name Filter Selected</description>
        <externalId>false</externalId>
        <label>Engagement Staff Name Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement_Staff_Product_Filter__c</fullName>
        <description>Engagement Staff Product Filter</description>
        <externalId>false</externalId>
        <inlineHelpText>Engagement Staff Product Filter</inlineHelpText>
        <label>Engagement Staff Product Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement_Staff_Role_Filter__c</fullName>
        <defaultValue>&quot;All&quot;</defaultValue>
        <description>Last Engagement Staff Role Filter Selected</description>
        <externalId>false</externalId>
        <label>Engagement Staff Role Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement_Stage_Filter__c</fullName>
        <defaultValue>&quot;All&quot;</defaultValue>
        <description>Last Engagement Stage Filter Selected</description>
        <externalId>false</externalId>
        <label>Engagement Stage Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engagement_Status_Filter__c</fullName>
        <defaultValue>&quot;Active&quot;</defaultValue>
        <description>Last Engagement Status Filter</description>
        <externalId>false</externalId>
        <label>Engagement Status Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Include_Reviewer_Roles__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Include Reviewer Roles (FAS) 
- Prelim Reviewer 
- Reviewer 
- Final Reviewer</description>
        <externalId>false</externalId>
        <inlineHelpText>Include Reviewer Roles</inlineHelpText>
        <label>Include Reviewer Roles</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Opportunity_Columns__c</fullName>
        <externalId>false</externalId>
        <label>Opportunity Columns</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Opportunity_Industry_Filter__c</fullName>
        <defaultValue>&quot;All&quot;</defaultValue>
        <description>Last Industry Filter Selected</description>
        <externalId>false</externalId>
        <label>Opportunity Industry Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Office_Filter__c</fullName>
        <defaultValue>&quot;All&quot;</defaultValue>
        <description>Last Office Filter Selection</description>
        <externalId>false</externalId>
        <label>Opportunity Office Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Show_Records_Filter__c</fullName>
        <defaultValue>&quot;TeamOpportunities&quot;</defaultValue>
        <description>Last Show Records Filter Selection</description>
        <externalId>false</externalId>
        <label>Opportunity Show Records Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Sort_Direction__c</fullName>
        <defaultValue>&quot;ASC&quot;</defaultValue>
        <description>Last Opportunity Sort Direction of the Opportunity Manager</description>
        <externalId>false</externalId>
        <label>Opportunity Sort Direction</label>
        <length>5</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Sort_Field__c</fullName>
        <description>Last Sorted Opportunity Field</description>
        <externalId>false</externalId>
        <label>Opportunity Sort Field</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Staff_Id_Filter__c</fullName>
        <description>Last Opportunity Staff Id Filter Selected</description>
        <externalId>false</externalId>
        <label>Opportunity Staff Id Filter</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Staff_Industry_Filter__c</fullName>
        <defaultValue>&quot;All&quot;</defaultValue>
        <description>Last Staff Industry Filter Selected</description>
        <externalId>false</externalId>
        <label>Opportunity Staff Industry Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Staff_Name_Filter__c</fullName>
        <description>Last Opportunity Staff Name Filter Selected</description>
        <externalId>false</externalId>
        <label>Opportunity Staff Name Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Staff_Product_Filter__c</fullName>
        <externalId>false</externalId>
        <label>Opportunity Staff Product Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Staff_Role_Filter__c</fullName>
        <defaultValue>&quot;All&quot;</defaultValue>
        <description>Stores the Last Selected Opportunity Staff Role</description>
        <externalId>false</externalId>
        <label>Opportunity Staff Role Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Stage_Filter__c</fullName>
        <defaultValue>&quot;All&quot;</defaultValue>
        <description>Last Opportunity Stage Filter Selection</description>
        <externalId>false</externalId>
        <label>Opportunity Stage Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Opportunity_Status_Filter__c</fullName>
        <defaultValue>&quot;Active&quot;</defaultValue>
        <description>Last Opportunity Status Filter</description>
        <externalId>false</externalId>
        <label>Opportunity Status Filter</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Production_SF_ID__c</fullName>
        <externalId>true</externalId>
        <label>Production SF ID</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Pipeline_Columns</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Pipeline Column</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>CREATED_DATE</columns>
        <columns>Engagement_Columns__c</columns>
        <columns>Opportunity_Columns__c</columns>
        <columns>User__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>PC-{0000}</displayFormat>
        <label>Pipeline Column Selection Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Pipeline Columns</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
