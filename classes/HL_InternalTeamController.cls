public with sharing class HL_InternalTeamController {
    public Opportunity__c Opp {get; set;}
    public Engagement__c Eng {get; set;}
    public String CustomFilter {get {return 'AND RecordType.Name = \'Houlihan Employee\' AND Status__c = \'Active\'';} set;}
    public String SpecialRole {get; set;}
    public String JobCode {get{
        if (jobCode == null)
            jobCode = HL_JobType.GetJobCode(EntityType == 'Opportunity__c' ? Opp.Job_Type__c : Eng.Job_Type__c);
        return jobCode;
    } set;
                          }
    public Boolean IsForeign {get{
        if (isForeign == null)
            isForeign = EntityType == 'Opportunity__c' ? Opp.Is_Foreign_Office__c : Eng.Is_Foreign_Office__c;
        return isForeign;
    } set;
                             }
    //Determines if an Initiator Role Selection is Required for the Opportunity/Engagement
    public Boolean InitiatorRequired {
        get{
            return HasModifyAccess && EntityType == 'Opportunity__c' && String.isBlank(SpecialRole) && !HasInitiator;
        }
    }
    private Boolean HasInitiator {get; set;}
    public List<Staff_Role__c> StaffRolesCollection {get{
        HL_Staff_Role.RoleType rt = EntityType == 'Opportunity__c' ?  HL_Staff_Role.RoleType.Opportunity : HL_Staff_Role.RoleType.Engagement;
        HL_Staff_Role.RoleLOB roleLOB = LOB == 'CF' ? HL_Staff_Role.RoleLOB.CF : LOB == 'FAS' ? HL_Staff_Role.RoleLOB.FAS : LOB == 'FR' ? HL_Staff_Role.RoleLOB.FR : HL_Staff_Role.RoleLOB.SC;

        if (staffRolesCollection == null)
            staffRolesCollection = String.isBlank(SpecialRole) ? HL_Staff_Role.GetRoles(rt, roleLOB) : HL_Staff_Role.GetByName(SpecialRole);

        return staffRolesCollection;
    } set;
                                                    }
    //Used by the component to allow it to be embedded in other objects, but hide the return button where not applicable
    public Boolean ShowReturn {get{
        return HL_PageUtility.GetParameter('showheader') <> null ? Boolean.valueOf(HL_PageUtility.GetParameter('showheader')) : true;
    } set;
                              }
    public String LOB {get; set;}
    private Id EntityId {get; set;}
    public String EntityType {get {if (entityType == null) {entityType = HL_Utility.GetObjectTypeById(EntityId);} return entityType; } set;}
    public String EntityName {get; set;}
    public String SelectedStaffId {get; set;}
    public String SelectedStaffName {get; set;}
    public Boolean HasModifyAccess {get{
        //Opportunities shouldn't be editable once Engaged
        //Only the System Admin and CAO Profile (for their respective LOB) should have access for engagements
        if (hasModifyAccess == null) {
            if (EntityType == 'Engagement__c') {
                if (HL_SecurityUtility.IsCAO())
                    hasModifyAccess = HL_Group.IsInGroup('CAO_' + LOB);
                else
                    hasModifyAccess = HL_SecurityUtility.IsSysAdmin();
            }
            else{
                Boolean accessCheck = true;

                //For FAS, lock down the Opportunity Roles After Initial Entry (We are using the Internal Team Prompt flag as that Check)
                if(Opp.Line_Of_Business__c == 'FAS' && !Opp.Internal_Team_Prompt__c)
                    accessCheck = (HL_SecurityUtility.IsCAO() && HL_Group.IsInGroup('CAO_' + LOB)) || HL_SecurityUtility.IsSysAdmin() || !String.isBlank(SpecialRole);

                hasModifyAccess = accessCheck && Opp.Stage__c <> 'Engaged';
            }
        }
        return hasModifyAccess;
    } set;
                                   }

    public HL_InternalTeamRecord StaffMemberToAdd {get{
        if (staffMemberToAdd == null) {
            if (!String.IsBlank(SelectedStaffId))
                staffMemberToAdd = new HL_InternalTeamRecord(EntityId, EntityType, HL_Contact.GetById(SelectedStaffId), JobCode, ByPassFS, IsForeign, StaffRolesCollection, null);
            else
                staffMemberToAdd = new HL_InternalTeamRecord(EntityId, EntityType, StaffRolesCollection);
        }

        return staffMemberToAdd;
    } set;
                                                  }

    public Map<Id, List<sObject>> ContactStaffRolesMap {
        get{
            if (contactStaffRolesMap == null) {
                String prefix = EntityType == 'Opportunity__c' ? 'Opportunity' : 'Engagement';
                Id id = EntityId;
                Id contactId;
                String sr = SpecialRole;

                contactStaffRolesMap = new Map<Id, List<sObject>>();

                for (sObject obj : Database.Query('SELECT Contact__c, Contact__r.Name, Contact__r.Title, Contact__r.Department, Contact__r.Is_Foreign_Office__c, ' +
                                                  '       Contact__r.Office__c, Contact__r.Line_Of_Business__c, Contact__r.Registered__c, Contact__r.Staff_Type__c, ' +
                                                  '       Staff_Role__r.Name, Start_Date__c, End_Date__c ' +
                                                  'FROM ' + prefix + '_Internal_Team__c ' +
                                                  'WHERE Staff_Role__c <> null AND Contact__c <> null AND End_Date__c=null AND ' + prefix + '__r.id=:id ' + (!String.isBlank(SpecialRole) ? 'AND Staff_Role__r.Name =:sr ' : '')))
                {

                    if (prefix == 'Opportunity')
                        contactId = ((Opportunity_Internal_Team__c)obj).Contact__c;
                    else
                        contactId = ((Engagement_Internal_Team__c)obj).Contact__c;

                    if (contactStaffRolesMap.get(contactId) == null)
                        contactStaffRolesMap.put(contactId, new List<sObject> {obj});
                    else {
                        List<sObject> revisedStaffRoleList = contactStaffRolesMap.get(contactId);
                        revisedStaffRoleList.add(obj);
                        contactStaffRolesMap.put(contactId, revisedStaffRoleList);
                    }
                }
            }
            return contactStaffRolesMap;
        }
        set;
    }

    //The CAO/CF Users would like to bypass the FS Restriction for them (FS Checkbox on the Staff Role)
    //The Compliance/Registration Requirements will still need to be met
    public Boolean BypassFS {
        get{
            if (bypassFS == null)
                bypassFS = HL_Group.IsInGroup('CAO_CF');

            return bypassFS;
        }
        set;
    }

    public HL_InternalTeamController(ApexPages.StandardController controller) {
        this(controller.getId());
    }
    public HL_InternalTeamController() {
        this(HL_PageUtility.GetParameter('Id'));
    }
    public HL_InternalTeamController(Id entityRecordId) {
        EntityId = entityRecordId;
        HasInitiator = false;

        if (EntityType == 'Opportunity_Approval__c') {
            EntityId = HL_OpportunityApproval.GetById(entityRecordId).Related_Opportunity__c;
            EntityType = 'Opportunity__c';
        }

        if (EntityType == 'Opportunity__c') {
            Opp = HL_Opportunity.GetById(EntityId);
            EntityName = Opp.Name;
            LOB = Opp.Line_of_Business__c;
        }
        else if (EntityType == 'Engagement__c') {
            Eng = HL_Engagement.GetById(EntityId);
            EntityName = Eng.Name;
            LOB = Eng.Line_of_Business__c;
        }
    }

    public List<HL_InternalTeamRecord> TeamRecords {get {
        if (teamRecords == null) {
            Contact c;
            Map<Id, HL_InternalTeamRecord> internalTeamMap = new Map<Id, HL_InternalTeamRecord>();
            HL_InternalTeamRecord itr;
            String roleDescription;
            HasInitiator = false;

            teamRecords = new List<HL_InternalTeamRecord>();

            //Create a map of the staff member to their roles
            for (Id contactId : ContactStaffRolesMap.keySet()) {
                List<sObject> roleList = ContactStaffRolesMap.get(contactId);

                if (EntityType == 'Opportunity__c')
                    c = ((Opportunity_Internal_Team__c)roleList[0]).Contact__r;
                else
                    c = ((Engagement_Internal_Team__c)roleList[0]).Contact__r;

                if (!internalTeamMap.containsKey(contactId)) {
                    itr = new HL_InternalTeamRecord(EntityId, EntityType, c, JobCode, ByPassFS, IsForeign, StaffRolesCollection, roleList);
                    if (!HasInitiator)
                        HasInitiator = itr.ActiveRolesSet.contains('Initiator');
                    internalTeamMap.put(contactId, itr);
                }

            }

            for (String id : internalTeamMap.keySet())
                teamRecords.add(internalTeamMap.get(id));
        }

        //Sort based on our comparable method
        teamRecords.sort();

        return teamRecords;
    } set;
                                                   }

    private void SaveAddedMember(List<sObject> upsertList) {
        if (SelectedStaffId <> '') {
            HL_InternalTeamRecord tr = StaffMemberToAdd;
            tr.ContactId = SelectedStaffId;

            if (!String.IsBlank(SpecialRole)) {
                HL_InternalTeamRecord.StaffRoleAssignment sra = new HL_InternalTeamRecord.StaffRoleAssignment(StaffRolesCollection[0], true, true);
                tr.RoleAssignments = new List<HL_InternalTeamRecord.StaffRoleAssignment> {sra};
                    }

            //Verify at least one role was selected
            if (tr.HasRoleAssigned) {
                //We still need to check for both here in case they choose a staff member
                //who is already on the team
                upsertList.addAll(tr.GetRoleUpdates());
                upsertList.addAll(tr.GetRoleInserts());
                SelectedStaffId = null;
                SelectedStaffName = null;
                StaffMemberToAdd = null;
            }
            else
                HL_PageUtility.ShowError('Please Select at least one Role for the New Team Member.');
        }
    }

    public void SaveTeamEdits() {
        List<SObject> upsertList = new List<SObject>();
        if (HasModifyAccess) {
            try {
                for (HL_InternalTeamRecord tr : TeamRecords) {
                    upsertList.addAll(tr.GetRoleUpdates());
                    upsertList.addAll(tr.GetRoleInserts());
                }

                TeamRecords = null;
                ContactStaffRolesMap = null;
                //Save Added Member at Same Time
                SaveAddedMember(upsertList);
                UpsertChanges(upsertList);
                if (!ApexPages.hasMessages())
                    HL_PageUtility.ShowConfirm(String.IsBlank(SpecialRole) ? 'Staff Roles Updated.' : 'Officers Updated');
            }
            catch (DmlException ex) {
                HL_PageUtility.ShowError(ex);
            }
        }
        else
            HL_PageUtility.ShowError('Insufficient Rights to Modify Roles');
    }

    //For upsertList, we cannot use a generic sObject so must type cast back
    //Ran into troubles casting the whole list without looping through
    private void UpsertChanges(List<SObject> upsertList) {
        if (upsertList.size() > 0) {
            if (EntityType == 'Opportunity__c') {
                List<Opportunity_Internal_Team__c> oitList = new List<Opportunity_Internal_Team__c>();
                for (sObject obj : upsertList)
                    oitList.add((Opportunity_Internal_Team__c)obj);
                upsert oitList;
            }
            else {
                List<Engagement_Internal_Team__c> eitList = new List<Engagement_Internal_Team__c>();
                for (sObject obj : upsertList)
                    eitList.add((Engagement_Internal_Team__c)obj);
                upsert eitList;
            }
        }
    }

    //Refreshes the list of possible role choices for a new staff member
    public void RefreshNewStaffRoles() {
        StaffMemberToAdd = null;
    }

    public PageReference EditClicked() {
        PageReference redirect = new PageReference('/apex/HL_InternalTeamModifyView?id=' + EntityId + '&type=' + EntityType);
        redirect.setRedirect(true);
        return redirect;
    }

    public PageReference BackToEntity() {
        if (Opp <> null && Opp.Internal_Team_Prompt__c && !InitiatorRequired) {
            Opp.Internal_Team_Prompt__c = false;
            update Opp;
        }
        else if (InitiatorRequired) {
            Opp.Internal_Team_Prompt__c = true;
            update Opp;
        }
        return new PageReference('/' + EntityId);
    }
}