@isTest
private class Test_HL_OpportunityViewController {
    @isTest private static void TestBasicFunctionality(){
        //Setup Test Data
        Opportunity__c o = SL_TestSetupUtils.CreateOpp('', 1)[0];
        insert o;
        ApexPages.StandardController sc = new ApexPages.StandardController(o);
        HL_OpportunityViewController con = new HL_OpportunityViewController(sc);
        Boolean onOIT = con.OnOIT;
        System.assert(!ApexPages.hasMessages(ApexPages.Severity.ERROR));
    }
}