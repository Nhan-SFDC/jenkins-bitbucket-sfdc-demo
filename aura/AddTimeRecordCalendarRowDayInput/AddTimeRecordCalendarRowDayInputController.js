({
	onActivityRecordChanged: function(component, event, helper) {
		helper.onActivityRecordChanged(component, helper);
	},
    onDeleteClicked: function(component, event, helper) {
        if (confirm('Are you sure you want to delete this record?')){
            var deleteControl = event.getSource();
   			var recordId = deleteControl.getElement().parentElement.getAttribute("data-key");
        	helper.deleteActivityRecord(component, helper, recordId);
        }
    }
})