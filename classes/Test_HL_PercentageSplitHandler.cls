@isTest
private class Test_HL_PercentageSplitHandler {

    static void dataFactory() {
        List<Role_Split__c> lstSplit = SL_TestSetupUtils.CreateRoleSplit('Role_Split__c', 2);
        lstSplit[0].Percentage_Allocation__c = 15;

        lstSplit[1].Percentage_Allocation__c = 25; 
        lstSplit[1].Role__c = 'Initiator';

        insert lstSplit;
    }
    
    //Add three team members, 1 at at time and check the splits after each addition.
    @isTest
    static void singleAdditions() {
        dataFactory();

        Engagement__c e = SL_TestSetupUtils.CreateEngagement('Engagement__c', 1)[0];
        insert e;

        List<Contact> cList = SL_TestSetupUtils.CreateContact('Contact', 2, SL_TestSetupUtils.ContactType.HOULIHAN_EMPLOYEE);
        insert cList;

        Test.startTest();
        //Add 1 Team and check if the Perent is 15
        Engagement_Internal_Team__c et1 = SL_TestSetupUtils.CreateEIT('Engagement_Internal_Team__c',1)[0]; 
        et1.Contact__c = cList[0].Id;
        et1.Engagement__c = e.Id;
        et1.Start_Date__c = System.today() - 1;
        et1.End_Date__c = System.today() + 1;
        Staff_Role__c staffRole = new Staff_Role__c(Name='Principal',
                                                    Display_Name__c='Principal', 
                                                    Display_Order__c=1,
                                                    CF__c=true, 
                                                    Engagement__c=true,
                                                    Execution_Team__c=true,
                                                    FAS__c=true,
                                                    FR__c=true,
                                                    Opportunity__c=true,
                                                    SC__c=true);
        insert staffRole;
        et1.Staff_Role__c = staffRole.Id;
        insert et1;

        et1 = [Select Id, Staff_Revenue_Split__c from Engagement_Internal_Team__c Where Id = : et1.Id];
        System.assertEquals(null, et1.Staff_Revenue_Split__c, 'Wrong Percent Split');

        //Add another and check the split
        Engagement_Internal_Team__c et2 = SL_TestSetupUtils.CreateEIT('Engagement_Internal_Team__c',1)[0];
        et2.Contact__c = cList[1].Id;
        et2.Engagement__c = e.Id;
        et2.Start_Date__c = System.today() - 1;
        et2.End_Date__c = System.today() + 1;
        et2.Staff_Role__c = staffRole.Id;
        insert et2;

        for (Engagement_Internal_Team__c et : [Select Id, Staff_Revenue_Split__c from Engagement_Internal_Team__c WHERE Engagement__c=: e.Id]) {
            System.assertEquals(null, et.Staff_Revenue_Split__c, 'Wrong Percent Split');
        }

        //Add third record check the split
        Engagement_Internal_Team__c et3 = SL_TestSetupUtils.CreateEIT('Engagement_Internal_Team__c',1)[0];
        et3.Contact__c = cList[1].Id; 
        et3.Engagement__c = e.Id;
        et3.Start_Date__c = System.today() - 1;
        et3.End_Date__c = System.today() + 1;
        et3.Staff_Role__c = staffRole.Id;
        insert et3;

        for (Engagement_Internal_Team__c et : [Select Id, Staff_Revenue_Split__c from Engagement_Internal_Team__c WHERE Engagement__c=: e.Id]) {
            System.assertEquals(null, et.Staff_Revenue_Split__c, 'Wrong Percent Split');
        }

        Test.stopTest();
    }
    //Add All three team members in bulk, check the splits after all the inserts.
     @isTest
    static void bulkAdditions() {
        dataFactory();

        Engagement__c e = SL_TestSetupUtils.CreateEngagement('Engagement__c', 1)[0];
        insert e;

        List<Contact> cList = SL_TestSetupUtils.CreateContact('Contact', 2, SL_TestSetupUtils.ContactType.HOULIHAN_EMPLOYEE);
        insert cList;

        Test.startTest();
        
        Staff_Role__c staffRole = new Staff_Role__c(Name='Principal',
                                                    Display_Name__c='Principal', 
                                                    Display_Order__c=1,
                                                    CF__c=true, 
                                                    Engagement__c=true,
                                                    Execution_Team__c=true,
                                                    FAS__c=true,
                                                    FR__c=true,
                                                    Opportunity__c=true,
                                                    SC__c=true);
        insert staffRole;

        List<Engagement_Internal_Team__c> lstET = SL_TestSetupUtils.CreateEIT('Engagement_Internal_Team__c',3);
        for(Engagement_Internal_Team__c et: lstEt)
        {            
            et.Engagement__c = e.Id;
            et.Staff_Role__c = staffRole.Id;
            et.End_Date__c = System.today() + 1;
            et.Start_Date__c = System.today() - 1;
        }

        lstEt[0].Contact__c = cList[0].Id;                
        lstEt[1].Contact__c = cList[1].Id;
        //Add third record check the split        
        lstEt[2].Contact__c = cList[1].Id;

        insert lstEt; 

        for (Engagement_Internal_Team__c et : [Select Id, Staff_Revenue_Split__c from Engagement_Internal_Team__c WHERE Engagement__c=: e.Id]) {
            System.assertEquals(null, et.Staff_Revenue_Split__c, 'Wrong Percent Split');
        }

        Test.stopTest();
    }
    //Add a team menber, check the split, delete it and undelete a team member and check the splits
     @isTest
    static void checkDeletionUndeletion() {
        dataFactory();

        Engagement__c e = SL_TestSetupUtils.CreateEngagement('Engagement__c', 1)[0];
        insert e;

        List<Contact> cList = SL_TestSetupUtils.CreateContact('Contact', 2, SL_TestSetupUtils.ContactType.HOULIHAN_EMPLOYEE);
        insert cList;

        Test.startTest();
        //Add 1 Team and check if the Perent is 15
        Engagement_Internal_Team__c et1 = SL_TestSetupUtils.CreateEIT('Engagement_Internal_Team__c',1)[0]; 
        et1.Contact__c = cList[0].Id;
        et1.Engagement__c = e.Id;
        et1.Start_Date__c = System.today() - 1;
        et1.End_Date__c = System.today() + 1;
        Staff_Role__c staffRole = new Staff_Role__c(Name='Principal',
                                                    Display_Name__c='Principal', 
                                                    Display_Order__c=1,
                                                    CF__c=true, 
                                                    Engagement__c=true,
                                                    Execution_Team__c=true,
                                                    FAS__c=true,
                                                    FR__c=true,
                                                    Opportunity__c=true,
                                                    SC__c=true);
        insert staffRole;
        et1.Staff_Role__c = staffRole.Id;
        insert et1;
        
        et1 = [Select Id, Staff_Revenue_Split__c from Engagement_Internal_Team__c Where Id = : et1.Id];
        System.assertEquals(null, et1.Staff_Revenue_Split__c, 'Wrong Percent Split');

        //Add another and check the split
        Engagement_Internal_Team__c et2 = SL_TestSetupUtils.CreateEIT('Engagement_Internal_Team__c',1)[0]; 
        et2.Contact__c = cList[1].Id;
        et2.Engagement__c = e.Id;        
        et2.Start_Date__c = System.today() - 1;
        et2.End_Date__c = System.today() + 1;
        et2.Staff_Role__c = staffRole.Id;
        insert et2;

        for (Engagement_Internal_Team__c et : [Select Id, Staff_Revenue_Split__c from Engagement_Internal_Team__c WHERE Engagement__c=: e.Id]) {
            System.assertEquals(null, et.Staff_Revenue_Split__c, 'Wrong Percent Split');
        }

       delete et1;
        

        et2 = [Select Id, Staff_Revenue_Split__c from Engagement_Internal_Team__c Where Id = : et2.Id];
        System.assertEquals(null, et1.Staff_Revenue_Split__c, 'Wrong Percent Split');

        Database.undelete(et1);

        for (Engagement_Internal_Team__c et : [Select Id, Staff_Revenue_Split__c from Engagement_Internal_Team__c WHERE Engagement__c=: e.Id]) {
            //System.assertEquals(7.5, et.Staff_Revenue_Split__c, 'Wrong Percent Split');
        }

        Test.stopTest();
    }
    
      @isTest
    static void checkbatchJob() {
        dataFactory();

        Engagement__c e = SL_TestSetupUtils.CreateEngagement('Engagement__c', 1)[0];        
        e.Recalculate_Deal_Percentages__c = true;
        insert e;

        List<Contact> cList = SL_TestSetupUtils.CreateContact('Contact', 2, SL_TestSetupUtils.ContactType.HOULIHAN_EMPLOYEE);
        insert cList;
        
        Staff_Role__c staffRole = new Staff_Role__c(Name='Principal',
                                                    Display_Name__c='Principal', 
                                                    Display_Order__c=1,
                                                    CF__c=true, 
                                                    Engagement__c=true,
                                                    Execution_Team__c=true,
                                                    FAS__c=true,
                                                    FR__c=true,
                                                    Opportunity__c=true,
                                                    SC__c=true);
        insert staffRole;
        
        Engagement_Internal_Team__c et1 = SL_TestSetupUtils.CreateEIT('Engagement_Internal_Team__c',1)[0]; 
        et1.Contact__c = cList[0].Id;
        et1.Engagement__c = e.Id;
        et1.Start_Date__c = System.today() - 1;
        et1.End_Date__c = System.today() + 1;
        et1.Staff_Role__c = staffRole.Id;
        insert et1;
        
        Engagement_Internal_Team__c et2 = SL_TestSetupUtils.CreateEIT('Engagement_Internal_Team__c',1)[0]; 
        et2.Contact__c = cList[1].Id;
        et2.Engagement__c = e.Id;
        et2.Start_Date__c = System.today() - 1;
        et2.End_Date__c = System.today() + 1;
        et2.Staff_Role__c = staffRole.Id;
        insert et2;

        //Add third record check the split
        Engagement_Internal_Team__c et3 = SL_TestSetupUtils.CreateEIT('Engagement_Internal_Team__c',1)[0]; 
        et3.Contact__c = cList[1].Id;
        et3.Engagement__c = e.Id;
        et3.Start_Date__c = System.today() - 1;
        et3.End_Date__c = System.today() - 1;
        et3.Staff_Role__c = staffRole.Id;		
        insert et3;

        et3.Staff_Revenue_Split__c = 5;
        update et3;

        Test.startTest();

        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('ScheduleApexClassTest',CRON_EXP, new SL_Scheduler_PercentageSplit());
        e = [Select Recalculate_Deal_Percentages__c from Engagement__c where Id=: e.Id]; 
        System.assertEquals(false, e.Recalculate_Deal_Percentages__c, 'Split not refreshed');    
   
        Test.stopTest();
    }
}