@IsTest
private class Test_HL_Staff_Role {
    @isTest private static void TestGetByName(){
        //Setup Test Data
        Staff_Role__c role = new Staff_Role__c(Name = 'Associate', Opportunity__c = True, CF__c = True);
        insert role;

        Test.startTest();

        //Result Found
        List<Staff_Role__c> resultList = HL_Staff_Role.GetByName('Associate');

        //No Result Found
        List<Staff_Role__c> noResultList = HL_Staff_Role.GetByName('NoRole');

        Test.stopTest();

        System.assert(resultList.size() == 1);
        System.assert(noResultList.size() == 0);
    }

     @isTest private static void TestRoleSort(){
       //Setup Test Data
       Staff_Role__c role = new Staff_Role__c(Name = 'Associate', Opportunity__c = True, CF__c = True);
       insert role;

       Test.startTest();

       List<Staff_Role__c> cfOppRoleList = HL_Staff_Role.GetRoles(HL_Staff_Role.RoleType.Opportunity, HL_Staff_Role.RoleLOB.CF);
       Set<String> rolesToSortSet = new Set<String>();
       for(Staff_Role__c sr : cfOppRoleList)
            rolesToSortSet.Add(sr.Name);
       List<String> sortedRoleList = HL_Staff_Role.GetSortedRoles(rolesToSortSet);

       Test.stopTest();

       //Verify the sorted roles list still contains roles
      System.assert(sortedRoleList.size() > 0);
    }
    
    @isTest private static void TestFilterSelectableRolesFS(){
        //Setup Test Data
        Contact c = SL_TestSetupUtils.CreateContact('', 1, SL_TestSetupUtils.ContactType.EXTERNAL_CONTACT)[0];
        c.Department = 'FS';
        c.Registered__c = true;
        insert c;
        Staff_Role__c role = new Staff_Role__c(Name = 'Associate', Opportunity__c = True, CF__c = True, FS__C = False);
        insert role;

        Test.startTest();

        List<Staff_Role__c> roleResultList = HL_Staff_Role.FilterSelectableRoles(c, 'jobCode', false, false, new List<Staff_Role__c>{role});

        Test.stopTest();

        System.assert(roleResultList.size() == 0);
    }
    
     @isTest private static void TestFilterSelectableRolesRegisteredStaffNonRegisteredOnlyRole(){
        //Setup Test Data
        Contact c = SL_TestSetupUtils.CreateContact('', 1, SL_TestSetupUtils.ContactType.EXTERNAL_CONTACT)[0];
         c.Registered__c = true;
        insert c;
        Staff_Role__c role = new Staff_Role__c(Name = 'Associate', Opportunity__c = True, CF__c = True, Non_Registered_Only__c = True);
        insert role;

        Test.startTest();

        List<Staff_Role__c> roleResultList = HL_Staff_Role.FilterSelectableRoles(c, 'jobCode', false, false, new List<Staff_Role__c>{role});

        Test.stopTest();

        System.assert(roleResultList.size() == 0);
    }
    
    @isTest private static void TestFilterSelectableRolesFinStaffNonFinOnlyRole(){
        //Setup Test Data
        Contact c = SL_TestSetupUtils.CreateContact('', 1, SL_TestSetupUtils.ContactType.EXTERNAL_CONTACT)[0];
        c.Registered__c = true;
        c.Staff_Type__c = 'FIN';
        insert c;
        Staff_Role__c role = new Staff_Role__c(Name = 'Associate', Opportunity__c = True, CF__c = True, Non_Fin_Only__c = True);
        insert role;

        Test.startTest();

        List<Staff_Role__c> roleResultList = HL_Staff_Role.FilterSelectableRoles(c, 'jobCode', false, false, new List<Staff_Role__c>{role});

        Test.stopTest();

        System.assert(roleResultList.size() == 0);
    }
    
    @isTest private static void TestFilterSelectableRolesNonFinStaffFinOnlyRole(){
        //Setup Test Data
        Contact c = SL_TestSetupUtils.CreateContact('', 1, SL_TestSetupUtils.ContactType.EXTERNAL_CONTACT)[0];
        c.Registered__c = true;
        c.Staff_Type__c = '';
        insert c;
        Staff_Role__c role = new Staff_Role__c(Name = 'Associate', Opportunity__c = True, CF__c = True, Fin_Only__c = True);
        insert role;

        Test.startTest();

        List<Staff_Role__c> roleResultList = HL_Staff_Role.FilterSelectableRoles(c, 'jobCode', false, false, new List<Staff_Role__c>{role});

        Test.stopTest();

        System.assert(roleResultList.size() == 0);
    }

    @isTest private static void TestFilterSelectableRolesNonRegistered(){
        //Setup Test Data
        Contact c = SL_TestSetupUtils.CreateContact('', 1, SL_TestSetupUtils.ContactType.EXTERNAL_CONTACT)[0];
        c.Office__c = 'LA';
        c.Registered__c = false;
        
        insert c;
        c = [SELECT Department, Is_Foreign_Office__c, Registered__c, Staff_Type__c FROM Contact WHERE Id =: c.Id];
        
        Staff_Role__c role = new Staff_Role__c(Name = 'Associate', Opportunity__c = True, CF__c = True, Registered_Only__c = True);
        insert role;

        Test.startTest();

        List<Staff_Role__c> roleResultList = HL_Staff_Role.FilterSelectableRoles(c, '', false, false, new List<Staff_Role__c>{role});

        Test.stopTest();

        System.assert(roleResultList.size() == 0);
    }
    
    @isTest private static void TestFilterSelectableRolesNonRegisteredForeign(){
        //Setup Test Data
        Contact c = SL_TestSetupUtils.CreateContact('', 1, SL_TestSetupUtils.ContactType.EXTERNAL_CONTACT)[0];
        c.Office__c = 'HK';
        c.Staff_Type__c = '';
        c.Registered__c = false;
        
        insert c;
        c = [SELECT Department, Is_Foreign_Office__c, Registered__c, Staff_Type__c FROM Contact WHERE Id =: c.Id];
        
        Staff_Role__c role = new Staff_Role__c(Name = 'Associate', Opportunity__c = True, CF__c = True, Registered_Only__c = True);
        insert role;

        Test.startTest();

        List<Staff_Role__c> roleResultList = HL_Staff_Role.FilterSelectableRoles(c, 'jobCode', false, false, new List<Staff_Role__c>{role});

        Test.stopTest();

        System.assert(roleResultList.size() == 0);
    }
}