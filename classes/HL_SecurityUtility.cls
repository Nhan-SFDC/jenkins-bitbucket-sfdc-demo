public without sharing class HL_SecurityUtility {
	public static Boolean HasProfile(string profile){
    	Profile p = [Select Id FROM Profile WHERE Name =: profile];
        return (p != null && Userinfo.getProfileId() == p.Id);
    }

	//Profile Related Checks
    public static Boolean IsCAO(){
        //Custom Setting
        Profile_Ids__c p = Profile_Ids__c.getOrgDefaults();
        return UserInfo.getProfileId() == p.CAO__c;
    }
    public static Boolean IsSysAdmin(){
         //Custom Setting
        Profile_Ids__c p = Profile_Ids__c.getOrgDefaults();
        Id userProfileId = UserInfo.getProfileId();
        return userProfileId == p.System_Administrator__c || userProfileId == p.System_Administrator_Read_Only__c;
    }

    //Verify Field Accessibility - Especially Useful for Lightning Queries
    public static Boolean VerifyFieldAccess(List<String> fieldList, Map<String, Schema.SObjectField> fieldMap){
        Boolean hasAccess = true;
        for(String field : fieldList){
            if(!fieldMap.get(field).getDescribe().isAccessible()){
                hasAccess = false;
                break;
            }
        }

        if(!hasAccess)
            throw new System.NoAccessException();

        return hasAccess;
    }
}