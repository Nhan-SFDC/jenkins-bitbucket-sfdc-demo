trigger HL_CampaignMemberTrigger on CampaignMember (before update, before delete, after insert, after update) {
    if(Trigger.isAfter && (Trigger.IsInsert || Trigger.IsUpdate)){
        if(!System.IsFuture())
            HL_CampaignMemberHandler.SynchronizeParentCampaignMembers(Trigger.newMap.keySet());
    }

    //Audit Tracking
    if(Trigger.IsBefore && (Trigger.IsUpdate || Trigger.IsDelete) &&
    		HL_TriggerSetting.IsEnabled(HL_TriggerSetting.TriggerType.Audit_Campaign_Member))
    {
        HL_AuditRecordHandler auditHandler = new HL_AuditRecordHandler(SObjectType.CampaignMember.getSobjectType());
        auditHandler.RecordAudit(Trigger.oldMap, Trigger.newMap);
    }
}