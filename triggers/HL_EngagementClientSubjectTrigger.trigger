trigger HL_EngagementClientSubjectTrigger on Engagement_Client_Subject__c (after insert, after update, after delete) {
	HL_EngagementClientSubjectHandler handler = new HL_EngagementClientSubjectHandler(Trigger.isExecuting, Trigger.size);
    
    if(Trigger.IsAfter){
        if(Trigger.isInsert)
            handler.OnAfterInsert(Trigger.New);
        else if(Trigger.isUpdate)
            handler.OnAfterUpdate(Trigger.New);
        else if(Trigger.isDelete)
            handler.OnAfterDelete(Trigger.Old);
    }
}