@isTest
private class Test_HL_CF_EngagementSummaryController {
    @testSetup
    private static void SetupEngagementSummaryData(){
        HL_General__c customSettings = new HL_General__c(Show_CF_Summary_Contact_Module__c = true);
        insert customSettings;

        Account accountRecord = (Account)HL_TestFactory.CreateSObject('Account',false);
        accountRecord.Name = 'Test Account 01';
        accountRecord.Ownership = 'Private';
        insert accountRecord;

        Engagement__c engagementRecord = HL_TestFactory.CreateEngagement(false);
        engagementRecord.RecordTypeId = Schema.SObjectType.Engagement__c.getRecordTypeInfosByName().get('Sellside').getRecordTypeId();
        engagementRecord.Client__c = accountRecord.Id;
        engagementRecord.Subject__c = accountRecord.Id;
        insert engagementRecord;

        Staff_Role__c staffRoleRecord = new Staff_Role__c(Name = 'Manager', Engagement__c = true);
        insert staffRoleRecord;

        Contact externalContactRecord = HL_TestFactory.CreateContact(HL_TestFactory.ContactRecordType.External_Contact, false);
        externalContactRecord.AccountId = accountRecord.Id;
        insert externalContactRecord;

        Engagement_External_Team__c externalTeamRecord = (Engagement_External_Team__c)HL_TestFactory.CreateSObject('Engagement_External_Team__c', false);
        externalTeamRecord.Contact__c = externalContactRecord.Id;
        externalTeamRecord.Engagement__c = engagementRecord.Id;
        externalTeamRecord.Role__c = 'Management Contact';
        insert externalTeamRecord;
    }

    @isTest
    private static void TestFullCompletion() {
        Account accountRecord = [SELECT Name FROM Account LIMIT 1];
        Engagement__c engagementRecord = [SELECT Name FROM Engagement__c LIMIT 1];
        Staff_Role__c staffRoleRecord = [SELECT Name, Engagement__c FROM Staff_Role__c LIMIT 1];

        Engagement_Summary__c engagementSummaryRecord = new Engagement_Summary__c(Engagement__c = engagementRecord.Id);
        engagementSummaryRecord.No_Credit_Facility_Data__c = false;
        insert engagementSummaryRecord;

        Contact contactRecord = HL_TestFactory.CreateContact(HL_TestFactory.ContactRecordType.Houlihan_Employee, false);
        contactRecord.User__c = UserInfo.getUserId();
        insert contactRecord;

        Engagement_Internal_Team__c internalTeamRecord = (Engagement_Internal_Team__c)HL_TestFactory.CreateSObject('Engagement_Internal_Team__c', false);
        internalTeamRecord.Engagement__c = engagementRecord.Id;
        internalTeamRecord.Contact__c = contactRecord.Id;
        internalTeamRecord.Staff_Role__c = staffRoleRecord.Id;
        insert internalTeamRecord;

        Test.startTest();

        ApexPages.StandardController standardController = new ApexPages.StandardController(engagementSummaryRecord);
        Test.setCurrentPage(new PageReference('/' + engagementSummaryRecord.id));
        HL_CF_EngagementSummaryController controller = new HL_CF_EngagementSummaryController(standardController);
        List<Engagement_External_Team__c> sellerTeam = controller.SellerContacts;
        List<Engagement_External_Team__c> externalTeamList = controller.ExternalTeam;
        List<Debt_Structure__c> debtStructureList = controller.DebtStructures;
        List<HL_CF_EngagementSummaryController.CapitalizationRecord> capitalizationList = controller.CapitalizationRecords;
        List<Engagement_Counterparty__c> winningCounterpartyList = controller.WinningCounterparties;
        List<Engagement_Counterparty_Contact__c> counterpartyContactList = controller.CounterpartyContacts;

        //Fix Required Errors
        controller.VerificationCode = String.valueOf(engagementRecord.Id).right(5);
  
        //Winning Counterparty Required
        Engagement_Counterparty__c engagementCounterpartyRecord = (Engagement_Counterparty__c)HL_TestFactory.CreateSObject('Engagement_Counterparty__c',false);
        engagementCounterpartyRecord.Engagement__c = engagementRecord.Id;
        engagementCounterpartyRecord.Company__c = accountRecord.Id;
        engagementCounterpartyRecord.Closing_Round_Bid_Date__c = Date.today();
        insert engagementCounterpartyRecord;

        controller.WinningCounterparties = null;
        //Winning Counterparty Fields Required
        engagementCounterpartyRecord.Process_Type__c = 'Process Type';
        engagementCounterpartyRecord.Platform_Type__c = 'Platform Type';
        update engagementCounterpartyRecord;
        controller.WinningCounterparties = null;
		
        //Sellside Fields Required
        for (FieldSetMember member : SObjectType.Engagement_Summary__c.FieldSets.CF_Required_Seller_Detail_Fields.getFields()) {
            if (member.getType() == Schema.DisplayType.DOUBLE)
                engagementSummaryRecord.put(member.getFieldPath(), 0);
            else
                engagementSummaryRecord.put(member.getFieldPath(), 'N/A');
        }

        //Public Company Process
        for (FieldSetMember member : SObjectType.Engagement_Summary__c.FieldSets.CF_Required_Public_Company_Fields.getFields()) {
            if (member.getType() == Schema.DisplayType.DOUBLE)
                engagementSummaryRecord.put(member.getFieldPath(), 0);
            else
                engagementSummaryRecord.put(member.getFieldPath(), 'N/A');
        }

        //Purchase Price and Structure Fields Required
        for (FieldSetMember member : SObjectType.Engagement_Summary__c.FieldSets.CF_Required_Purchase_Price_Fields.getFields()) {
            if (member.getType() == Schema.DisplayType.DOUBLE)
                engagementSummaryRecord.put(member.getFieldPath(), 0);
            else
                engagementSummaryRecord.put(member.getFieldPath(), 'N/A');
        }

        //Key Contract Terms Required
        for (FieldSetMember member : SObjectType.Engagement_Summary__c.FieldSets.CF_Required_Key_Contract_Fields.getFields()) {
            if (member.getType() == Schema.DisplayType.DOUBLE)
                engagementSummaryRecord.put(member.getFieldPath(), 0);
            else
                engagementSummaryRecord.put(member.getFieldPath(), 'N/A');
        }

        //Employee Terms
        for (FieldSetMember member : SObjectType.Engagement_Summary__c.FieldSets.CF_Required_Employee_Terms_Fields.getFields()) {
            if (member.getType() == Schema.DisplayType.DOUBLE)
                engagementSummaryRecord.put(member.getFieldPath(), 0);
            else
                engagementSummaryRecord.put(member.getFieldPath(), 'N/A');
        }

        //Credit Facility
        engagementSummaryRecord.No_Credit_Facility_Data__c = true;

        update engagementSummaryRecord;

        Debt_Structure__c debtStructureRecord = controller.NewDebtStructure;
        controller.SaveNewDebtStructure();

        controller.SubmitForApproval();
        controller.PrepareApprovalSubmission();
        controller.RejectProcess();
        controller.PrepareApprovalSubmission();
        controller.ApproveProcess();
        Test.stopTest();

        //Confirm no Error Messages
        System.assert(!ApexPages.hasMessages(ApexPages.severity.ERROR));
    }

    @isTest
    private static void TestFullCompletionWithMultipleApproversAndRunAs() {
        integer approverSize = 3;

        Account accountRecord = [SELECT Name FROM Account LIMIT 1];
        Engagement__c engagementRecord = [SELECT Name FROM Engagement__c LIMIT 1];
        Staff_Role__c staffRoleRecord = [SELECT Name, Engagement__c FROM Staff_Role__c LIMIT 1];

        Engagement_Summary__c engagementSummaryRecord = new Engagement_Summary__c(Engagement__c = engagementRecord.Id);
        engagementSummaryRecord.No_Credit_Facility_Data__c = false;
        insert engagementSummaryRecord;

        //Need EIT Principal or Manager for Submission/ We will Run As Using Standard User Template
        Profile standardProfile = [SELECT Id FROM Profile WHERE Name = 'Standard User Template'];
        List<Contact> managerList = HL_TestFactory.CreateContactList(HL_TestFactory.ContactRecordType.Houlihan_Employee, false, approverSize);
        List<User> userList = new List<User>();
        for (integer i = 0; i < managerList.size(); i++) {
            userList.Add(new User(Alias = 'standt', Email = 'standarduser' + i + '@testorg.com',
                                  EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                                  LocaleSidKey = 'en_US', ProfileId = standardProfile.Id,
                                  TimeZoneSidKey = 'America/Los_Angeles', UserName = 'standardusertemplate' + i + '@testorg.com'));

        }
        insert userList;

        for (integer i = 0; i < managerList.size(); i++)
            managerList[i].User__c = userList[i].Id;

        insert managerList;

        List<Engagement_Internal_Team__c> engagementInternalTeamList = (List<Engagement_Internal_Team__c>)HL_TestFactory.CreateSObjectList('Engagement_Internal_Team__c', false, approverSize);
        for (integer i = 0; i < managerList.size(); i++) {
            engagementInternalTeamList[i].Engagement__c = engagementRecord.Id;
            engagementInternalTeamList[i].Contact__c = managerList[i].Id;
            engagementInternalTeamList[i].Staff_Role__c = staffRoleRecord.Id;
        }
        insert engagementInternalTeamList;

        ApexPages.StandardController standardController = new ApexPages.StandardController(engagementSummaryRecord);
        Test.setCurrentPage(new PageReference('/' + engagementSummaryRecord.id));
        HL_CF_EngagementSummaryController controller = new HL_CF_EngagementSummaryController(standardController);
        List<Engagement_External_Team__c> sellerTeamList = controller.SellerContacts;
        List<Engagement_External_Team__c> externalTeamList = controller.ExternalTeam;
        List<Debt_Structure__c> debtStructureList = controller.DebtStructures;
        List<HL_CF_EngagementSummaryController.CapitalizationRecord> capitalizationList = controller.CapitalizationRecords;
        List<Engagement_Counterparty__c> winningCounterpartyList = controller.WinningCounterparties;
        List<Engagement_Counterparty_Contact__c> counterpartyContactList = controller.CounterpartyContacts;

        //Fix Required Errors
		controller.VerificationCode = String.valueOf(engagementRecord.Id).right(5);
        
        //Winning Counterparty Required
        Engagement_Counterparty__c engagementCounterpartyRecord = (Engagement_Counterparty__c)HL_TestFactory.CreateSObject('Engagement_Counterparty__c',false);
        engagementCounterpartyRecord.Engagement__c = engagementRecord.Id;
        engagementCounterpartyRecord.Company__c = accountRecord.Id;
        engagementCounterpartyRecord.Closing_Round_Bid_Date__c = Date.today();
        insert engagementCounterpartyRecord;

        controller.WinningCounterparties = null;
        //Winning Counterparty Fields Required
        engagementCounterpartyRecord.Process_Type__c = 'Process Type';
        engagementCounterpartyRecord.Platform_Type__c = 'Platform Type';
        update engagementCounterpartyRecord;
        controller.WinningCounterparties = null;

        //Sellside Fields Required
        for (FieldSetMember member : SObjectType.Engagement_Summary__c.FieldSets.CF_Required_Seller_Detail_Fields.getFields()) {
            if (member.getType() == Schema.DisplayType.DOUBLE)
                engagementSummaryRecord.put(member.getFieldPath(), 0);
            else
                engagementSummaryRecord.put(member.getFieldPath(), 'N/A');
        }

        //Public Company Process
        for (FieldSetMember member : SObjectType.Engagement_Summary__c.FieldSets.CF_Required_Public_Company_Fields.getFields()) {
            if (member.getType() == Schema.DisplayType.DOUBLE)
                engagementSummaryRecord.put(member.getFieldPath(), 0);
            else
                engagementSummaryRecord.put(member.getFieldPath(), 'N/A');
        }

        //Purchase Price and Structure Fields Required
        for (FieldSetMember member : SObjectType.Engagement_Summary__c.FieldSets.CF_Required_Purchase_Price_Fields.getFields()) {
            if (member.getType() == Schema.DisplayType.DOUBLE)
                engagementSummaryRecord.put(member.getFieldPath(), 0);
            else
                engagementSummaryRecord.put(member.getFieldPath(), 'N/A');
        }

        //Key Contract Terms Required
        for (FieldSetMember member : SObjectType.Engagement_Summary__c.FieldSets.CF_Required_Key_Contract_Fields.getFields()) {
            if (member.getType() == Schema.DisplayType.DOUBLE)
                engagementSummaryRecord.put(member.getFieldPath(), 0);
            else
                engagementSummaryRecord.put(member.getFieldPath(), 'N/A');
        }

        //Employee Terms
        for (FieldSetMember member : SObjectType.Engagement_Summary__c.FieldSets.CF_Required_Employee_Terms_Fields.getFields()) {
            if (member.getType() == Schema.DisplayType.DOUBLE)
                engagementSummaryRecord.put(member.getFieldPath(), 0);
            else
                engagementSummaryRecord.put(member.getFieldPath(), 'N/A');
        }

        //Credit Facility
        engagementSummaryRecord.No_Credit_Facility_Data__c = true;

        update engagementSummaryRecord;

        Debt_Structure__c debtStructureRecord = controller.NewDebtStructure;
        controller.SaveNewDebtStructure();

        controller.SubmitForApproval();
        controller.PrepareApprovalSubmission();

        Test.startTest();

        System.runAs(userList[1]) {
            controller.ApproveProcess();
        }

        System.runAs(userList[2]) {
            controller.ApproveProcess();
        }

        Test.stopTest();

        //Confirm no Error Messages
        System.assert(!ApexPages.hasMessages(ApexPages.severity.ERROR));
    }

    @isTest
    private static void TestAddSellerContact(){
        Engagement__c engagementRecord = [SELECT Name FROM Engagement__c LIMIT 1];
        Account accountRecord = [SELECT Name FROM Account LIMIT 1];
        Contact contactRecord = HL_TestFactory.CreateContact(HL_TestFactory.ContactRecordType.External_Contact, false);
        contactRecord.AccountId = accountRecord.Id;
        insert contactRecord;
        Engagement_Summary__c engagementSummaryRecord = new Engagement_Summary__c(Engagement__c = engagementRecord.Id);
        engagementSummaryRecord.No_Credit_Facility_Data__c = false;
        insert engagementSummaryRecord;

        Test.startTest();

        ApexPages.StandardController standardController = new ApexPages.StandardController(engagementSummaryRecord);
        HL_CF_EngagementSummaryController controller = new HL_CF_EngagementSummaryController(standardController);
        controller.SelectedSellerContactId = contactRecord.Id;
        controller.SelectedSellerContactName = contactRecord.Name;
        controller.SelectedSellerContactRole = 'Management Contact';
        controller.AddSellerContact();

        Test.stopTest();

        //Verify there are now 2 (One is Setup in the Initial Data)
        System.assertEquals(2, controller.SellerContacts.size());
    }

    @isTest
    private static void TestDeleteSellerContact(){
        Engagement__c engagementRecord = [SELECT Name FROM Engagement__c LIMIT 1];
        Account accountRecord = [SELECT Name FROM Account LIMIT 1];
        Contact contactRecord = HL_TestFactory.CreateContact(HL_TestFactory.ContactRecordType.External_Contact, false);
        contactRecord.AccountId = accountRecord.Id;
        insert contactRecord;
        Engagement_Summary__c engagementSummaryRecord = new Engagement_Summary__c(Engagement__c = engagementRecord.Id);
        engagementSummaryRecord.No_Credit_Facility_Data__c = false;
        insert engagementSummaryRecord;
        ApexPages.StandardController standardController = new ApexPages.StandardController(engagementSummaryRecord);
        HL_CF_EngagementSummaryController controller = new HL_CF_EngagementSummaryController(standardController);
        controller.SelectedSellerContactId = contactRecord.Id;
        controller.SelectedSellerContactName = contactRecord.Name;
        controller.SelectedSellerContactRole = 'Management Contact';
        controller.AddSellerContact();

        Test.startTest();

        controller.DelId = controller.SellerContacts[0].Id;
        controller.DeleteSellerContact();

        Test.stopTest();

        //Verify there are now 1 (One is Setup in the Initial Data)
        System.assertEquals(1, controller.SellerContacts.size());
    }


    @isTest
    private static void TestAddBuyerContact(){
        Engagement__c engagementRecord = [SELECT Name FROM Engagement__c LIMIT 1];
        Account accountRecord = [SELECT Name FROM Account LIMIT 1];
        Contact contactRecord = HL_TestFactory.CreateContact(HL_TestFactory.ContactRecordType.External_Contact, false);
        contactRecord.AccountId = accountRecord.Id;
        insert contactRecord;
        Engagement_Summary__c engagementSummaryRecord = new Engagement_Summary__c(Engagement__c = engagementRecord.Id);
        engagementSummaryRecord.No_Credit_Facility_Data__c = false;
        insert engagementSummaryRecord;

        Test.startTest();

        ApexPages.StandardController standardController = new ApexPages.StandardController(engagementSummaryRecord);
        HL_CF_EngagementSummaryController controller = new HL_CF_EngagementSummaryController(standardController);
        controller.SelectedBuyerContactId = contactRecord.Id;
        controller.SelectedBuyerContactName = contactRecord.Name;
        controller.SelectedBuyerContactRole = 'Financial Advisor';
        controller.AddBuyerContact();

        Test.stopTest();

        System.assertEquals(1, controller.BuyerContacts.size());
    }

     @isTest
    private static void TestDeleteBuyerContact(){
        Engagement__c engagementRecord = [SELECT Name FROM Engagement__c LIMIT 1];
        Account accountRecord = [SELECT Name FROM Account LIMIT 1];
        Contact contactRecord = HL_TestFactory.CreateContact(HL_TestFactory.ContactRecordType.External_Contact, false);
        contactRecord.AccountId = accountRecord.Id;
        insert contactRecord;
        Engagement_Summary__c engagementSummaryRecord = new Engagement_Summary__c(Engagement__c = engagementRecord.Id);
        engagementSummaryRecord.No_Credit_Facility_Data__c = false;
        insert engagementSummaryRecord;
        ApexPages.StandardController standardController = new ApexPages.StandardController(engagementSummaryRecord);
        HL_CF_EngagementSummaryController controller = new HL_CF_EngagementSummaryController(standardController);
        controller.SelectedBuyerContactId = contactRecord.Id;
        controller.SelectedBuyerContactName = contactRecord.Name;
        controller.SelectedBuyerContactRole = 'Financial Advisor';
        controller.AddBuyerContact();

        Test.startTest();

        controller.DelId = controller.BuyerContacts[0].Id;
        controller.DeleteBuyerContact();

        Test.stopTest();

        System.assertEquals(0, controller.BuyerContacts.size());
    }

    @isTest
    private static void TestAddWinningCounterpartyContact(){
        Engagement__c engagementRecord = [SELECT Name FROM Engagement__c LIMIT 1];
        Account accountRecord = [SELECT Name FROM Account LIMIT 1];
        Contact contactRecord = HL_TestFactory.CreateContact(HL_TestFactory.ContactRecordType.External_Contact, false);
        contactRecord.AccountId = accountRecord.Id;
        insert contactRecord;
        Engagement_Summary__c engagementSummaryRecord = new Engagement_Summary__c(Engagement__c = engagementRecord.Id);
        engagementSummaryRecord.No_Credit_Facility_Data__c = false;
        insert engagementSummaryRecord;
        Engagement_Counterparty__c engagementCounterpartyRecord = (Engagement_Counterparty__c)HL_TestFactory.CreateSObject('Engagement_Counterparty__c',false);
        engagementCounterpartyRecord.Engagement__c = engagementRecord.Id;
        engagementCounterpartyRecord.Company__c = accountRecord.Id;
        engagementCounterpartyRecord.Closing_Round_Bid_Date__c = Date.today();
        insert engagementCounterpartyRecord;

        Test.startTest();

        ApexPages.StandardController standardController = new ApexPages.StandardController(engagementSummaryRecord);
        HL_CF_EngagementSummaryController controller = new HL_CF_EngagementSummaryController(standardController);
        String winningCounterpartyContactFilter = controller.CustomWinningCounterpartyContactFilter;
        Boolean allowDirectContactAdditions = controller.AllowDirectContactAdditions;
        controller.SelectedWinningCounterpartyContactId = contactRecord.Id;
        controller.SelectedWinningCounterpartyContactName = contactRecord.Name;
        controller.NewEngagementCounterpartyContact.Type__c = 'Primary';
        controller.AddWinningCounterpartyContact();

        Test.stopTest();

        System.assert(allowDirectContactAdditions == true);
        System.assert(winningCounterpartyContactFilter.contains(accountRecord.Id));
        System.assertEquals(1, controller.CounterpartyContacts.size());
    }

    @isTest
    private static void TestDeleteWinningCounterpartyContact(){
        Engagement__c engagementRecord = [SELECT Name FROM Engagement__c LIMIT 1];
        Account accountRecord = [SELECT Name FROM Account LIMIT 1];
        Contact contactRecord = HL_TestFactory.CreateContact(HL_TestFactory.ContactRecordType.External_Contact, false);
        contactRecord.AccountId = accountRecord.Id;
        insert contactRecord;
        Engagement_Summary__c engagementSummaryRecord = new Engagement_Summary__c(Engagement__c = engagementRecord.Id);
        engagementSummaryRecord.No_Credit_Facility_Data__c = false;
        insert engagementSummaryRecord;
        Engagement_Counterparty__c engagementCounterpartyRecord = (Engagement_Counterparty__c)HL_TestFactory.CreateSObject('Engagement_Counterparty__c',false);
        engagementCounterpartyRecord.Engagement__c = engagementRecord.Id;
        engagementCounterpartyRecord.Company__c = accountRecord.Id;
        engagementCounterpartyRecord.Closing_Round_Bid_Date__c = Date.today();
        insert engagementCounterpartyRecord;
        ApexPages.StandardController standardController = new ApexPages.StandardController(engagementSummaryRecord);
        HL_CF_EngagementSummaryController controller = new HL_CF_EngagementSummaryController(standardController);
        controller.SelectedWinningCounterpartyContactId = contactRecord.Id;
        controller.SelectedWinningCounterpartyContactName = contactRecord.Name;
        controller.NewEngagementCounterpartyContact.Type__c = 'Primary';
        controller.AddWinningCounterpartyContact();

        Test.startTest();

        controller.DelId = controller.CounterpartyContacts[0].Id;
        controller.DeleteCounterpartyContact();

        Test.stopTest();

        System.assertEquals(0, controller.CounterpartyContacts.size());
    }

    @isTest
    private static void TestUpdatesToRelatedListRecordsAndFunctionality() {
        Account accountRecord = [SELECT Name FROM Account LIMIT 1];
        Engagement__c engagementRecord = [SELECT Name FROM Engagement__c LIMIT 1];
        Staff_Role__c staffRoleRecord = [SELECT Name, Engagement__c FROM Staff_Role__c LIMIT 1];
        Engagement_Summary__c engagementSummaryRecord = new Engagement_Summary__c(Engagement__c = engagementRecord.Id);
        engagementSummaryRecord.No_Credit_Facility_Data__c = false;
        insert engagementSummaryRecord;

        Test.startTest();

        ApexPages.StandardController standardController = new ApexPages.StandardController(engagementSummaryRecord);
        Test.setCurrentPage(new PageReference('/' + engagementSummaryRecord.id));
        HL_CF_EngagementSummaryController controller = new HL_CF_EngagementSummaryController(standardController);
        List<Engagement_External_Team__c> sellerTeam = controller.SellerContacts;
        List<Engagement_External_Team__c> externalTeamList = controller.ExternalTeam;
        List<Debt_Structure__c> debtStructureList = controller.DebtStructures;
        List<HL_CF_EngagementSummaryController.CapitalizationRecord> capitalizationList = controller.CapitalizationRecords;
        List<Engagement_Counterparty__c> winningCounterpartyList = controller.WinningCounterparties;
        List<Engagement_Counterparty_Contact__c> counterpartyContactList = controller.CounterpartyContacts;

        controller.SaveAll();
        controller.SaveAndReturn();
        engagementSummaryRecord.No_Credit_Facility_Data__c = true;
        engagementSummaryRecord.Purchase_Type__c = 'Purchase Type';
        engagementSummaryRecord.Took_338_Selection__c = 'Yes';
        engagementSummaryRecord.Purchase_Price_Base__c = 0;
        engagementSummaryRecord.Seller_Had_Incentive_Plan__c = 'Yes';
        engagementSummaryRecord.Required_Rollover_Equity__c = 'Yes';
        engagementSummaryRecord.Was_Earnout_Included__c = 'No';
        engagementSummaryRecord.Consideration_Seller_Notes__c = 1.0;
        update engagementSummaryRecord;

        //Debt Structure Required
        Debt_Structure__c debtStructureRecord = controller.NewDebtStructure;
        controller.SaveNewDebtStructure();
        controller.RefreshCreditFacilities();
        //Insert a Lender
        Engagement_Client_Subject__c lender = new Engagement_Client_Subject__c(Engagement__c = engagementRecord.Id, Client_Subject__c = accountRecord.Id, Other_Related_Object_Id__c = debtStructureRecord.Id, Type__c = 'Lender');
        insert lender;
        capitalizationList = controller.CapitalizationRecords;
        List<Engagement_Client_Subject__c> lenderList = capitalizationList[0].Lenders;

        //Winning Counterparty Required
        Engagement_Counterparty__c engagementCounterpartyRecord = (Engagement_Counterparty__c)HL_TestFactory.CreateSObject('Engagement_Counterparty__c',false);
        engagementCounterpartyRecord.Engagement__c = engagementRecord.Id;
        engagementCounterpartyRecord.Company__c = accountRecord.Id;
        engagementCounterpartyRecord.Closing_Round_Bid_Date__c = Date.today();
        insert engagementCounterpartyRecord;
        controller.WinningCounterparties = null;

        //Winning Counterparty Process Type Required
        controller.SubmitForApproval();

        engagementCounterpartyRecord.Process_Type__c = 'Process Type';
        update engagementCounterpartyRecord;
        controller.WinningCounterparties = null;

        controller.CancelAction();
        controller.RefreshSellerTeam();
        controller.DelId = debtStructureRecord.Id;
        controller.DeleteDebtStructure();

        Boolean approvedEditAccess = controller.ApprovedEditAccess;
        String warningMessages = controller.WarningMessage;
        String approvalStatus = controller.ApprovalStatus;

        Test.stopTest();

        System.assert(ApexPages.hasMessages(ApexPages.severity.ERROR));
        System.assert(lenderList.size() == 1);
        System.assert(!String.isBlank(warningMessages));
        System.assert(String.isBlank(approvalStatus));
    }
}