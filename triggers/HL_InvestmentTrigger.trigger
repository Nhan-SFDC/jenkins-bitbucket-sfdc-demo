trigger HL_InvestmentTrigger on Investment__c (after insert) {
	if(Trigger.isAfter && Trigger.isInsert){
		HL_InvestmentHandler handler = new HL_InvestmentHandler(Trigger.isExecuting, Trigger.size);
		handler.OnAfterInsert(Trigger.newMap);
	}
}