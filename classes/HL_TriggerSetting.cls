public with sharing class HL_TriggerSetting {
	public enum TriggerType{Account, Audit_Account, Audit_Bid, Audit_Campaign_Member, Audit_Contact, Audit_Coverage_Team,
													Audit_Engagement, Audit_Engagement_Counterparty, Audit_Engagement_External_Team, Audit_Engagement_Internal_Team,
													Audit_Event, Audit_FS_Engagement, Audit_FS_Opp, Audit_Opportunity, Audit_Opportunity_Counterparty,
													Audit_Opportunity_External_Team, Audit_Opportunity_Internal_Team, Audit_Relationship, Audit_Revenue_Accrual,
											    Engagement, Engagement_Comment, Engagement_External_Team, Event, Gift, Monthly_Revenue_Process_Control,
											    Opportunity_Comment, Opportunity_Counterparty, Opportunity_External_Team, Revenue_Accrual, Riva_Activity,
											    Riva_Activity_Link, Time_Record_Period
											   }

	public static boolean IsEnabled(TriggerType triggerName){
		Trigger_Setting__mdt triggerSetting = [SELECT Enabled__c FROM Trigger_Setting__mdt WHERE DeveloperName =: triggerName.name()];

		return triggerSetting == null ? true : triggerSetting.Enabled__c;
	}
}