public with sharing class HL_ExchangeRate {
    public static Map<String,Id> GetCurrencyIsoCodeMap(){
        Map<String,Id> currencyToRate = new Map<String, Id>();
        for(Exchange_Rate__c er : [SELECT CurrencyIsoCode FROM Exchange_Rate__c])
            currencyToRate.put(er.CurrencyIsoCode, er.Id);
        return currencyToRate;
    }
}