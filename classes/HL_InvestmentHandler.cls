public with sharing class HL_InvestmentHandler {
	private boolean isExecuting = false;
    private integer batchSize = 0;
    public boolean IsTriggerContext{get{ return isExecuting;}}
    public static Boolean isAfterInsertFlag = false;

    public HL_InvestmentHandler(boolean isExecuting, integer size){
        isExecuting = isExecuting;
        batchSize = size;
    }
    
     public void OnAfterInsert(Map<Id, Investment__c> insertedMap){
        if(!isAfterInsertFlag){            
            isAfterInsertFlag = true;
            HL_OffsiteTemplate.HandleInsertedInvestmentRecords(insertedMap);
        }
    }
}