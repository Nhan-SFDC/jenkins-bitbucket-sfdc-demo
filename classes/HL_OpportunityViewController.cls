public class HL_OpportunityViewController {
    
    
    //Assuming that SysAdmin and CAO's are included as part of OIT Permissions
    public Boolean OnOIT{get{
        if(onOIT == null)
            onOIT = HL_Utility.IsSysAdmin() || HL_Utility.IsCAO() || HL_OIT.IsActiveOnTeam(Opp.Id,UserInfo.getUserId());
        return onOIT;
    }set;}
    
    public Opportunity__c Opp {get; set;}  
    public boolean displayError {get; set;}
    public boolean displayLockMessage {get; set;}
    
    
    public HL_OpportunityViewController(ApexPages.StandardController controller){
        Opp = (Opportunity__c)controller.getRecord();
        Opp = [SELECT Id, Line_of_Business__c, Confidentiality_Agreement__c, Engagement_Team_Assembled__c, Conflicts_Outcome__c, Converted_to_Engagement__c,
               Client_Ownership__c, Subject_Company_Ownership__c, Date_Engaged__c, SIC_Code__c, Retainer__c, Opportunity_Description__c,
               SubjectId__c, ClientId__c, ProgressMonthly_Fee__c, Job_Type__c, EBITDA_MM__c, Estimated_Close_Date__c, Tail_Expires__c,
               Contingent_Fee__c, NBC_Approved__c, Estimated_Transaction_Size_MM__c, Fairness_Opinion_Component__c, Fee__c,
               FEIS_Approved__c, Valuation_Date__c, TAS_Services__c FROM Opportunity__c WHERE Id =: Opp.Id];
        ApexPages.getMessages().clear();	
        displayError = false;

    }
    
    public PageReference reload() 
    {
    PageReference pageRef = ApexPages.currentPage();
    pageRef.setRedirect(true);
    return pageRef;
	}
    
    public boolean submitApprovalRequest(Opportunity__c oppToRequestApproval)
    {
        oppToRequestApproval.Approval_Process__c = 'CONVERSION';
        oppToRequestApproval.Engagement_Approval_Status__c = 'Requesting Engagement Number';
        displayError = false;
        displayLockMessage = true;
        update oppToRequestApproval;
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setObjectId(oppToRequestApproval.id);
        Approval.ProcessResult result = Approval.process(req1);
        return result.isSuccess();

    }

    public void RequestEngamentNumberValidation() 
    {
        ApexPages.getMessages().clear();	
       //query and arrays for Subject and Client address information 
       List<Account> SubjectQuery = [SELECT BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry FROM Account WHERE Id =: Opp.SubjectId__c];
        String SubStreet = SubjectQuery[0].BillingStreet; 
        String SubCity= SubjectQuery[0].BillingCity; 
        String SubState = SubjectQuery[0].BillingState; 
        String SubPost = SubjectQuery[0].BillingPostalCode; 
        String SubCountry = SubjectQuery[0].BillingCountry; 


        List<Account> ClientQuery = [SELECT BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry FROM Account WHERE Id =: Opp.ClientId__c]; 
        String ClientStreet = ClientQuery[0].BillingStreet; 
        String ClientCity= ClientQuery[0].BillingCity; 
        String ClientState = ClientQuery[0].BillingState; 
        String ClientPost = ClientQuery[0].BillingPostalCode; 
        String ClientCountry = ClientQuery[0].BillingCountry; 
                
        integer ExContactQuery = [SELECT COUNT() FROM Opportunity_External_Team__c WHERE Opportunity__c=: Opp.Id AND Primary__c = TRUE];
        integer ExBillingContact = [SELECT COUNT() FROM Opportunity_External_Team__c WHERE Opportunity__c=: Opp.Id AND Billing_Contact__c = true];

        
        
        boolean BasicConditions = false; 
        boolean SubStateCheck = ((!String.isEmpty(SubCountry) && (SubCountry == 'USA' || SubCountry == 'US' || SubCountry == 'United States' || 
                                SubCountry == 'United States of America' || SubCountry == 'CA' || SubCountry == 'Canada') && SubState==null) ? false : true);
        
        boolean ClientStateCheck = ((!String.isEmpty(ClientCountry) &&  (ClientCountry == 'USA' || ClientCountry == 'US' || ClientCountry == 'United States' ||
                                      ClientCountry == 'United States of America' || ClientCountry == 'CA' || ClientCountry == 'Canada') && ClientState==null) ? false : true); 

        string  LoB             = String.valueOf(Opp.Line_of_Business__c);
        string  CA              = String.valueOf(Opp.Confidentiality_Agreement__c);
        boolean Team            = Opp.Engagement_Team_Assembled__c;
        boolean ConflictOutcome = Opp.Conflicts_Outcome__c;
        boolean Converted       = Opp.Converted_to_Engagement__c;
        string  ClientOwn       = Opp.Client_Ownership__c; 
        string  SubjectOwn      = Opp.Subject_Company_Ownership__c;
        string  DateEngaged     = String.valueOf(Opp.Date_Engaged__c);
        string  SIC             = Opp.SIC_Code__c;
        string  Retainer        = String.valueOf(Opp.Retainer__c);
        string  Description     = Opp.Opportunity_Description__c;
            
            
        if(!String.isEmpty(CA) && 
           ConflictOutcome && 
           Team && 
           !Converted && 
           !String.isEmpty(ClientOwn) && 
           !String.isEmpty(SubjectOwn) && 
           !String.isEmpty(DateEngaged) && 
           !String.isEmpty(SIC) && 
           !String.isEmpty(Retainer) && 
           !String.isEmpty(Description) && 
           !String.isEmpty(SubStreet) && 
           !String.isEmpty(SubCity) && 
           !String.isEmpty(SubState) && 
           !String.isEmpty(SubPost) && 
           !String.isEmpty(ClientStreet) && 
           !String.isEmpty(ClientCity) && 
           !String.isEmpty(ClientPost) && 
           !String.isEmpty(SubPost) && 
           ExContactQuery > 0 &&
           ( (!String.isEmpty(LoB) && !LoB.equals('CF')) || (!String.isEmpty(LoB) && LoB.equals('CF') && ExBillingContact > 0)))
        {
            BasicConditions = true; 
        }


        //Conditions for CF/FR 
        string ProgressFee = String.valueOf(Opp.ProgressMonthly_Fee__c); 
        
        //Conditions only for CF 
        string JobType          = String.valueOf(Opp.Job_Type__c); 
        string EBITDA           = String.valueOf(Opp.EBITDA_MM__c); 
        string EstClose         = String.valueOf(Opp.Estimated_Close_Date__c); 
        string Tail             = String.valueOf(Opp.Tail_Expires__c); 
        string ContFee          = String.valueOf(Opp.Contingent_Fee__c); 
        boolean NBC             = Opp.NBC_Approved__c; 
        string TranSize         = String.valueOf(Opp.Estimated_Transaction_Size_MM__c); 
        string FairnessComponent =String.valueOf(Opp.Fairness_Opinion_Component__c); 
        
        //Conditions only for FAS 
        string FASFee   = String.valueOf(Opp.Fee__c); 
        boolean FEIS    = Opp.FEIS_Approved__c; 
        string ValDate  = String.valueOf(Opp.Valuation_Date__c); 
        string TAS      = String.valueOf(Opp.TAS_Services__c);

        if(Approval.isLocked(Opp.Id))
        {
        	displayError = false;
            displayLockMessage = true;
            reload();
        }
        //CF Logic 
        if (!String.isEmpty(LoB) && LoB.equals('CF') && !String.isEmpty(JobType) && JobType.equals('Sellside') && !String.isEmpty(EBITDA) && !String.isEmpty(EstClose) &&  !String.isEmpty(Tail) && !String.isEmpty(ProgressFee) && !String.isEmpty(ContFee) && 
            NBC && !String.isEmpty(TranSize) && !String.isEmpty(FairnessComponent) && BasicConditions &&  !Approval.isLocked(Opp.Id)) 
        {
            submitApprovalRequest(Opp);
        } 
        else
        {
            if (!String.isEmpty(LoB) && LoB.equals('CF') && !String.isEmpty(JobType) && !JobType.equals('Sellside') && !String.isEmpty(EstClose) && !String.isEmpty(Tail) && !String.isEmpty(ProgressFee)
                && !String.isEmpty(ContFee) && NBC && !String.isEmpty(TranSize) && !String.isEmpty(FairnessComponent) && BasicConditions &&
               !Approval.isLocked(Opp.Id)) 
            {              
                submitApprovalRequest(Opp);
            } 
            else 
            {
                //FAS Logic 
                //Fairness Deals
                if (!String.isEmpty(LoB) && LoB.equals('FAS') && !String.isEmpty(FASFee) && !String.isEmpty(JobType) &&
                    (JobType.equals('ESOP Fairness') || JobType.equals('Fairness')|| JobType.equals('Negotiated Fairness') )
                     && FEIS && BasicConditions &&  !Approval.isLocked(Opp.Id)) 
                    {
                    submitApprovalRequest(Opp);

                } else {
                    //Financial Advisory Deals
                    if (!String.isEmpty(LoB) && LoB.equals('FAS') && !String.isEmpty(FASFee) && !String.isEmpty(JobType) && 
                        (JobType.equals('FA - Portfolio-Valuation') || JobType.equals('FA - Portfolio-Advis/Consultng') || 
                         JobType.equals('FA - Portfolio-Auto Loans') || JobType.equals('FA - Portfolio-Auto Struct Prd') || 
                         JobType.equals('FA - Portfolio-Deriv/Risk Mgmt') || JobType.equals('FA - Portfolio-Diligence/Assets') || 
                         JobType.equals('FA - Portfolio-Funds Transfer') || JobType.equals('FA - Portfolio-GP interest') || 
                         JobType.equals('FA - Portfolio-Real Estate')) && !String.isEmpty(ValDate) && 
                        BasicConditions &&  !Approval.isLocked(Opp.Id)) 
                    {                      
                        submitApprovalRequest(Opp);

                    } 
                    else {
                        //TAS Deals
                        if (!String.isEmpty(LoB) && LoB.equals('FAS') && !String.isEmpty(FASFee) && !String.isEmpty(JobType) && (JobType.equals('TAS - Due Diligence Services'))
                            && !String.isEmpty(TAS) && BasicConditions &&  !Approval.isLocked(Opp.Id)) 
                        {
            				submitApprovalRequest(Opp);

                        } else {
                            if (!String.isEmpty(LoB) && LoB.equals('FAS') && !String.isEmpty(FASFee) && !String.isEmpty(JobType) && 
                                !(JobType.equals('TAS - Due Diligence Services') || JobType.equals('ESOP Fairness') || 
                                  JobType.equals('Fairness') || JobType.equals('Negotiated Fairness') || 
                                  JobType.equals('FA - Portfolio-Valuation') || JobType.equals('FA - Portfolio-Advis/Consultng') || 
                                  JobType.equals('FA - Portfolio-Auto Loans') || JobType.equals('FA - Portfolio-Auto Struct Prd') || 
                                  JobType.equals('FA - Portfolio-Deriv/Risk Mgmt') || JobType.equals('FA - Portfolio-Diligence/Assets') || 
                                  JobType.equals('FA - Portfolio-Funds Transfer') || JobType.equals('FA - Portfolio-GP interest') || 
                                  JobType.equals('FA - Portfolio-Real Estate')) &&
                                BasicConditions &&  !Approval.isLocked(Opp.Id)) 
                            {                              
            					submitApprovalRequest(Opp);


                            } else {
                                //FR Logic 
                                if (LoB.equals('FR') && !String.isEmpty(ProgressFee) && BasicConditions &&  !Approval.isLocked(Opp.Id)) {
            						submitApprovalRequest(Opp);

                                } else {
                                    //SC Logic 
                                    if (LoB.equals('SC') && BasicConditions &&  !Approval.isLocked(Opp.Id)) 
                                    {
                                        submitApprovalRequest(Opp);
                                    }
        
                                    //Error Msg Components in order of their appearance on page layout 
                                    else {
                                        if(!Approval.isLocked(Opp.Id))
                                        {
                                            displayError = true;
                                            displayLockMessage = false;
                                        }
                                        else
                                        {
                                            displayError = false;
                                            displayLockMessage = true;
                                        }

                                        
                                        //Opportunity Detail
                                        //Client/Subject Address Errors
                                        if (String.isEmpty(ClientStreet)) 
                                        {
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Opportunity Detail - Client: Street Address.'));
                                        }
                                        if (String.isEmpty(ClientCity)) 
                                        {
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Opportunity Detail - Client: City Address.'));
                                        }
                                        if (!String.isEmpty (ClientCountry) && (ClientCountry.equals('USA') || ClientCountry.equals('US') || ClientCountry.equals('United States') || 
                                            ClientCountry.equals('United States of America') || ClientCountry.equals('CA') || ClientCountry.equals('Canada')) && String.isEmpty(ClientState)) 
                                        {
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Opportunity Detail - Client: State Address.'));
                                        }
                                        if (String.isEmpty(ClientPost)) 
                                        {
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Opportunity Detail - Client: Postal Code.'));
                                        }                                     
                                        if (String.isEmpty(ClientOwn)) 
                                        {
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Opportunity Detail - Client: Ownership.'));
                                        }
                                        if (String.isEmpty(SubStreet)) {
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Opportunity Detail - Subject: Street Address.'));
                                        }
                                        if (String.isEmpty(SubCity)) {
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Opportunity Detail - Subject: City Address.'));
                                        }
                                        if (!String.isEmpty(SubCountry) && (SubCountry.equals('USA') || SubCountry.equals('US') || SubCountry.equals('United States') || 
                                            SubCountry.equals('United States of America') || SubCountry.equals('CA') || SubCountry.equals('Canada') )&& 
                                            String.isEmpty(SubState)) 
                                        {
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Opportunity Detail - Subject: State Address.'));
                                        }
                                        if (String.isEmpty(SubPost)) 
                                        {
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Opportunity Detail - Subject: Postal Code Address.'));
                                        }
                                        //End Client/Subject Address Errors 
                                        if (String.isEmpty(SubjectOwn)) 
                                        {                                         
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Opportunity Detail - Subject: Ownership.'));
                                        }
                                        if (String.isEmpty(SIC)) 
                                        {                                          
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Opportunity Detail - SIC Code.'));
                                        }
                                        if (!String.isEmpty(LoB) && LoB.equals('FAS') && !String.isEmpty(JobType) &&  (JobType.equals('FA - Portfolio-Valuation') || JobType.equals('FA - Portfolio-Advis/Consultng') || JobType.equals('FA - Portfolio-Auto Loans') || JobType.equals('FA - Portfolio-Auto Struct Prd') || JobType.equals('FA - Portfolio-Deriv/Risk Mgmt') || JobType.equals('FA - Portfolio-Diligence/Assets') || JobType.equals('FA - Portfolio-Funds Transfer') || JobType.equals('FA - Portfolio-GP interest') || JobType.equals('FA - Portfolio-Real Estate')) && String.isEmpty(ValDate)) {                                         
                                           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Opportunity Detail - Valuation Date.'));
                                        }
                                        
                                        //Opportunity Description
                                        if (String.isEmpty(Description)) 
                                        {                                          
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Opportunity Description - Opportunity Description.'));
                                        }
                                        
                                        //Estimated Financials
                                        if (!String.isEmpty(LoB) && (LoB.equals('CF')) && String.isEmpty(TranSize)) 
                                        {                                           
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Estimated Financials - Est. Transaction Size/Market Cap.'));
                                        }
                                        if (!String.isEmpty(LoB) && LoB.equals('CF') && !String.isEmpty(JobType) && JobType.equals('Sellside') && String.isEmpty(EBITDA)) 
                                        {                                           
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Estimated Financials - Client/Subject: EBITDA(MM), please provide a EBITDA(MM) value on the Client or Subject Company record.'));
                                        }
                                        
                                        //Estimated Fees
                                        if (String.isEmpty(Retainer)) 
                                        {                                          
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Estimated Fees - Retainer, input zero if there\'s no Retainer fee.'));
                                        }
                                        if (!String.isEmpty(LoB) && LoB.equals('CF') && String.isEmpty(Tail)) 
                                        {                                           
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Estimated Fees - Tail Expires.'));
                                        }
                                        if (!String.isEmpty(LoB) && (LoB.equals('CF') || LoB.equals('FR')) && String.isEmpty(ProgressFee)) 
                                        {                                         
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Estimated Fees - Progress/Monthly Fee.'));
                                        }
                                        if (!String.isEmpty(LoB) && (LoB.equals('CF')) && String.isEmpty(ContFee)) 
                                        {
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Estimated Fees - Contingent Fee.'));
                                        }
                                        if (!String.isEmpty(LoB) && LoB.equals('FAS') && String.isEmpty(FASFee)) {
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Estimated Fees - Fee.'));
                                        }
                                        
                                        //HL Internal Team
                                        if (!Team) 
                                        {                                     
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'HL Internal Team - Team must include the following roles: Initiator, Seller, Principal, Manager, Associate(Optional), Analyst(Optional).'));
                                        }
                                        
                                        //CA
                                        if (String.isEmpty(CA)) 
                                        {                                          
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Confidentiality Agreement -  Confidentiality Agreement.'));
                                        }
                                        
                                        //Conflicts Check
                                        if (!ConflictOutcome) 
                                        {                                           
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Conflicts Check - A Conflicts Check was completed more than 30 days ago. A new Conflicts Check must be completed.'));
                                        }
                                        
                                        //Administration
                                        if (!String.isEmpty(LoB) && LoB.equals('CF') && String.isEmpty(EstClose)) {
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Administration - Estimated Closed Date.'));
                                        }
                                        if ( !String.isEmpty(LoB) && (LoB.equals('CF')) && String.isEmpty(FairnessComponent)) {                                        
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Administration - Fairness Opinion Component.'));
                                        }
                                        if (String.isEmpty(DateEngaged)) {
                                           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Administration - Date Engaged - Date of Executed Retainer or similar document.'));
                                        }

                                        //TAS
                                        if (!String.isEmpty(LoB) && LoB.equals('FAS') && !String.isEmpty(JobType) && JobType.equals('TAS - Due Diligence Services') && String.isEmpty(TAS)) {                                         
                                           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'TAS Services - TAS Services.'));
                                        }
                                        
                                        //FEIS/NBC
                                        if (!String.isEmpty(LoB) && LoB.equals('FAS') && !String.isEmpty(JobType) && 
                                            (JobType.equals('ESOP Fairness') || JobType.equals('Fairness') || JobType.equals('Negotiated Fairness.')) &&
                                             !FEIS) 
                                        {                                                             
                                           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Approved FEIS form - Please complete and submit this form via the FEIS button.'));
                                        }
                                        if ( !String.isEmpty(LoB) && LoB.equals('CF') && !NBC) 
                                        {                                         
                                           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Approved NBC form - Please complete and submit this form via the NBC button.'));
                                        }
                                        
                                        //Opportunity Contacts
                                        if (ExContactQuery == 0) 
                                        {                                                                                    
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Opportunity Contacts - Add at least one Primary Opportunity Contact.'));
                                        }
                                        
                                        //Opportunity Billing Contacts 
                                        if (!String.isEmpty(LoB) && LoB.equals('CF') && ExBillingContact == 0) 
                                        {                                                                                    
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Opportunity Contacts - Add at least one Billing Contact.'));
                                        }
                                        //Converted Check
                                        if (Converted) 
                                        {
                                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Opportunity has already been engaged previously, cannot submit Opportunities with existing Engagements.'));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    }
}